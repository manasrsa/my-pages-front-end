export const environment = {
  production: true,
  environmentName: 'PRODUCTION',
  apiURL: 'productionApi',
  appInsights: {
    instrumentationKey: '9bb98244-781a-423f-8835-600af942bc1b'
  }
};