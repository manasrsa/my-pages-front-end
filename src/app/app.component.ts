import { Component, Renderer2, OnInit, HostListener, Inject } from '@angular/core';
import {
  Router,
  NavigationStart,
  NavigationEnd,
  ActivatedRoute,
  ActivatedRouteSnapshot,
  Params,
} from '@angular/router';
import { Title } from '@angular/platform-browser';
import { BreadrumService } from './shared/_service/breadcrum/breadcrum.service';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { ValidationService } from './shared/_service/validation/validation.service';
import { CommonService } from './shared/_service/common/common.service';
import { CookieService } from 'ngx-cookie-service';
import { APP_BASE_HREF } from '@angular/common';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})


export class AppComponent implements OnInit {
  prviousUrl = '';
  appTitle = 'pageTitleMypage';
  pageTitle = '';
  currentLang:string='sv';
  countryCode:string='';
  conuntryCode = 'se';
  constructor(
    public title: Title,
    private renderer: Renderer2,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public breadCrumService: BreadrumService,
    public translate: TranslateService,
    private validationService: ValidationService,
    private commonService:CommonService,
    private cookieService:CookieService,
    @Inject(APP_BASE_HREF) private baseHref: string
     ) {
    this.subscribeToRouterEvents();
    this.conuntryCode = this.baseHref.split("/")[1];
    this.currentLang = this.conuntryCode == 'se' ? 'sv' : 'fi';
    //translate.addLangs(['en', 'sv', 'fi']);
    // this.commonService.getLanguage.subscribe((data:any) => {
    //   this.translate.use(data).subscribe((res:any)=>{
        
    //     this.changePageTitle();
    //   });
    // });
    //translate.setDefaultLang('en');
    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      // Change page title when user changes language preference
      this.setTitleTranslation();
    });
    this.countryCode=this.baseHref.split("/")[1];
  }
  @HostListener('document:keyup', ['$event'])
  @HostListener('document:click', ['$event'])
  @HostListener('document:wheel', ['$event'])
  resetTimer() {
    this.commonService.notifyUserAction();
  }
  ngOnInit() {
    this.validationService.getValidation();
  }
  setTitleTranslation(){
    const routeParts = this.generateRouteParts(this.activeRoute.snapshot);
    if (!routeParts.length) { 
      return this.title.setTitle(this.translate.instant(this.appTitle)); }
    // Extract title from parts;
    if(routeParts){
      this.pageTitle = routeParts
      .reverse()
      .map((part) => part.title)
      .reduce((partA, partI) => {
        return `${this.translate.instant(partA)} > ${this.translate.instant(partI)}`;
      });
      this.pageTitle=this.translate.instant(this.pageTitle);
      this.pageTitle += ` | ${this.translate.instant(this.appTitle)}`;
       this.title.setTitle(this.pageTitle);
    }
 
  }
  // Application change page title
  changePageTitle() {
    
    const routeParts = this.generateRouteParts(this.activeRoute.snapshot);
    this.breadCrumService.setBreadcrum(routeParts);
    if (!routeParts.length) { 
      return this.title.setTitle(this.translate.instant(this.appTitle)); }
    // Extract title from parts;
    if(routeParts){
      this.pageTitle = routeParts
      .reverse()
      .map((part) => part.title)
      .reduce((partA, partI) => {
        return `${this.translate.instant(partA)} > ${this.translate.instant(partI)}`;
      });
      this.pageTitle=this.translate.instant(this.pageTitle);
      this.pageTitle += ` | ${this.translate.instant(this.appTitle)}`;
       this.title.setTitle(this.pageTitle);
    }
    
    
  }
  // Generate router part for page title
  generateRouteParts(snapshot: ActivatedRouteSnapshot): IRoutePart[] {
    let routeParts =  [] as IRoutePart[];
    if (snapshot) {
      if (snapshot.firstChild) {
        routeParts = routeParts.concat(
          this.generateRouteParts(snapshot.firstChild)
        );
      }

      if (snapshot.data.title && snapshot.url.length) {
        routeParts.push({
          title: snapshot.data.title,
          breadcrumb: snapshot.data.breadcrumb,
          url: snapshot.url[0].path,
          urlSegments: snapshot.url,
          params: snapshot.params,
        });
      }
    }
    return routeParts;
  }
  // subscribe to router Events
  subscribeToRouterEvents() {
    this.router.events.subscribe((event: any) => {

      if (event instanceof NavigationStart) {
        if (this.prviousUrl) {
          this.renderer.removeClass(document.body, this.prviousUrl);
        }
        const currentUrlSlug = event.url.slice(1);
        this.renderDomClass(currentUrlSlug); // call the render dom class
        this.prviousUrl = 'path-' + currentUrlSlug;
      }
      if (event instanceof NavigationEnd) {
        this.changePageTitle();
        this.translate.addLangs(['sv', 'en', 'fi']);
      // this.translate.setDefaultLang(countryCode);
    if (this.translate.getBrowserLang() !== undefined) {
      let lang = this.cookieService.get('.AspNetCore.Culture');
      if (lang) {
        var langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.currentLang=langCode;
        this.translate.use(langCode);
      }else {
        this.translate.use(this.currentLang);
      }
    }
    else {
      this.translate.use(this.currentLang);
    }
    //if(this.countryCode=='se' && langCode=='fi'){
    //  this.commonService.setLanguage('sv');
    
    //  }
    //  else{
    //    this.commonService.setLanguage(this.currentLang);
    //  }
      this.changePageTitle();
      }
      
    });
  }

  // get current uesr  remove class and add class for body
  renderDomClass(currentUrlSlug) {
    
    switch (currentUrlSlug) {
      case 'overview':
        this.renderer.removeClass(document.body, 'path-login-page');
        this.renderer.addClass(document.body, 'path-' + currentUrlSlug);
        break;
      case 'contact-us':
        this.renderer.removeClass(document.body, 'path-login-page');
        this.renderer.addClass(document.body, 'path-' + currentUrlSlug);
        break;
      case 'power-of-attorney':
        this.renderer.addClass(document.body, 'path-' + currentUrlSlug);
        break;
      case 'messages':
        this.renderer.removeClass(document.body, 'path-login-page');
        this.renderer.addClass(document.body, 'path-' + currentUrlSlug);
        break;
      case 'nets-terms-of-delivery':
        this.renderer.addClass(document.body, 'path-' + currentUrlSlug);
        break;
      default:
        '';
    }
  }
}

interface IRoutePart {
  title: string;
  breadcrumb: string;
  params?: Params;
  url: string;
  urlSegments: any[];
}
