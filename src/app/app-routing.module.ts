
import { Routes } from '@angular/router';
import { LayoutComponent } from './shared/components/layout/layout.component';
export const appRoutingModule: Routes = [
    {
      path: 'overview',
      redirectTo: '/overview',
      pathMatch: 'full'
    },
    {
      path: '', 
      component: LayoutComponent,
        children: [
          {
            path: 'overview',
            loadChildren: () => import('./views/overview/overview.module').then(m => m.OverviewModule),
            data:{
              title: "breacrumbOverview",
              breadcrumb: "breacrumbOverview",
              auth: 'Overview'
            }
          },
          {
           path: 'power-of-attorney', 
           loadChildren:() => import('./views/power-of-attorney/power-of-attoney.module').then(m => m.PowerOfAttoneyModule), 
           data: {title: 'power-of-attorney', breadcrumb: 'Power Of Attorney', auth: 'Power Of Attorney'},
          },
          {
           path: 'contact-us', 
           loadChildren: () => import('./views/contact-us/contactus.module').then(m => m.ContactUsModule),
           data: {title: 'contact-us', breadcrumb: 'Contact Us', auth: 'Contact Us'},
          },
          {
           path: 'messages', 
           loadChildren: () => import('./views/messages/messages.module').then(m => m.MessagesModule),
           data: {title: 'Messages', breadcrumb: 'breadcrumMessages', auth: 'Messages'},
          },
          {
            path: 'nets-terms-of-delivery',
            loadChildren:() => import('./views/nets-terms-of-delivery/net-terms-of-delivery.module').then(m => m.NetTermsOfDeliveryModule),
            data: {title: 'titleNetTerm', breadcrumb: 'Nets terms of delivery', auth: 'NetTermsOfDelivery'},
          }
          // {
          //  path: 'profile', 
          //  loadChildren: './views/profile/profile.module#ProfileModule',
          //  data: {title: 'Profile', breadcrumb: 'Profile', auth: 'Profile'},
          // }
        ]  
    },
    

    // otherwise redirect to home
    { path: '**', redirectTo: 'overview' }
];

