import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared/shared.module';
import { RouterModule, PreloadAllModules, ExtraOptions } from '@angular/router';
import { appRoutingModule } from './app-routing.module';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {DateAdapter, MatNativeDateModule, MatRippleModule, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { ModalComponent } from './shared/components/modal/modal.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CustomDirective } from './shared/directive/custom.directive';
import { ErrorHandler } from '@angular/core';
import { GlobalErrorHandlerService } from './shared/_service/globalError/global-error-handler.service';
import { HttpErrorInterceptor } from './shared/_service/globalError/http-error-interceptor.service';
import { SpinnerComponent } from './shared/components/spinner/spinner.component';
import { APP_BASE_HREF } from '@angular/common';
import { CookieService } from 'ngx-cookie-service';
import { MatTabsModule } from '@angular/material/tabs';
const config: ExtraOptions = {
    useHash: false,
    scrollPositionRestoration: 'enabled',
    enableTracing: false // Turn this on to log routing events to the console
    // ,preloadingStrategy: PreloadAllModules
    ,
    relativeLinkResolution: 'legacy'
};
export const MY_FORMATS = {
   parse: {
     dateInput: 'YYYY-MM-DD',
   },
   display: {
     dateInput: 'YYYY-MM-DD',
     monthYearLabel: 'MMM YYYY',
     dateA11yLabel: 'YYYY-MM-DD',
     monthYearA11yLabel: 'MMMM YYYY',
   },
 };
@NgModule({
   declarations: [
      AppComponent,
      ModalComponent,
      CustomDirective,
      SpinnerComponent

   ],
   imports: [
      BrowserAnimationsModule,
      BrowserModule,
      SharedModule,
      FormsModule,
      ReactiveFormsModule,
      HttpClientModule,
      MatButtonModule,
      MatDialogModule,
      MatInputModule,
      MatTabsModule,
      MatNativeDateModule, MatRippleModule,
      TranslateModule.forRoot({
         loader: {
           provide: TranslateLoader,
           useFactory: httpTranslateLoader,
           deps: [HttpClient]
         }
       }),

      RouterModule.forRoot( appRoutingModule, config)
   ],
   providers: [
      
      {provide: ErrorHandler, useClass: GlobalErrorHandlerService },
      {
         provide: HTTP_INTERCEPTORS,
         useClass: HttpErrorInterceptor,
         multi: true
     },
     { provide: APP_BASE_HREF, useFactory: getBaseUrl },
     CookieService
   ],
   entryComponents: [ModalComponent],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
export function httpTranslateLoader(http: HttpClient) {
     return new TranslateHttpLoader(http, './assets/i18n/', '.json');
 }
 export function getBaseUrl() {
   window['base-href'] = window.location.pathname;
   var country = window['base-href'].split('/')[1];
   return "/" + country + "/"; 
 }