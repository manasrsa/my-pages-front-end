import { APP_BASE_HREF } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
@Injectable({
    providedIn: 'root'
})
export class ConfigSetting {
    public apiUrl;
    constructor( @Inject(APP_BASE_HREF) private baseHref: string) {
        this.apiUrl = this.baseHref.split('/')[1] + '/api/';
    }
}
