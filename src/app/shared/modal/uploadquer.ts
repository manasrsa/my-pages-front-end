import { UploadQueue } from './uploader';


export class Uploader {
  queue: UploadQueue[];

  constructor() {
    this.queue = [];
  }
}