import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { LayoutComponent } from '../components/layout/layout.component';
import { FooterComponent } from '../components/footer/footer.component';
import { HeaderComponent } from '../components/header/header.component';
import { RouterModule } from '@angular/router';
import { BreadcrumbComponent } from '../components/breadcrumb/breadcrumb.component';
import { TranslateModule } from '@ngx-translate/core';
import { DateFormatPipe } from '../pipe/date.format.pipe';
import { CurrencyFormatPipe } from '../pipe/currency.pipe';
import { CurrencyPipe } from '../pipe/currencyPipe';
import { GlobalErrorComponent } from '../components/global-error/global-error.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { DateTimeFormatPipe } from '../pipe/datetime.format.pipe';
import { ErrorMessageComponent } from '../components/ErrorMessage/ErrorMessage.component';
import { ToastsComponent } from '../components/toasts/toasts.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { ProfileModalComponent } from '../components/profile-modal/profile-modal.component';

@NgModule({

  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule,
    MatDatepickerModule,
    MatSnackBarModule,
    MatDialogModule,
    MatIconModule,
    MatTabsModule
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    LayoutComponent,
    BreadcrumbComponent,
    DateFormatPipe,
    CurrencyFormatPipe,
    CurrencyPipe,
    GlobalErrorComponent,
    DateTimeFormatPipe,
    ErrorMessageComponent,
    ToastsComponent,
    ProfileModalComponent
  ],
  exports: [HeaderComponent,
    FooterComponent, LayoutComponent,
    BreadcrumbComponent,
    DateFormatPipe,
    CurrencyFormatPipe,
    CurrencyPipe,
    GlobalErrorComponent,
    DateTimeFormatPipe,
    ErrorMessageComponent

  ],
  entryComponents: [ToastsComponent,ProfileModalComponent]
})
export class SharedModule { }
