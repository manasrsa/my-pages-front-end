import {TranslateLoader} from "@ngx-translate/core";
import {Observable, of} from "rxjs";

declare let readJSON: any;

export class TranslateOverviewLoader implements TranslateLoader {
  constructor(){
    
  }
  getTranslation(lang: string): Observable<any> {
    if (lang=="sv"){
      let sv = readJSON('assets/i18n/se/overview/sv.json');
      return of(sv);
    }
    let en = readJSON('assets/i18n/se/overview/en.json');
    return of(en);
  }
}