import {TranslateLoader} from "@ngx-translate/core";
import {Observable, of} from "rxjs";

declare let readJSON: any;

export class TranslateContactLoader implements TranslateLoader {
  constructor(){
    
  }
  getTranslation(lang: string): Observable<any> {
    if (lang=="sv"){
      let sv = readJSON('assets/i18n/se/contactus/sv.json');
      return of(sv);
    }
    let en = readJSON('assets/i18n/se/contactus/en.json');
    return of(en);
  }
}