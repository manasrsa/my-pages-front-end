import {TranslateLoader} from "@ngx-translate/core";
import {Observable, of} from "rxjs";

declare let readJSON: any;

export class TranslateMessagesLoader implements TranslateLoader {
  constructor(){
    
  }
  getTranslation(lang: string): Observable<any> {
    if (lang=="sv"){
      let sv = readJSON('assets/i18n/se/messages/sv.json');
      return of(sv);
    }
    let en = readJSON('assets/i18n/se/messages/en.json');
    return of(en);
  }
}