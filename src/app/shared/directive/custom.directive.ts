import { Directive, ElementRef, HostListener, Output, EventEmitter } from "@angular/core";
import { Router } from '@angular/router';

@Directive({
  selector: "[click-custom]"
})
export class CustomDirective {
  @Output() public clickOutside = new EventEmitter();
  constructor(private elem: ElementRef,private router:Router) {}

  @HostListener("document:click", ["$event"]) clickDocument(event:Event) {
    if (this.elem.nativeElement.contains(event.target)) {
      if (event.target instanceof HTMLAnchorElement) {
           const element = event.target as HTMLAnchorElement;
           if (element.className === 'routerlink text-underline') {
            
            this.router.navigate(['/messages']);
            this.clickOutside.emit(null);
           }
        }
    } else {
     
    }
  }

  ndOnDestroy() {
    //removeEventListener. Not needed incase of hostlistener. It does this automatically
  }
}


