interface CaseDetails{
    caseNumber:number;
    clientName:string;
    paymentPlanAllowed:any;
    paymentPlanMaxNumOfOccasions:any;
    debtCollectionFee:any;
    interest:any;
    paidAmount:0;
    respiteMaxDaysAllowed:any;
    status:any;
    debtToPay:any;
    debts:any;
}