import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ValidationService } from '../../_service/validation/validation.service';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-ErrorMessage',
  templateUrl: './ErrorMessage.component.html',
  styleUrls: ['./ErrorMessage.component.scss']
})
export class ErrorMessageComponent implements OnInit {

  @Input() control: FormControl;

  constructor(private translate:TranslateService) {
      }

  validFile: any;
  
  get errorMessage() {
    
    for (const propertyName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
        return ValidationService.getValidatorErrorMessage(propertyName);
      }
    }

    return null;
  }

  ngOnInit() {
  }

}
