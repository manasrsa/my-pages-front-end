/* tslint:disable:no-unused-variable */
import { ComponentFixture, TestBed, fakeAsync, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';


import { FormsModule, ReactiveFormsModule, FormGroup, FormControl, ControlContainer, FormBuilder } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ErrorMessageComponent } from './ErrorMessage.component';
import { ValidationService } from '../../_service/validation/validation.service';
import { APP_BASE_HREF } from '@angular/common';

describe('ErrorMessageComponent', () => {
  let component: ErrorMessageComponent;
  let fixture: ComponentFixture<ErrorMessageComponent>;
  let mockForm: FormGroup;
  // create new instance of FormBuilder
  const formBuilder: FormBuilder = new FormBuilder();
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        TranslateModule.forRoot()
      ],
      declarations: [ ErrorMessageComponent ],
      providers: [
        {provide: ValidationService},
        {
          provide: APP_BASE_HREF,
          useValue: "/se"
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorMessageComponent);
    component = fixture.componentInstance;
    component.control = new FormControl({errors: {required: true}, status: '"INVALID"', touched: false});
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should error message not null', fakeAsync(() => {
    fixture.detectChanges();
    // Act
    const result =  fixture.componentInstance.errorMessage;
    // Assert
    expect(result).toBe(null);
    for (const propertyName in component.control.errors) {
      console.log(propertyName);
      expect(component.control.errors.hasOwnProperty(propertyName) && component.control.touched).toBeFalsy();
    }
    expect(ValidationService.getValidatorErrorMessage).toBeTruthy();
  }));

});
