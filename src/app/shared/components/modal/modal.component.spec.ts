import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalComponent } from './modal.component';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ModalActionsService } from '../../_service/modal/modal.service';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import { CustomDirective } from '../../directive/custom.directive';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { APP_BASE_HREF } from '@angular/common';

describe('ModalComponent', () => {
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;
  let mockmodelService;
  let anchorEl: HTMLElement;
  let mockEvent;
  const dialogMock = {
    close: () => { }
    };
  const modalData = {
    name: 'having-paid',
    title: 'What if I have already paid?',
    cancelButtonText: '',
    actionButtonText: 'Got it',
    modalName: 'overview'
  };
  beforeEach(waitForAsync(() => {
    mockmodelService = jasmine.createSpyObj('ModelService',['modalAction']);
    TestBed.configureTestingModule({
      imports: [
        MatDialogModule,
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot()
      ],
      declarations: [ ModalComponent,CustomDirective ],
      providers: [
        {provide: ModalActionsService, useValue: mockmodelService},
        { provide: MAT_DIALOG_DATA, useValue: {modalData: {bodyContent: '<a class="routerLink>a-message</a>'}} },
        { provide: MatDialogRef, useValue: dialogMock },
        {
          provide: APP_BASE_HREF,
          useValue: "/se"
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalComponent);
    component = fixture.componentInstance;
    mockEvent = jasmine.createSpy('event');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call the actionFunction', () => {
    let spy = spyOn(component, 'actionFunction').and.callThrough();
    component.actionFunction();
    fixture.detectChanges();
    expect(spy);
  });
  it('click over elements', waitForAsync(() => {
    const event = new Event('click', { bubbles: true });
    spyOn(event, 'preventDefault');
    component.modalData.actionButtonText='OK'
    fixture.detectChanges();
    const elementDe = fixture.debugElement.query(By.css('.having-paid'));
    if(elementDe){
      elementDe.triggerEventHandler('click', null);
    }
    
    document.dispatchEvent(event)
  }));
  
});
