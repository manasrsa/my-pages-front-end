import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { ModalActionsService } from '../../_service/modal/modal.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  constructor(
    private modalService:ModalActionsService,
    @Inject(MAT_DIALOG_DATA) public modalData:any,
    public dialogRef: MatDialogRef<ModalComponent>,
    private router:Router
  ) { }

  ngOnInit(): void {
  }
   // When the user clicks the action button, the modal calls the service\
  // responsible for executing the action for this modal (depending on\
  // the name passed to `modalData`). After that, it closes the modal
  actionFunction() {
    this.modalService.modalAction(this.modalData);
    this.closeModal();
  }
  // just close the modal
  closeModal() {
    this.dialogRef.close();
  }

}
