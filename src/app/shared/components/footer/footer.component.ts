import { APP_BASE_HREF } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { CommonService } from '../../_service/common/common.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  findAns = "you-will-find-the-answers";
  gotquestion = "gotQuestion";
  countryCode: string = '';
  currentLang: string = 'sv';
  constructor(
    @Inject(APP_BASE_HREF) private baseHref: string,
    private translate: TranslateService,
    private commonService: CommonService,
    private cookieService: CookieService
  ) {
    this.countryCode = this.baseHref.split("/")[1];
    this.currentLang = this.countryCode == 'se' ? 'sv' : 'fi';
    this.commonService.getLanguage.subscribe((res:any) => {
      this.translate.use(res).subscribe(response => {
        this.findAns = this.translate.instant('you-will-find-the-answers');
        this.gotquestion = this.translate.instant("gotQuestion");
      });
    })
  }

  ngOnInit() {
    this.setLocalization();
  }
  // set localization
  setLocalization() {
    this.translate.addLangs(['sv', 'en', 'fi']);
    if (this.translate.getBrowserLang() !== undefined)
    {
      let lang = this.cookieService.get('.AspNetCore.Culture');
      if (lang) {
        var langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.currentLang = langCode;
        this.translate.use(langCode);
      }
      else {
        this.translate.use(this.currentLang);
      }
    }
    else {
      this.translate.use(this.currentLang);
    }
  }
}
