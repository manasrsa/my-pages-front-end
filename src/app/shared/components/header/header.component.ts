import { Component, Inject, Injector, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { interval, throwError } from 'rxjs';
import { CommonService } from '../../_service/common/common.service';
import { MessageService } from '../../_service/messages/messages.service';
import { LogOutService } from '../../_service/logout/logout.service';
import { APP_BASE_HREF } from '@angular/common';
import { ApplicationInsightsService } from '../../_service/app-insights/appInsights-service';
import { ToastsComponent } from '../toasts/toasts.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ProfileModalComponent } from '../profile-modal/profile-modal.component';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  countryCode: any = '';
  toogleMenu = false;
  userName = '';
  /********** language declaration ************/
  toogleLngMenu = false;
  switcherLang = 'SV';
  switcherLngOptions = ['EN', 'SV'];
  // account menu declaration
  toogleAccountMenu = false;
  userAccountOptions = ['Profile', 'LogOut'];
  /** Menu list declaration */
  navigateList = [{ 'name': 'Overview', 'path': 'overview' }, { 'name': 'power-of-attorney', 'path': 'power-of-attorney' }, { 'name': 'contact-us', 'path': 'contact-us' }];
  messageCount = 0;
  currentLang: string = 'sv';
  availableLangs = [{
    name: 'English',
    code: 'en',
    lngCode: 'en-US',
    key: 'c=en-US|uic=en-US'
  }, {
    name: 'Swedish',
    code: 'sv',
    lngCode: 'sv-SE',
    key: 'c=sv-SE|uic=sv-SE'
  }
    , {
    name: 'Finnish',
    code: 'fi',
    lngCode: 'fi-FI',
    key: 'c=fi-FI|uic=fi-FI'
    }];
  activePoa: boolean = false;
  constructor(
    private router: Router,
    public translate: TranslateService,
    private commonService: CommonService,
    private messageService: MessageService,
    private logOutService: LogOutService,
    private snackBar: MatSnackBar,
    private cookieService: CookieService,
    @Inject(APP_BASE_HREF) private baseHref: string,
    private injector: Injector,
    public dialog: MatDialog
  ) {
    this.countryCode = this.baseHref.split("/")[1];
    this.commonService.activePoa.subscribe(res => {
      console.log(res);
      this.activePoa = res;
      if (res) {
        this.navigateList=[{ 'name': 'Overview', 'path': 'overview' },  { 'name': 'contact-us', 'path': 'contact-us' }];
      }
      else {
        this.navigateList = [{ 'name': 'Overview', 'path': 'overview' }, { 'name': 'power-of-attorney', 'path': 'power-of-attorney' }, { 'name': 'contact-us', 'path': 'contact-us' }];
      }
    })
    if (this.countryCode == 'se') {
      this.switcherLngOptions = ['SV', 'FI', 'EN'];
      this.switcherLang = 'SV';


    }
    else {
      this.switcherLngOptions = ['FI', 'SV', 'EN'];
      this.switcherLang = 'FI';
    }
  }

  ngOnInit() {

    // interval(1 * 60 * 1000)
    //   .subscribe(() => {
    //     this.getMessageUnread();
    //   });

    this.getMessageUnread();
    this.debtorDetails();
    // this.setCookie('en');
    let lang = this.cookieService.get('.AspNetCore.Culture');
    if (lang) {
      var langCode = lang.split("|")[0].split("=")[1].split("-")[0];

      //this.translate.use(langCode);

      if (langCode)
      {
        this.translate.use(langCode);
        this.commonService.setLanguage(langCode);
        this.switcherLang = langCode.toUpperCase();
      }
      

    }
    else {
      this.switcherLang = this.countryCode == 'se' ? 'SV' : 'FI';
      var langCode = this.countryCode == 'se' ? 'sv' : 'fi';
      this.translate.use(langCode);
      this.commonService.setLanguage(langCode)
    }

  }

  setCookie(code) {
    this.logOutService.setLanguageCookies(code).subscribe((res: any) => {
      this.debtorDetails();
      this.commonService.setLanguage(this.switcherLang.toLowerCase());
    })
  }
  // debtor details
  debtorDetails() {
    this.commonService.notifySpinnerAction(true);
    this.logOutService.getDebtorDetails().subscribe((response: any) => {
      if (response.result.resultCode === 0) {
        if (response.data.debtors) {
          this.userName = response.data.debtors[0].debtorName;
          sessionStorage.setItem("DebtorDetails",JSON.stringify(response.data.debtors));
          const appInsights = this.injector.get(ApplicationInsightsService);
          appInsights.setUserId(this.userName);
        }
        this.commonService.setErrorMsg('');
      }

      else {
        this.commonService.setErrorMsg(response.result.resultText);
        this.openErrorToast();
      }
      this.commonService.notifySpinnerAction(false);
    }, error => {
        this.commonService.notifySpinnerAction(false);
        let msgerror = this.translate.instant('technicalIssue');
        throwError(msgerror);
        this.commonService.setErrorMsg(msgerror);
      this.openErrorToast();
    })
  }
  // get message unread
  getMessageUnread() {

    this.messageService.unReadMessageCount().subscribe((res: any) => {
      if (res.result.resultCode === 0) {
        this.messageCount = res.data.unReadMsgCount;
      } else {
        //this.commonService.setErrorMsg(res.result.resultText);
        //this.openErrorToast();
        this.commonService.notifySpinnerAction(false);
      }

    }, error => {
        let msgerror = this.translate.instant('technicalIssue');
        throwError(msgerror);
      //this.commonService.setErrorMsg('Some issues for fetching the case list for unread message');
      //this.openErrorToast();
    });
  }
  /**Switch language */
  toogleSwitch() {
    this.toogleLngMenu = !this.toogleLngMenu;
  }
  /** toogle class add or remove for menu  */
  toogleClass() {
    this.toogleMenu = !this.toogleMenu;
  }
  /** Switch language */
  switchLanguage(lang) {
    this.switcherLang = lang;
    this.translate.use(lang.toLowerCase());
    this.commonService.setLanguage(lang.toLowerCase());


    this.availableLangs.map((item, val) => {
      if (lang.toLowerCase() === item.code) {
        // this.setCookie(item.lngCode);
        this.setCookie(item.lngCode);
        //this.currentLang = item.code;
      }
    })


  }
  // switch account menu
  toggleAccountOption() {
    this.toogleAccountMenu = !this.toogleAccountMenu;
  }

  //selectedAccountOption
  selectedAccountOption(selectedOption) {
    console.log(selectedOption);
    if (selectedOption == "LogOut") {
      this.logOut();
    } else {
      // this.router.navigate(['../profile']);
    }
  }
  /** log out  */
  logOut() {
    //this.logOutService.logout().subscribe((res: any) => {

    //  if (res.result.resultCode === 0) {
    //    window.open(this.countryCode + '/Login/BankID', '_self')
    //  }
    //});
    window.open(this.countryCode + '/Logout/SignOut', '_self')
  }
  //open profile modal 
  openProfileModal() {
    const dialogRef = this.dialog.open(ProfileModalComponent, {
      height: 'auto',
      width: '900px',
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
  //open error Toast Component
  openErrorToast() {
    this.snackBar.openFromComponent(ToastsComponent
      , {
        panelClass: 'error',
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
  }
}
