/* tslint:disable:no-unused-variable */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, Directive, Input, HostListener } from '@angular/core';

import { HeaderComponent } from './header.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { APP_BASE_HREF, CommonModule, Location } from '@angular/common';
import { Router } from '@angular/router';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpLoaderFactory } from 'src/app/views/overview/overview.module';

// mock Router link Directive Stub;
@Directive({
  selector: '[routerLink]'
})
export class RouterLinkDirectiveStub {
  @Input('routerLink') linkParams: any;
  navigatedTo: any = null;

  @HostListener('click')
  onClick() {
    this.navigatedTo = this.linkParams;
  }
}
describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let linkDes;
  let routerLinks;
  let tick: (milliseconds: number) => void;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderComponent, RouterLinkDirectiveStub ],
      imports: [
        CommonModule,
        RouterTestingModule,
        HttpClientModule,
        HttpClientTestingModule,
        TranslateModule.forRoot()
      ],
      providers:[
        {
          provide: APP_BASE_HREF,
          useValue: "/fi"
        }
      ]
    })
    .compileComponents();

  }));

  beforeEach(() => {

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    linkDes = fixture.debugElement
    .queryAll(By.directive(RouterLinkDirectiveStub));
    
   component.countryCode='fi';
    routerLinks = linkDes.map(de => de.injector.get(RouterLinkDirectiveStub));
    fixture.detectChanges();
  });
  
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have call the toogleClass for toogleLngMenu', () => {
    component.toogleClass();
    fixture.detectChanges();
    expect(component.toogleMenu).toBe(true);
  });
  it('should have call the toogleSwitch when click the language switcher icon', () => {
    const dropwonDE = fixture.debugElement.query(By.css('.dropwon'));
    dropwonDE.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.toogleLngMenu).toBe(true);
  });
  it('should have selected for switch language sv', () => {
    const dropdownmenuDe = fixture.debugElement.queryAll(By.css('.dropdown-menu li'));
    dropdownmenuDe[1].triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(fixture.componentInstance.switcherLang).toEqual('SV');
  });

  it('should have call the logout button', waitForAsync(() => {
    spyOn(component, 'logOut');

    const logoutDE = fixture.debugElement.queryAll(By.css('.logout'));
    logoutDE[0].triggerEventHandler('click', null);

    fixture.whenStable().then(() => {
    expect(component.logOut).toHaveBeenCalled();
   });
   }));
  it('should call the logout methid', waitForAsync(() => {
     spyOn(component, 'logOut');
     component.logOut();
     expect(component.logOut).toHaveBeenCalledTimes(1);
   }));
  it('should have render user name ', () => {
    fixture.detectChanges();
    const userNameDe = fixture.debugElement.query(By.css('.username'));
   // expect(userNameDe.nativeElement.textContent).toContain('Anna Christina Olsson', 'when call string interpolation user');
    //expect(component.userName).toBe('Anna Christina Olsson');
   });
  it('should have render the li when desktop', () => {
    fixture.detectChanges();
    const listDE = fixture.debugElement.queryAll(By.css('.d-region li'));
    expect(listDE.length).toEqual(3);
   });
  it('should have render the li when mobile', () => {
    fixture.detectChanges();
    const listDE = fixture.debugElement.queryAll(By.css('.m-region li'));
    expect(listDE.length).toEqual(4);
   });
});
