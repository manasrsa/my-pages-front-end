import { Component, Inject, Injector, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { APP_BASE_HREF } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationService } from 'src/app/shared/_service/validation/validation.service';
import { CookieService } from 'ngx-cookie-service';
import { NotificationChannelService } from 'src/app/shared/_service/profile/notification-channel.service';
import { throwError } from 'rxjs';
import { CommonService } from 'src/app/shared/_service/common/common.service';
import { ToastsComponent } from 'src/app/shared/components/toasts/toasts.component';
import { LogOutService } from 'src/app/shared/_service/logout/logout.service';

@Component({
  selector: 'user-profile',
  templateUrl: './profile-modal.component.html',
  styleUrls: ['./profile-modal.component.scss']
})
export class ProfileModalComponent implements OnInit {

  fullName = '';
  debtors:any;
  public notificationChannels = [
    {key:0,value:"None"},
    {key:1,value:"SMS"},
    {key:2,value:"Email"},
    {key:3,value:"Both"}
  ];
  public showEmail = false;
  public showPhone = false;

  notificationChannelForm: FormGroup;

  currentLang:string='';
  currency = 'kr'; // '€'
  conuntryCode = 'se';

  debtorDetail = [];


  constructor(
    private formBuilder: FormBuilder,
    public translate: TranslateService,
    @Inject(APP_BASE_HREF) private baseHref: string,
    public dialog: MatDialog,
    private cookieService:CookieService,
    private commonService: CommonService,
    private notificationChannelService:NotificationChannelService,
    private logOutService: LogOutService,
    private snackBar:MatSnackBar
  ) {
      this.conuntryCode=this.baseHref.split("/")[1];
      this.currentLang=this.conuntryCode=='se' ? 'sv' : 'fi';
      this.currency = this.conuntryCode == 'se' ? 'kr' : '€';
   }

  ngOnInit() {
    this.setLocalization();
    this.fullName = "Full Name";

    this.notificationChannelForm = this.formBuilder.group({
      notificationsDropdown: [''],
      phone: [''],
      email: ['']
    }
    );
    this.debtorDetails();
  }

  // set localization
  setLocalization() {
    this.translate.addLangs(['sv', 'en', 'fi']);
    if (this.translate.getBrowserLang() !== undefined) {
      let lang = this.cookieService.get('.AspNetCore.Culture');
      console.log(lang);
      if (lang) {
        var langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.currentLang=langCode;
        this.translate.use(langCode);
      }else {
        this.translate.use(this.currentLang);
      }
    }
    else {
      this.translate.use(this.currentLang);
    }
  }

  notificationChannelChanged(value,debtors) {
    if (value == '0') {
      this.showEmail = false;
      this.showPhone = false;
      this.notificationChannelForm.controls['phone'].setValidators([]);
      this.notificationChannelForm.controls['phone'].updateValueAndValidity();
      this.notificationChannelForm.controls['email'].setValidators([]);
      this.notificationChannelForm.controls['email'].updateValueAndValidity();
    }
    if (value == '1') {
      this.showEmail = false;
      this.showPhone = true;
      this.notificationChannelForm.controls['phone'].setValue(debtors[0].conversationNotificationCellularPhone);
      this.notificationChannelForm.controls['phone'].setValidators([Validators.required,ValidationService.getNotificationMobileValidate]);
      this.notificationChannelForm.controls['phone'].updateValueAndValidity();
      this.notificationChannelForm.controls['email'].setValidators([]);
      this.notificationChannelForm.controls['email'].updateValueAndValidity();
    }
    if (value == '2') {
      this.showEmail = true;
      this.showPhone = false;
      this.notificationChannelForm.controls['email'].setValue(debtors[0].conversationNotificationEmail);
      this.notificationChannelForm.controls['email'].setValidators([Validators.required,ValidationService.getEmailValidate]);
      this.notificationChannelForm.controls['email'].updateValueAndValidity();
      this.notificationChannelForm.controls['phone'].setValidators([]);
      this.notificationChannelForm.controls['phone'].updateValueAndValidity();
    }
    if (value == '3') {
      this.showEmail = true;
      this.showPhone = true;
      this.notificationChannelForm.controls['email'].setValue(debtors[0].conversationNotificationEmail);
      this.notificationChannelForm.controls['phone'].setValue(debtors[0].conversationNotificationCellularPhone);
      this.notificationChannelForm.controls['phone'].setValidators([Validators.required,ValidationService.getMobileValidate]);
      this.notificationChannelForm.controls['phone'].updateValueAndValidity();
      this.notificationChannelForm.controls['email'].setValidators([Validators.required,ValidationService.getEmailValidate]);
      this.notificationChannelForm.controls['email'].updateValueAndValidity();
    }
  }

  //open error Toast Component
  openErrorToast()
  {
    this.snackBar.openFromComponent(ToastsComponent
      , {
      panelClass: 'error',
      horizontalPosition:  'center',
      verticalPosition: 'top',
      });
  }

  //open success Toast Component
  openSuccessToast()
  {
    this.snackBar.openFromComponent(ToastsComponent
      , {
      panelClass: 'success',
      horizontalPosition:  'center',
      verticalPosition: 'top',
      });
  }

  //Debtor details
  debtorDetails() {
    this.debtorDetail = JSON.parse(sessionStorage.getItem("DebtorDetails"));
    // this.commonService.notifySpinnerAction(true);
    console.log(this.debtorDetail);
        if (this.debtorDetail) {
          this.debtors = this.debtorDetail;
          this.notificationChannelForm.controls['notificationsDropdown'].setValue(this.debtorDetail[0].conversationNotificationChannel);
          this.notificationChannelChanged(this.debtorDetail[0].conversationNotificationChannel,this.debtors);
          if(this.debtorDetail[0].conversationNotificationChannel == 1){
            this.notificationChannelForm.controls['phone'].setValidators([Validators.required,ValidationService.getMobileValidate]);
            this.notificationChannelForm.controls['phone'].updateValueAndValidity();
          }
          if(this.debtorDetail[0].conversationNotificationChannel == 2){
            this.notificationChannelForm.controls['email'].setValidators([Validators.required,ValidationService.getEmailValidate]);
            this.notificationChannelForm.controls['email'].updateValueAndValidity();
          }
          if(this.debtorDetail[0].conversationNotificationChannel == 3){
            this.notificationChannelForm.controls['phone'].setValidators([Validators.required,ValidationService.getMobileValidate]);
            this.notificationChannelForm.controls['phone'].updateValueAndValidity();
            this.notificationChannelForm.controls['email'].setValidators([Validators.required,ValidationService.getEmailValidate]);
            this.notificationChannelForm.controls['email'].updateValueAndValidity();
          }
        }else{
          this.getDebtorDetails();
        }
      }

  // debtor details
  getDebtorDetails() {
    this.commonService.notifySpinnerAction(true);
    this.logOutService.getDebtorDetails().subscribe((response: any) => {
      if (response.result.resultCode === 0) {
        if (response.data.debtors) {
          this.debtors = response.data.debtors;
          this.commonService.setCaseDetails(response.data.debtors); 
          sessionStorage.setItem("DebtorDetails",JSON.stringify(response.data.debtors));
          this.notificationChannelForm.controls['notificationsDropdown'].setValue(response.data.debtors[0].conversationNotificationChannel);
          this.notificationChannelChanged(response.data.debtors[0].conversationNotificationChannel,this.debtors);
          if(response.data.debtors[0].conversationNotificationChannel == 1){
            this.notificationChannelForm.controls['phone'].setValidators([Validators.required,ValidationService.getMobileValidate]);
            this.notificationChannelForm.controls['phone'].updateValueAndValidity();
          }
          if(response.data.debtors[0].conversationNotificationChannel == 2){
            this.notificationChannelForm.controls['email'].setValidators([Validators.required,ValidationService.getEmailValidate]);
            this.notificationChannelForm.controls['email'].updateValueAndValidity();
          }
          if(response.data.debtors[0].conversationNotificationChannel == 3){
            this.notificationChannelForm.controls['phone'].setValidators([Validators.required,ValidationService.getMobileValidate]);
            this.notificationChannelForm.controls['phone'].updateValueAndValidity();
            this.notificationChannelForm.controls['email'].setValidators([Validators.required,ValidationService.getEmailValidate]);
            this.notificationChannelForm.controls['email'].updateValueAndValidity();
          }
        }
        this.commonService.setErrorMsg('');
      }

      else {
        this.commonService.setErrorMsg(response.result.resultText);
        this.openErrorToast();
      }
      this.commonService.notifySpinnerAction(false);
    }, error => {
      this.commonService.notifySpinnerAction(false);
      throwError('Some issues for fetching the debtor name');
      this.commonService.setErrorMsg('Some issues for fetching the debtor Details');
      this.openErrorToast();
    })
  }

  submitNotice(){
    this.commonService.notifySpinnerAction(true);
    let requestPayload;
    if(this.notificationChannelForm.get('notificationsDropdown').value == 3){
      requestPayload={
        "NotificationChannel":3,
        "CellularPhone":this.notificationChannelForm.get('phone').value,
        "Email":this.notificationChannelForm.get('email').value
      }
    }else if (this.notificationChannelForm.get('notificationsDropdown').value == 1) {
      requestPayload={
        "NotificationChannel":1,
        "CellularPhone":this.notificationChannelForm.get('phone').value

      }
    }else if (this.notificationChannelForm.get('notificationsDropdown').value == 2) {
      requestPayload={
        "NotificationChannel":2,
        "Email":this.notificationChannelForm.get('email').value
      }
    }else {
      requestPayload={
        "NotificationChannel":0
      }
    }

    this.notificationChannelService.PostNotice(requestPayload).subscribe((res: any) => {
      this.commonService.notifySpinnerAction(false);
      if (res.result.resultCode == 0) {
        sessionStorage.removeItem("DebtorDetails");
        this.getDebtorDetails();
        this.commonService.setSuccessMsg("submitted successfully");
        this.openSuccessToast();
        // this.dialog.closeAll();
      }
      else{
        this.commonService.setErrorMsg(res.result.resultText);
        this.openErrorToast();
      } 

    }, error => {
      this.commonService.notifySpinnerAction(false);
      this.commonService.setErrorMsg('Some issues for Submit');
      this.openErrorToast();
      throwError('Some error occured while submitting');
    });
    this.dialog.closeAll();
  }

}
