import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BreadrumService } from '../../_service/breadcrum/breadcrum.service';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  public breadcrumbs: any = [];

  constructor(
    public breadCrumService: BreadrumService
  ) {
      // get breadcrum data
    this.breadCrumService.getBreadcrum().subscribe(data => {

      if (data.result.length > 1) {
        this.breadcrumbs =this.getBreadCrum(data.result);
        this.breadcrumbs.reverse();
       } else {
        this.breadcrumbs = [];

      }

    });
  }
  getBreadCrum(result){
    result.reverse().map((obj,index)=>{
      if(index!=0){
        result[index].url= result[index-1].url+'/'+(result[index].url.replace(/^\s+|\s+$/gm,''));
      }
    });
    return result
  }
  ngOnInit() {
   

  }


}
export interface IBreadCrumb {
    label: string;
    url: string;
}
