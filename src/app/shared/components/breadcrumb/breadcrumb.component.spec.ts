import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BreadcrumbComponent } from './breadcrumb.component';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { APP_BASE_HREF } from '@angular/common';

describe('BreadcrumbComponent', () => {
  let component: BreadcrumbComponent;
  let fixture: ComponentFixture<BreadcrumbComponent>;
  let mockBreadcrumService;
  beforeEach(waitForAsync(() => {
    mockBreadcrumService=jasmine.createSpyObj('BreadcrumService',['setBreadcrum','getBreadcrum']);
    TestBed.configureTestingModule({
      imports:[RouterTestingModule],
      declarations: [ BreadcrumbComponent ],
      providers:[
        {
          provide: APP_BASE_HREF,
          useValue: "/se"
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreadcrumbComponent);
    component = fixture.componentInstance;
    mockBreadcrumService.getBreadcrum.and.returnValue([{'title':"Secure messages","breadcrumb":"New message",'url':"secure-messages","urlSegments":[{"path":"secure-messages","parameters":{}}],"params":{}},{"title":"Messages","breadcrumb":"Messages","url":"messages","urlSegments":[{"path":"messages","parameters":{}}],"params":{}}]);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call the breadcrumservice and get breadcrumbs result',()=>{
    mockBreadcrumService.getBreadcrum.and.returnValue([{"title":"Secure messages","breadcrumb":"New message","url":"secure-messages","urlSegments":[{"path":"secure-messages","parameters":{}}],"params":{}},{"title":"Messages","breadcrumb":"Messages","url":"messages","urlSegments":[{"path":"messages","parameters":{}}],"params":{}}]);
    component.breadCrumService.setBreadcrum([{"title":"Secure messages","breadcrumb":"New message","url":"secure-messages","urlSegments":[{"path":"secure-messages","parameters":{}}],"params":{}},{"title":"Messages","breadcrumb":"Messages","url":"messages","urlSegments":[{"path":"messages","parameters":{}}],"params":{}}])
    fixture.detectChanges()
    expect(component.breadcrumbs.length).toEqual(2);
   
  });
  it('should render the breadcrum a element',()=>{
    component.breadCrumService.setBreadcrum([{"title":"Secure messages","breadcrumb":"New message","url":"secure-messages","urlSegments":[{"path":"secure-messages","parameters":{}}],"params":{}},{"title":"Messages","breadcrumb":"Messages","url":"messages","urlSegments":[{"path":"messages","parameters":{}}],"params":{}}])
    fixture.detectChanges()
    let anchorDE=fixture.debugElement.queryAll(By.css('.breadcrumb'));
    expect(anchorDE.length).toEqual(2);
  })
  it('should call the breadcrumservice and breadcrumbs value null',()=>{
    component.breadCrumService.setBreadcrum([{"title":"Secure messages","breadcrumb":"New message","url":"secure-messages","urlSegments":[{"path":"secure-messages","parameters":{}}],"params":{}}]);
    fixture.detectChanges()
    expect(component.breadcrumbs.length).toEqual(0);
  })
});
