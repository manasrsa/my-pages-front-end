import { APP_BASE_HREF } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { Subject, Subscription, throwError, timer } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { CommonService } from '../../_service/common/common.service';
import { LogOutService } from '../../_service/logout/logout.service';
import { ModalComponent } from '../modal/modal.component';
import { ToastsComponent } from '../toasts/toasts.component';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, OnDestroy {
  minutesDisplay = 0;
  secondsDisplay = 0;

  endTime = 15;

  unsubscribe$: Subject<void> = new Subject();
  timerSubscription: Subscription;
  countryCode:any='';
  currentLang:string='';
  constructor(
    private commonService: CommonService,
    private matDialog: MatDialog,
    private logoutService: LogOutService,
    private translate:TranslateService,
    private snackBar:MatSnackBar,
    private cookieService:CookieService,
    @Inject(APP_BASE_HREF) private baseHref: string
  ) {
    this.countryCode = this.baseHref.split("/")[1];
    this.currentLang=this.countryCode=='se' ? 'sv' : 'fi';
     // get localization
     this.commonService.getLanguage.subscribe((data:any) => {
      this.translate.use(data);
    });
   }

  ngOnInit() {
    this.resetTimer();
    this.commonService.userActionOccured.pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe(() => {
      if (this.timerSubscription) {
        this.timerSubscription.unsubscribe();
      }
      this.resetTimer();
    });
    this.setLocalization();
    
  }
   // set localization
   setLocalization() {
    this.translate.addLangs(['sv', 'en', 'fi']);
    if (this.translate.getBrowserLang() !== undefined) {
      let lang = this.cookieService.get('.AspNetCore.Culture');

      if (lang) {
        var langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.currentLang=langCode;
        this.translate.use(langCode);
      }else {
        this.translate.use(this.currentLang);
      }
    }
    else {
      this.translate.use(this.currentLang);
    }
  }
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  resetTimer(endTime: number = this.endTime) {
    const interval = 1000;
    const duration = endTime * 60;
    this.timerSubscription = timer(0, interval).pipe(
      take(duration)
    ).subscribe(value =>
      this.render((duration - +value) * interval),
      err => { },
      () => {
      this.autoLogOut();
      this.resetTimeTwoMinute(2);
      }
    );
  }
  resetTimeTwoMinute(endTime: number) {
    const interval = 1000;
    const duration = endTime * 60;
    this.timerSubscription = timer(0, interval).pipe(
      take(duration)
    ).subscribe(value =>
      this.render((duration - +value) * interval),
      err => { },
      () => {
        window.open(this.countryCode + '/Logout/SignOut', '_self')
       // window.open(this.countryCode+'/Login/BankID', '_self');
      //  this.logoutService.logout().subscribe((res: any) => {
      //    if (res.result.resultCode === 0) {
      //      window.open(this.countryCode+'/Login/BankID', '_self');
      //      this.commonService.setErrorMsg('');
      //     } else {
      //        this.commonService.setErrorMsg(res.result.resultText);
      //        this.openErrorToast();
      //      }
      //}, error => {
      //    this.commonService.setErrorMsg('Some issues for fetching the logout');
      //    this.openErrorToast();
      //    throwError('Some issues for fetching the logout');
      //});
      }
    );
  }
  private render(count) {
    this.secondsDisplay = this.getSeconds(count);
    this.minutesDisplay = this.getMinutes(count);
  }

  private getSeconds(ticks: number) {
    const seconds = ((ticks % 60000) / 1000).toFixed(0);
    return this.pad(seconds);
  }

  private getMinutes(ticks: number) {
    const minutes = Math.floor(ticks / 60000);
    return this.pad(minutes);
  }

  private pad(digit: any) {
    return digit <= 9 ? '0' + digit : digit;
  }
  //open error Toast Component
  openErrorToast()
  {
    this.snackBar.openFromComponent(ToastsComponent
			, {
			panelClass: 'error',
			horizontalPosition:  'center',
			verticalPosition: 'top',
			});
  }
  autoLogOut() {
    const modalName = 'logout';
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.id = 'modal-component';
    dialogConfig.height = 'auto';
    dialogConfig.width = '600px';
    dialogConfig.data = {
      name: 'logout',
      // title: 'Are you sure you want to revoke this power of attorney?',
      bodyContent: '',
      cancelButtonText: '',
      actionButtonText: '',
      modalName
    };
    dialogConfig.data.actionButtonText = this.translate.instant('Yes');
    dialogConfig.data.cancelButtonText = this.translate.instant('No');
    dialogConfig.data.bodyContent = '<h3>'+this.translate.instant('site-alert')+'</h3><p>'+this.translate.instant('siteAlertReset')+'</p>';
    const modalDialog = this.matDialog.open(ModalComponent, dialogConfig);
  }
}
