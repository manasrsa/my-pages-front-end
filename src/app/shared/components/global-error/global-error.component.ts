import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../_service/common/common.service';

@Component({
  selector: 'app-global-error',
  templateUrl: './global-error.component.html',
  styleUrls: ['./global-error.component.scss']
})
export class GlobalErrorComponent implements OnInit {
  errorMessage:string='';
  constructor(private commonService:CommonService) { 

    this.commonService.getErrorMsg.subscribe(res=>{
      this.errorMessage=res;
    })
  }

  ngOnInit(): void {
  }
  // getMassage(msg:string){
  //   this.errorMessage=msg;
  // }
}
