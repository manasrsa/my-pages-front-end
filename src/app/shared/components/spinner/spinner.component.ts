import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../_service/common/common.service';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {
  loading = false;
  constructor(
    private commonService: CommonService
  ) {

    this.commonService.getNotifySpinnerAction().subscribe((res: any) => {
      this.loading = res;
      
    });
  }

  ngOnInit() {
  }

}
