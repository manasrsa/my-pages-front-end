import { Component, OnInit, Input } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CommonService } from '../../_service/common/common.service';

@Component({
  selector: 'app-toasts',
  templateUrl: './toasts.component.html',
  styleUrls: ['./toasts.component.scss'],
  // providers:[ToastsComponent]
})
export class ToastsComponent implements OnInit {
  toasts: any= [];
  errorMessage: string = "";
  successMessage: string = "";
  constructor(private commonService: CommonService,
    private snackBar: MatSnackBar) {
    this.commonService.getErrorMsg.subscribe(res => {
      this.errorMessage = res;
      console.log(this.errorMessage);
    })

    this.commonService.getSuccessMsg.subscribe(res => {
      // this.successMessage = res;
      if(this.errorMessage == ''){
        this.successMessage = res;
      }else{
        this.successMessage = '';
      }
    })
  }

  ngOnInit() {
   
    
  }
  closeToast() {
    this.errorMessage = '';
    this.successMessage = '';
    this.snackBar.dismiss();
  }

}
