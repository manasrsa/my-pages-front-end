import { Injectable } from '@angular/core';
import { ConfigSetting } from '../../modal/config';
import { HttpClientService } from '../httpclient/http-client.service';

@Injectable({
    providedIn: 'root'
})
export class NotificationChannelService {
    baseUrl = '';
    constructor(
       private httpClient: HttpClientService,
       private configSetting: ConfigSetting
       ) {
       this.baseUrl = this.configSetting.apiUrl;
     }
     // Post Notice
     PostNotice(data){
        const url = this.baseUrl + 'Utility/ConversationNotification';
        return this.httpClient.post(url, data);
     }
     
    }
