import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { ConfigSetting } from '../../modal/config';
import { HttpClientService } from '../httpclient/http-client.service';

@Injectable({
   providedIn: 'root'
})
export class LogOutService {
    baseUrl = ''; // "../../assets/data/messages.json"; //'../../assets/data/messages.json';
    constructor(
        private httpClient: HttpClientService,
        private configSetting: ConfigSetting
        ) {
        this.baseUrl = this.configSetting.apiUrl;
    }
    logout(){
        const url = this.baseUrl + 'Utility/Logout';
        return this.httpClient.get(url);
    }
    //https://localhost:5050/se/api/Utility/SetLanguageCookies/se
    setLanguageCookies(code){
        let url = this.baseUrl +'Utility/SetLanguageCookies/'+code
        return this.httpClient.get(url);
    }
    //https://localhost:5050/se/api/Utility/DebtorDetails
    getDebtorDetails(){
        const url = this.baseUrl + 'Utility/DebtorDetails';
        return this.httpClient.get(url);
    }
}
