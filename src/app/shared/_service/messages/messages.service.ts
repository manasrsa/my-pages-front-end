import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { ConfigSetting } from '../../modal/config';
import { HttpClientService } from '../httpclient/http-client.service';

@Injectable({
   providedIn: 'root'
})
export class MessageService {
    baseUrl = ''; // "../../assets/data/messages.json"; //'../../assets/data/messages.json';
    constructor(
        private httpClient: HttpClientService,
        private configSetting: ConfigSetting
        ) {
        this.baseUrl = this.configSetting.apiUrl;
    }
    // Message/GetCaseListForMessage
    getCaseListForMessage() {
        const url = this.baseUrl + 'Message/GetCaseListForMessage';
        return this.httpClient.get(url);
    }
    // https://localhost:5050/se/api/Message/PostMessage
    createMessage(data) {
        const url = this.baseUrl + 'Message/PostMessage';
        return this.httpClient.post(url, data);
    }
    // : https://localhost:5050/se/api/Message/GetMessages/ConvergGuid =06564313-D326-4177-8E81-060896A2E74B
    conversionMessages(guid) {
        const url = this.baseUrl + 'Message/GetMessages/' + guid;
        return this.httpClient.get(url);
    }
    // https://localhost:5050/se/api/Message/GetCollectionConversations
    getCollectionConversations() {
        const url = this.baseUrl + 'Message/GetCollectionConversations';
        return this.httpClient.get(url);
    }
    //https://localhost:5050/se/api/Message/UnReadMessagesCount
    unReadMessageCount(){
        const url = this.baseUrl + 'Message/UnReadMessagesCount';
        return this.httpClient.get(url);
    }
}
