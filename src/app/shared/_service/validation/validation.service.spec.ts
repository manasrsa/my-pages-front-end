/* tslint:disable:no-unused-variable */

import { TestBed, inject, waitForAsync } from '@angular/core/testing';
import { ValidationService } from './validation.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { FormControl, FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
const mockData = {
  firstName: {
      pattern: '^[^0-9]+$'
  },
  lastName: {
      pattern: '^[^0-9]+$'
  },
  ssn: {
      se: {
          pattern: '^(19|20)?\\d{8}([-]|\\s)\\d{4}$',
          datepattern: '^(?!0000)(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])$'
      },
      en: {
          pattern: '^(?!0000)[0-9]{6}[-A][0-9]{3}[0-9A-Z]{1}$',
          datepattern: '^(?!0000)(0[1-9]|[1-2][0-9]|3[0-1])(0[1-9]|1[0-2])$'
      }

  },
  zipCode: {
      pattern: '^(?!0+$)[0-9]{5}$'
  },
  phone: {
      pattern: '^[+]?[0-9- ]*$'
  },
  email: {
      pattern: '^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'
  },
  mobile: {
      pattern: '^[+]?[0-9- ]*$'
  },
  config: {
      required: 'Please fill this field.',
      invalidFirstName: 'Numbers are not allowed.',
      invalidLastName: 'Numbers are not allowed.',
      invalidSsnFormat: 'Format should be YYYYMMDD-NNNN.',
      invalidSsnDate: 'Please enter a valid SSN.',
      invalidSsnDebtor: 'Receiver of power of attorney can\'t have the same SSN as debtor',
      invalidPoa_ssn_list: 'There already is a power of attorney for this SSN',
      invalidSocialSecurity: 'Please enter social security number in the format DDMMYY-NNNN.',
      invalidZipCode: 'Format should be 01234',
      invalidMobile: 'Please enter a valid phone number',
      invalueWhitespace: 'Please enter valid input',
      invalidPhone: 'Please enter a valid phone number',
      invalidEmail: 'The email is not in valid format.'

  }
};
describe('Service: Validation', () => {
  let service: ValidationService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ValidationService,{
        provide: APP_BASE_HREF,
        useValue: "/se"
      },
      HttpClientModule
    ]
    });
    service = TestBed.get(ValidationService);
    httpMock = TestBed.get(HttpTestingController);
    ValidationService.validFile = mockData;
    // form=new FormBuilder()
  });

  it('should ...', inject([ValidationService], (service: ValidationService) => {
    expect(service).toBeTruthy();
  }));
  it('should call the getValidation ', () => {
    service.fetchViaHttp().subscribe(data => {
      ValidationService.validFile = data; // .json();
      expect(ValidationService.validFile).toBe('config');
    });

    const req = httpMock.expectOne(service.baseUrl);
    expect(req.request.method).toBe('GET');
  });
  it('should call the getValidation', () => {
    // service.fetchViaHttp.and.returnValue()
   const spy = spyOn(service, 'getValidation').and.callThrough();
   expect(spy);
    // service.getValidation();
  });
  it('should call the getValidatorErrorMessage method', () => {
   // ValidationService.validFile=mockData;
    ValidationService.getValidatorErrorMessage('firstName');
    expect(ValidationService.getValidatorErrorMessage).toBeTruthy();
    expect(ValidationService.validFile.config).toEqual(mockData.config);
  });
  describe('getFirstNameValidator', () => {
    const control = new FormControl('input');
    it('should return null if input string  is alphabet', () => {
      control.setValue('mukesh');
      expect(ValidationService.getFirstNameValidator(control)).toBeNull();
    });
    it('should return in value Whitespace if input string only space', () => {
      control.setValue('  ');
      expect(ValidationService.getFirstNameValidator(control)).toEqual({invalueWhitespace: true });
    });
    it('should return invalid FirstName if input string is wrong data', () => {
      control.setValue('12381238');
      expect(ValidationService.getFirstNameValidator(control)).toEqual({invalidFirstName: true });
    });
  });
  describe('getLastNameValidator', () => {
    const control = new FormControl('input');
    it('should return null if input string  is alphabet', () => {
      control.setValue('singh');
      expect(ValidationService.getLastNameValidator(control)).toBeNull();
    });
    it('should return in value Whitespace if input string only space', () => {
      control.setValue('  ');
      expect(ValidationService.getLastNameValidator(control)).toEqual({invalueWhitespace: true });
    });
    it('should return invalid LastName if input string is wrong data', () => {
      control.setValue('12381238');
      expect(ValidationService.getLastNameValidator(control)).toEqual({invalidLastName: true });
    });
  });
  describe('getSssnValidator', () => {
    const control = new FormControl('input');
    // it('should return null if input string  is correct', () => {
    //   control.setValue('1983062321');
    //   expect(ValidationService.getSssnValidator(control)).toBeNull();
    // });
    it('should return in value Whitespace if input string only space', () => {
      control.setValue('  ');
      expect(ValidationService.getSssnValidator(control)).toEqual({invalueWhitespace: true });
    });
    it('should return invalid FirstName if input string is wrong data', () => {
      control.setValue('12381238');
      expect(ValidationService.getSssnValidator(control)).toEqual({invalidSsnFormat: true });
    });
  });
  describe('getCityValidate', () => {
    const control = new FormControl('input');
    it('should return null if input string  is correct', () => {
        control.setValue('1983062321');
        expect(ValidationService.getCityValidate(control)).toBeNull();
      });
    it('should return null if input string  is space only', () => {
        control.setValue('  ');
        expect(ValidationService.getCityValidate(control)).toEqual({invalueWhitespace: true });
      });
  });
  describe('getZipCodeValidate', () => {
    const control = new FormControl('input');
    it('should return null if input string  is correct', () => {
        control.setValue('01234');
        expect(ValidationService.getZipCodeValidate(control)).toBeNull();
      });
    it('should return null if input string  is space only', () => {
        control.setValue('   ');
        expect(ValidationService.getZipCodeValidate(control)).toEqual({invalueWhitespace: true });
      });
    it('should return null if input string  is invalid data', () => {
        control.setValue('1983062321');
        expect(ValidationService.getZipCodeValidate(control)).toEqual({invalidZipCode: true });
      });
  });
  describe('getPhoneValidate', () => {
    const control = new FormControl('input');
    it('should return null if input string  is correct', () => {
        control.setValue('+01234123');
        expect(ValidationService.getPhoneValidate(control)).toBeNull();
      });
    // it('should return null if input string  is space only', () => {
    //     control.setValue('   ');
    //     expect(ValidationService.getPhoneValidate(control)).toEqual({invalueWhitespace: true });
    //   });
    it('should return null if input string  is invalid data', () => {
        control.setValue('ajsdh');
        expect(ValidationService.getPhoneValidate(control)).toEqual({invalidPhone: true });
      });
  });
    
});
