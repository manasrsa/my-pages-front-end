import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl, AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';
import { CommonService } from '../common/common.service';
import { APP_BASE_HREF, ɵNullViewportScroller } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {
  public static  countryCode = 'se';
  public static validFile: any = '';
  // tslint:disable-next-line: quotemark
  baseUrl: any = "../../assets/data/validation.json";
  public static activePowerOfAttorney=[];  
 public static totalDebt='';
  constructor(private http: HttpClient,
              private commonService: CommonService,
              @Inject(APP_BASE_HREF) private baseHref: string
              ) {
                ValidationService.countryCode=this.baseHref.split("/")[1];
              
                this.commonService.getActivePowerOfAttorney.subscribe((data: any)=>{
                 console.log(data);
                  ValidationService.activePowerOfAttorney = data;

              })
              this.commonService.getTotalDebt.subscribe(res=>{
                ValidationService.totalDebt = res;
              })
    }
  // tslint:disable-next-line: member-ordering
  public static getValidatorErrorMessage(
    validatorName: string,
  ) {
    const config = ValidationService.validFile.config;
    return config[validatorName];
  }
  // tslint:disable-next-line: member-ordering
  public static getFirstNameValidator(control) {
    const regEx = new RegExp(ValidationService.validFile.firstName.pattern);
    const isWhitespace = (control.value || '').trim().length === 0;
    if (isWhitespace) {
      return {
        invalueWhitespace: true
      };
      } else if (regEx.test(control.value)) {
        return null; // Promise.resolve(null);
      } else {
      return {'invalidFirstName': true};

    }
  }
  // tslint:disable-next-line: member-ordering
  public static getLastNameValidator(control) {
    const regEx = new RegExp(ValidationService.validFile.firstName.pattern);
    const isWhitespace = (control.value || '').trim().length === 0;
    if (isWhitespace) {
      return {
        invalueWhitespace: true
      };
      } else if (regEx.test(control.value)) {
      return null;
    } else {
      return {
        invalidLastName: true
      };
    }
  }
  // tslint:disable-next-line: member-ordering
  public static getSssnValidator(control) {
    let ssnday =  control.value ? control.value.split('-') : '';
    
    
    console.log(ValidationService.activePowerOfAttorney);
    if (ValidationService.countryCode == 'se') {
      let ssn=ValidationService.activePowerOfAttorney.length>0 ? ValidationService.activePowerOfAttorney.filter(power=>power.powerOfAttorney.socialSecurityNumber.replace(/-/g,'')==(control.value.substr(2) || '').replace(/-/g,'')) : [];
      const ssnRegEx = new RegExp(ValidationService.validFile.ssn.se.pattern);
      const ssnDateRegEx = new RegExp(ValidationService.validFile.ssn.se.datepattern);
      const currentdate = (new Date()).toISOString().slice(0, 10).replace(/-/g, '');
      const pastdate = (new Date(1910, 1)).toISOString().slice(0, 10).replace(/-/g, '');
      // const ssnday = control.value ? control.value.split('-') : '';
      const testSsnVal = control.value ? control.value.slice(2) : '';
      let sameSsn = (control.value || '').replace('-', '');
      const isWhitespace = (control.value || '').trim().length === 0;
    
    if (isWhitespace) {
      return {
        invalueWhitespace: true
      };
      } else if (!ssnRegEx.test(control.value)) {
      return {
        invalidSsnFormat: true
      };
      // editssn.setCustomValidity(Drupal.t('Format should be YYYYMMDD-NNNN.'));
    } else if (ssnday[0] > currentdate) {
      return {
        invalidSsnDate: true
      };
    } else if (ssnday[0] <= pastdate) {
      return {
        invalidSsnDate: true
      };
    } else if (!ssnDateRegEx.test(ssnday[0].slice(4, 8))) {
      return {
        invalidSsnDate: true
      };
    } 
    else if (ssn.length >0) {
      return { invalidPoa_ssn_list:true}
    }
    else {
      return null;
    }
   } else {
    const ssnRegEx = new RegExp(ValidationService.validFile.ssn.en.pattern);
    const ssnDateRegEx = new RegExp(ValidationService.validFile.ssn.en.datepattern);
    const isWhitespace = (control.value || '').trim().length === 0;
    let ssn=ValidationService.activePowerOfAttorney.length>0 ? ValidationService.activePowerOfAttorney.filter(power=>power.powerOfAttorney.socialSecurityNumber.replace(/-/g,'')==(control.value || '').replace(/-/g,'')) : [];
    
    if (isWhitespace) {
      return {
        invalueWhitespace: true
      };
      } else if (!ssnRegEx.test(control.value)) {
      return {
        invalidSsnFormat: true
      };
    }
    else if(!ssnDateRegEx.test(ssnday[0].slice(0,4))){
        return {invalidSsnDate:true}
    }
    // else if (debtor_ssn == ssnVal) {
    //   //editssn.setCustomValidity(Drupal.t("Receiver of power of attorney can't have the same SSN as debtor"));
    // }
    else if (ssn.length >0) {
      return { invalidPoa_ssn_list:true}
    }
    else {
      return null;
    }

  }

  }
  //Address validation
  static AddressValidator(control) {
    if (control.value) {
      var regExp = new RegExp(ValidationService.validFile.addressvalidator.regularExp);
      if (control.value.match(regExp)) {
        return null;
      } else {
        return { 'invalidAddress': true };
      }
    }

  }

  // City validation
  static getCityValidate(control) {
    if (control.value) {
      var regExp = new RegExp(ValidationService.validFile.cityvalidator.regularExp);
      if (control.value.match(regExp)) {
        return null;
      }
      else {
        return { 'invalidCity': true };
      }
    }

  }
  public static noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }
  //public static getCityValidate(control) {
  //  const isWhitespace = (control.value || '').trim().length === 0;
  //  if (isWhitespace) {
  //    return {
  //      invalueWhitespace: true
  //    };
  //    } else {
  //      return null;
  //    }
  //}
  // tslint:disable-next-line: member-ordering
  public static getZipCodeValidate(control) {

    const regEx = new RegExp(ValidationService.validFile.zipCode.pattern);
    const isWhitespace = (control.value || '').trim().length === 0;
    if (isWhitespace) {
      return {
        invalueWhitespace: true
      };
      } else if (regEx.test(control.value)) {
      return null;
    } else {
      return {
        invalidZipCode: true
      };
    }
  }
  // tslint:disable-next-line: member-ordering
  public static getWhiteSpace(control) {
    const isWhitespace = (control.value || '').trim().length === 0;
    if (isWhitespace) {
      return {invalueWhitespace: true};
      } else {
        return null;
      }
  }
  public static getPhoneValidate(control) {
    const regEx = new RegExp(ValidationService.validFile.phone.pattern);
    if (regEx.test(control.value)) {
      return null;
    } else {
      return {invalidPhone: true};
    }
  }
  public static getMobileValidate(control){
    const regEx = new RegExp(ValidationService.validFile.mobile.pattern);
    if (regEx.test(control.value)) {
      return null;
    } else {
      return {invalidMobile: true};
    }
  }
  public static getNotificationMobileValidate(control){
    const regEx = new RegExp(ValidationService.validFile.notificationMobile.pattern);
    if (regEx.test(control.value)) {
      return null;
    } else {
      return {invalidMobile: true};
    }
  }
  mobileNotification
  public static getEmailValidate(control){
    const regEx = new RegExp(ValidationService.validFile.email.pattern);
    if (regEx.test(control.value)) {
      return null;
    } else {
      return {invalidEmail: true};
    }
  }
  public static validateInformPartialAmount(control){
    const pattern = new RegExp(ValidationService.validFile.partialAmount.pattern);
    
    let total=parseFloat(ValidationService.totalDebt);
    
    if (!pattern.test(control.value)) {
      return {invalidPartialRegexp:true};
    }
   else if (total<control.value) {
    return {invalidPartialAmount: true};
    }
     else {
        return null;
      }
  }
fetchViaHttp(): Observable<any> {
    return this.http.get(this.baseUrl);
  }

  getValidation() {
    this.fetchViaHttp().subscribe(res => {
      ValidationService.validFile = res; // .json();

    });
  }
}
