import { APP_BASE_HREF } from "@angular/common";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { TestBed } from "@angular/core/testing"
import { Router } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";
import { CommonService } from "../common/common.service";
import { ModalActionsService } from './modal.service'

describe('ModalService', () => {
    let service:ModalActionsService;
    beforeEach(()=>{
        TestBed.configureTestingModule({
            imports:[
                        HttpClientTestingModule,
                        RouterTestingModule
            ],
            providers: [ModalActionsService,
                CommonService,
                {
                    provide: APP_BASE_HREF,
                    useValue: "/se"
                  }
            ]
        });
        service=TestBed.get(ModalActionsService);
    });
    it('should call the modalAction',()=>{
        spyOn(service,'modalAction').and.callThrough();
        let modal={name:'overview'};
        service.modalAction(modal);
        
        expect(service.modalAction).toHaveBeenCalled();
    });
    it('should have call the modalAction method when modalAction name null',()=>{
        spyOn(service,'modalAction').and.callThrough();
        let modal={name:''};
        service.modalAction(modal);
        expect(service.modalAction).toHaveBeenCalled();
    });
   
})