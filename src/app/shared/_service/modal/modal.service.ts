import { Injectable } from '@angular/core';
import { PoaService } from '../poa/poa.service';
import { CommonService } from '../common/common.service';
import { throwError } from 'rxjs';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
    providedIn: 'root'
})
export class ModalActionsService {
constructor(
    private poaService: PoaService,
    private commonService: CommonService,
  private router: Router,
    private translate:TranslateService
) {
  this.commonService.getLanguage.subscribe((data: any) => {

    this.translate.use(data).subscribe(() => {
    });
  });
}


/* This function is the only way this service is directly called in the modal.
  The modal passes to it the received `data` object and then this function
  calls the appropriate function based on the name of the modal. Then, that
  function receives whatever values it needs that were included in `data` */
        modalAction(modalData: any) {
            
            switch (modalData.name) {
            case 'overview':
                            break;
            case 'revoke':
                    this.revokeCase(modalData.PartyGuid);
                    break;
            case 'logout':
                        this.commonService.autoLogout();
                        break;
            case 'transaction':
                            // history.go(-1);
                            this.commonService.transactionSet('yes');
                            break;
            case 'secure-message':
                            this.router.navigateByUrl('/messages')
                                break;

            default:
                break;
            }
        }
        revokeCase(PartyGuid) {
            this.commonService.setRevokeCaseActive(true);
            this.poaService.deletePoa(PartyGuid).subscribe((res: any) => {
                if (res.result.resultCode == 0) {
                    this.commonService.setRevoke('success');
                    this.commonService.setRevokeCaseActive(false);
                } else {
                    this.commonService.setErrorMsg(res.result.resultText);
                }
            }, error => {
                let msgerror = this.translate.instant('technicalIssue');
                this.commonService.setRevokeCaseActive(false);
                this.commonService.setErrorMsg(msgerror);
                throwError(msgerror);
              });
        }
}
