import { Injectable } from '@angular/core';
import { ConfigSetting } from '../../modal/config';
import { HttpClientService } from '../httpclient/http-client.service';

@Injectable({
    providedIn: 'root'
})
export class PoaService {
    baseUrl = '';
    constructor(
       private httpClient: HttpClientService,
       private configSetting: ConfigSetting
       ) {
       this.baseUrl = this.configSetting.apiUrl;
     }
     // add poa
     addPoa(data){
        
        const url = this.baseUrl + 'PowerOfAttorney/CreatePowerOfAttorney';
        return this.httpClient.post(url, data);
     }
     //Get poa list
     getPoa() {
        const url = this.baseUrl + 'PowerOfAttorney/GetPowerOfAttorneys';
        return this.httpClient.get(url);
     }
     // delete poa method
     deletePoa(PartyGuid) {
        const url = this.baseUrl + 'PowerOfAttorney/DeletePowerOfAttorney/'+PartyGuid;
        return this.httpClient.get(url);
     }
    }
