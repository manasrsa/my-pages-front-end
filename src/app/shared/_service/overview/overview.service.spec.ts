/* tslint:disable:no-unused-variable */

import { TestBed, inject, waitForAsync } from '@angular/core/testing';
import { OverviewService } from './overview.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpEvent, HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { APP_BASE_HREF } from '@angular/common';
let service:OverviewService
let httpMock:HttpTestingController;
const mockUsers = [
  { name: 'Bob', website: 'www.yessss.com' },
  { name: 'Juliette', website: 'nope.com' }
];
describe('Service: Overview', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientTestingModule],
      providers: [
        OverviewService,
        {
          provide: APP_BASE_HREF,
          useValue: "/se"
        }
      ]
    });
    service=TestBed.get(OverviewService)
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should have call the get http', () => {
    service.getOverView().subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
            case HttpEventType.Response:
              expect(event.body).toEqual(mockUsers);
          }
    }, (error: HttpErrorResponse) => {

    });
    const mockReq = httpMock.expectOne('se/api/Overview/DebtorSummary');
    expect(mockReq.cancelled).toBeFalsy();
    expect(mockReq.request.responseType).toEqual('json');
    mockReq.flush(mockUsers);
    // mockReq.flush(emsg, { status: 404, statusText: 'Not Found' });
});
});
