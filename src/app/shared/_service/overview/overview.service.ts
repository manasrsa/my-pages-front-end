import { Injectable } from '@angular/core';
import { ConfigSetting } from '../../modal/config';
import { HttpClientService } from '../httpclient/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class OverviewService {
  baseUrl = ''; // '../../assets/data/overview.json';
  constructor(
    private httpClient: HttpClientService,
    private configSetting: ConfigSetting
    ) {
    this.baseUrl = this.configSetting.apiUrl;
   }
  // Get Overview Debtor Summary
   getOverView() {
    const url = this.baseUrl + 'Overview/DebtorSummary';
    return this.httpClient.get(url);
   }
   // Get Case Details
   getCaseDetails(caseNumber) {
    return this.httpClient.get(this.baseUrl + 'Overview/GetCaseDetails/' + caseNumber);
  }
  // Greeting
  getGreeting() {
    return this.httpClient.get(this.baseUrl + 'Utility/GetGreetings');
  }
 // Payment Reference
  getPaymentReference(caseGuid) {
    return this.httpClient.get(this.baseUrl + 'HowToPay/GetPaymentReference/' + caseGuid);
  }
 // Request Respite
  getRequestRespite(caseGuid, day, countryCode) {
    if (countryCode == 'se') {
      return this.httpClient.get(this.baseUrl + 'HowToPay/GetRequestRespite/' + caseGuid + '/' + day);
    }
    else {
      return this.httpClient.get(this.baseUrl + 'HowToPay/GetRequestPromiseToPay/' + caseGuid);
    }
    
  }
  // Calculate Instalement plan
  getCalculateInstalement(caseGuid, month) {
    return this.httpClient.get(this.baseUrl + 'InstalmentPlan/CalculateInstalementPlan/' + caseGuid + '/' + month);
  }
  // Create installment plan
  createInstalmentPlan(caseGuid, amount) {
    return this.httpClient.get(this.baseUrl + 'InstalmentPlan/CreateInstalmentPlan/' + caseGuid + '/' + amount);
  }
  //create swish payment https://localhost:5050/se/api/SwishPayment/SwishPaymentRequestCreate
  createSwishPayment(obj){
    return this.httpClient.post(this.baseUrl +'SwishPayment/SwishPaymentRequestCreate',obj);
  }
  //get Swish payment https://localhost:5050/se/api/SwishPayment/GetSwishPaymentStatus/84397856
  getSwishPayment(transactionID){
    return this.httpClient.get(this.baseUrl + 'SwishPayment/GetSwishPaymentStatus/' + transactionID);
  }
  //https://localhost:5050/se/api/HowToPay/HavingPaid?Amounts=0&CaseGuid=EAB18D59-6058-4A1E-8C63-FE4EBA013136&isFullAmount=true&PaymentDateVal=2021-01-14
  getHowToPay(obj){
    
    return this.httpClient.get(this.baseUrl+'HowToPay/HavingPaid/'+ obj.CaseGuid+'/'+obj.isFullAmount+'/'+obj.Amount+'/'+obj.PaymentDateVal+'/'+obj.isNotify);
  }
}
