import { BreadrumService } from "./breadcrum.service";
import { TestBed } from '@angular/core/testing';
import { of } from "rxjs";

describe('BreadcrumService',()=>{
    let service: BreadrumService;
    let mockBreadcrumService;
    let bredCrumData=[{"title":"Secure messages","breadcrumb":"New message","url":"secure-messages","urlSegments":[{"path":"secure-messages","parameters":{}}],"params":{}},{"title":"Messages","breadcrumb":"Messages","url":"messages","urlSegments":[{"path":"messages","parameters":{}}],"params":{}}]
    beforeEach(()=>{
        mockBreadcrumService=jasmine.createSpyObj('',['getBreadcrum','setBreadcrum']);
        TestBed.configureTestingModule({
            providers:[BreadrumService]
        })
        service=TestBed.get(BreadrumService);
    });
    it('should get breadcrum',()=>{
        // 
        mockBreadcrumService.getBreadcrum.and.returnValue(of(bredCrumData));
        service.getBreadcrum().subscribe(data=>{
                expect(data.result).toBe(bredCrumData);
            });
        //mockBreadcrumService.setBreadcrum(bredCrumData)
    })
})