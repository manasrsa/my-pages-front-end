import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class BreadrumService {
    subject: Subject<any> = new Subject(); // declare of subject
    constructor() {}
    // set breadcrum data
    setBreadcrum(breadCrumData: any) {
        const breadCrum = this.listenRouting(breadCrumData);
        this.subject.next({result: breadCrum});
    }
    // get breadcrum data
    getBreadcrum() {
        return this.subject.asObservable();
    }
    listenRouting(breadCrumData) {
  
        breadCrumData.map((obj, index) => {
            if (obj.params.id) {
                breadCrumData[index].breadcrumb = obj.params.id;
            } 
            else if (obj.params.name) {
                breadCrumData.pop();
            }
        });
        return breadCrumData;
    }
}
