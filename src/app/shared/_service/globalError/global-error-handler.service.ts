import { Injectable, ErrorHandler, Injector } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { ApplicationInsightsService } from '../app-insights/appInsights-service';
@Injectable()
export class GlobalErrorHandlerService implements ErrorHandler {
    constructor(private injector: Injector) { }
    messages: any = {msg: '', type: 'error'};
    handleError(error: any) {

      const appInsights = this.injector.get(ApplicationInsightsService);
      appInsights.logException (error.message);
      if (error instanceof HttpErrorResponse) {
        this.messages.msg = error.status;
          // Backend returns unsuccessful response codes such as 404, 500 etc.
        this.errorMessage(this.messages);
         // console.error('Response body:', error.message);
      } else {
        this.messages.msg = error.message;
          // A client-side or network error occurred.
          // console.error('An error occurred:', error.message);
      }

    }
    // error message service call
  errorMessage(message: string) {

    // this.toastservice.error(message);
    // this.toastservice.errorSnackBar(message);
}
}
