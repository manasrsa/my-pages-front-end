import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, inject, getTestBed } from '@angular/core/testing';
import { HttpEvent, HttpEventType, HttpErrorResponse, HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { HttpClientService } from './http-client.service';
const mockUsers = [
    { name: 'Bob', website: 'www.yessss.com' },
    { name: 'Juliette', website: 'nope.com' }
  ];
describe('CustomHttpClient', () => {
    let service: HttpClientService;
    let httpMock: HttpTestingController;
    const url = 'https://jsonplaceholder.typicode.com/users';
    const errorResponse = new HttpErrorResponse({
        error: '404 error',
        status: 404,
        statusText: 'Not Found'
      });
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [HttpClientService]
        });
        service = TestBed.get(HttpClientService);
        httpMock = TestBed.get(HttpTestingController);
    });
    it('should have call the get http', () => {
        service.get(url).subscribe((event: HttpEvent<any>) => {
            switch (event.type) {
                case HttpEventType.Response:
                  expect(event.body).toEqual(mockUsers);
              }
        }, (error: HttpErrorResponse) => {

        });
        const mockReq = httpMock.expectOne(url);
        expect(mockReq.cancelled).toBeFalsy();
        expect(mockReq.request.responseType).toEqual('json');
        mockReq.flush(mockUsers, errorResponse);
        // mockReq.flush(emsg, { status: 404, statusText: 'Not Found' });
    });
    
    it('should have call the post http', () => {
        const user = {firstname: 'firstname'};
        service.post(url, user).subscribe((event: HttpEvent<any>) => {
            switch (event.type) {
                case HttpEventType.Response:
                  expect(event.body).toEqual(user);
              }
        }, (error: HttpErrorResponse) => {

        });
        const mockReq = httpMock.expectOne(url, 'post to api');
        expect(mockReq.request.method).toBe('POST');
        mockReq.flush({
            firstname: 'firstname'
          });

        httpMock.verify();
        // mockReq.flush(emsg, { status: 404, statusText: 'Not Found' });
    });
   
});
