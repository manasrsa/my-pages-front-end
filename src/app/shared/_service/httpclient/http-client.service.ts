import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class HttpClientService {
  http: HttpClient;
  urlPrefix: string;
  //httpOptions = {
  //  headers: new HttpHeaders({
  //    'Content-Type': 'application/json',
  //    Authorization: `Basic ` + btoa('user:password'),
  //  })

  //  }
  constructor(http: HttpClient) {
    this.http = http;
  }
  get(url) {
    return this.http.get(url).pipe(catchError((err) => { return Observable.throw(err)}));
  }
  post(url, data) {
    return this.http.post(url, data).pipe(catchError((err) => {return Observable.throw(err)}));
  }
}
