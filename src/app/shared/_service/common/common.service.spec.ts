import { APP_BASE_HREF } from "@angular/common";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { TestBed } from "@angular/core/testing";
import { CommonService } from "./common.service"

describe('CommonService',()=>{
    let service:CommonService;
    beforeEach(()=>{
        TestBed.configureTestingModule({
            imports:[HttpClientTestingModule],
            providers:[CommonService,
                {
                    provide: APP_BASE_HREF,
                    useValue: "/se"
                }
            ]
        })
        service=TestBed.get(CommonService);
    });
    it('should call the notifySpinnerAction',()=>{
        spyOn(service,'notifySpinnerAction').and.callThrough();
        service.notifySpinnerAction(true)
        expect(service.notifySpinnerAction).toHaveBeenCalled();
    })
    it('should call the setRevoke',()=>{
        spyOn(service,'setRevoke').and.callThrough();
        service.setRevoke(true);
        expect(service.setRevoke).toHaveBeenCalled();
    });
    it('should call the setRevokeCaseActive',()=>{
        spyOn(service,'setRevokeCaseActive').and.callThrough();
        service.setRevokeCaseActive(true);
        expect(service.setRevokeCaseActive).toHaveBeenCalled();
    })
    it('should call the notifyUserAction',()=>{
        spyOn(service,'notifyUserAction').and.callThrough();
        service.notifyUserAction();
        expect(service.notifyUserAction).toHaveBeenCalled();
    })
})