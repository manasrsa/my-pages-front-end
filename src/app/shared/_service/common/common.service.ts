import { APP_BASE_HREF } from '@angular/common';
import { Injector } from '@angular/core';
import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, throwError } from 'rxjs';
import { LogOutService } from '../logout/logout.service';

@Injectable({
    providedIn: 'root'
})
export class CommonService {
    countryCode='';
    private languageSource = new Subject();
    getLanguage = this.languageSource.asObservable();
    private revokeCase = new BehaviorSubject('');
    revoke = this.revokeCase.asObservable();
    private revokeCaseActive = new BehaviorSubject(false);
     getRevokeCaseActive = this.revokeCaseActive.asObservable();
     private activePowerOfAttorney = new BehaviorSubject('');
     getActivePowerOfAttorney = this.activePowerOfAttorney.asObservable();
     private totalDebt=new BehaviorSubject('');
     getTotalDebt=this.totalDebt.asObservable();
     private caseDetail = new BehaviorSubject('');
     getCaseDetail = this.caseDetail.asObservable();
     private errorMsg = new BehaviorSubject('');
     swishErrorMsg = new BehaviorSubject('');
     getErrorMsg = this.errorMsg.asObservable();
     private successMsg = new BehaviorSubject('');
     getSuccessMsg = this.successMsg.asObservable();
     private days = new BehaviorSubject('');
     getDays = this.days.asObservable();
     _userActionOccured: Subject<void> = new Subject();
     get userActionOccured(): Observable<void> { return this._userActionOccured.asObservable() };
    _spinnerActionOccured: Subject<void> = new Subject();
    private activePoaSubject = new BehaviorSubject(false);
    activePoa=this.activePoaSubject.asObservable();
     //get spinnerActionOccured() : Observable<void>{
     //    return this._spinnerActionOccured.asObservable();
     //}
     transactionVal:string='';
     public caseDetailsSubject = new Subject<any>();
     constructor(
         private logoutService:LogOutService,
         private injector: Injector,
         @Inject(APP_BASE_HREF) private baseHref: string
     ) {

        this.countryCode = this.baseHref.split("/")[1];
    }
    notifySpinnerAction(active){
        this._spinnerActionOccured.next(active);
  }
    getNotifySpinnerAction() {
      return this._spinnerActionOccured.asObservable();
    }
    notifyUserAction() {
        this._userActionOccured.next();
      }
    setLanguage(message: string) {
        this.languageSource.next(message);
      }
    setRevoke(msg: any) {
        this.revokeCase.next(msg);
    }
    setRevokeCaseActive(active: any) {
        this.revokeCaseActive.next(active);
    }
    setActivePowerOfAttorney(powerofattorney: any) {
        this.activePowerOfAttorney.next(powerofattorney);
    }
    setTotalDebt(totalDebtAmount:any){
        this.totalDebt.next(totalDebtAmount)
    }
    setCaseDetail(obj: any) {
 
        this.caseDetail.next(obj);
    }
    
    setErrorMsg(msg: any) {
        this.errorMsg.next(msg);
    }
    setSuccessMsg(msg: any) {
        this.successMsg.next(msg);
    }
    setDays(day: any) {
       
        this.days.next(day);
    }
    setswishErrorMsg(msg:any){
        this.swishErrorMsg.next(msg);
    }
    getSwishErrorMsg(){
        return this.swishErrorMsg.asObservable();
    }
    setSessionStorageMsg(obj: any) {
        sessionStorage.setItem('msg', JSON.stringify(obj));
    }
    getSessionStorageMsg() {
       let obj = sessionStorage.getItem('msg');
        return obj;
    }
    setBreadCrumDynamic(obj:any){
        sessionStorage.setItem('bredcrum', JSON.stringify(obj));
    }
    getBreadCrumDynamic() {
        let obj = sessionStorage.getItem('bredcrum');
        return obj;
     }
     setConfirmation(obj:any){
        sessionStorage.setItem('conf-comp', JSON.stringify(obj));
     }
     getConfirmation() {
        let obj = sessionStorage.getItem('conf-comp');
        return obj;
     }
     removeBreadCrum(){
         sessionStorage.removeItem('bredcrum');
     }
     transactionSet(val:string){
        this.transactionVal=val
     }
     transactionGet(){
         return  this.transactionVal;
  }
  setPoa(val) {
    this.activePoaSubject.next(val)
  }
     autoLogout(){
        this.notifySpinnerAction(true);
        //this.logoutService.logout().subscribe((res: any) => {
        //    if (res.result.resultCode === 0) {
        //      window.open(this.countryCode+'/Login/BankID', '_self')
        //      this.setErrorMsg('');
        //     } else {
        //        this.setErrorMsg(res.result.resultText);
        //     }
        //     this.notifySpinnerAction(false);
        //}, error => {
        //    this.notifySpinnerAction(false);
        //    this.setErrorMsg('Some issues for fetching the logout');
        //    throwError('Some issues for fetching the logout');
        //});
       window.open(this.countryCode + '/Logout/SignOut', '_self');
     }
     setAppinsightErrorLog(){

     }

      setCaseDetails(caseDetailsData) {
        this.caseDetailsSubject.next(caseDetailsData)
      }
      getCaseDetails() {
        return this.caseDetailsSubject.asObservable();
      }
}
