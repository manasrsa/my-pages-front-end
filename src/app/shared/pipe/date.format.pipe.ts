import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({ name: 'dateFormat' })
export class DateFormatPipe implements PipeTransform {
  // adding a default value in case you don't want to pass the format then 'yyyy-MM-dd' will be used
  transform(date: Date | string, conuntryCode: string): string {
    date = new Date(date);  // if orginal type was a string
   let  format='';
    if(conuntryCode=='se'){
        format='yyyy-MM-dd'
    }
    else{
        format='dd.MM.yyyy'
    }

    return new DatePipe('en-US').transform(date, format);
  }
}