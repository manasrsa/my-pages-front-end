import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'currencyPipe' })
export class CurrencyFormatPipe implements PipeTransform {


  transform(value: any): any {
    if (!value) {
      return ""
    }
    const currencySymbol = 'SEK'
    const currencyArray = value == undefined ? '0' : value.toString();
    const decimalPos = currencyArray.indexOf('.');
    const commaPos = currencyArray.indexOf(',');
    if (currencyArray.indexOf('.') != -1) {
      let format  = currencyArray.replace('.', ',');
      const pos = format.indexOf(',');
      let len = format.length;
      if (len - pos < 3) {
          format = format + '0 ';
          return format;
        }
      format = format + ' ' ;
      return format;
    } else if (currencyArray.indexOf(',') != -1) {
      let format  = currencyArray;
      const pos = format.indexOf(',');
      let len = format.length;
      if (len - pos < 3) {
          format = format + '0 ';
          return format;
        }
      format = format + ' ' ;
      return format;
    } else {
        let format  = currencyArray;
        format = format + ',00 ';
        return format;
    }

  }

}
