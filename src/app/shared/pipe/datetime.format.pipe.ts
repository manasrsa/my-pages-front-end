import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({ name: 'datetimeFormat' })
export class DateTimeFormatPipe implements PipeTransform {
  // adding a default value in case you don't want to pass the format then 'yyyy-MM-dd' will be used
  transform(date: Date | string, conuntryCode: string): string {
    date = new Date(date);  // if orginal type was a string
   let  format = '';
    if (conuntryCode == 'se') {
        format = 'dd/MM/yy hh:mm';
    } else {
        format = 'dd/MM/yy hh:mm';
    }

    return new DatePipe('en-US').transform(date, format);
  }
}
