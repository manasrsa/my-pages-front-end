import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContactUsComponent } from './contact-us.component';
import { ContactRoutes } from './contactus.module.router';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href.split('/')[3];
}
export function HttpLoaderFactory(httpClient: HttpClient) {
    const countryCode=getBaseUrl();
    return new TranslateHttpLoader(httpClient, './assets/i18n/'+countryCode+'/contactus/', '.json');
  }
@NgModule({
    imports:[
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        TranslateModule.forChild({
            loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
            },
            isolate: true
          }),
        RouterModule.forChild(ContactRoutes)
    ],
    declarations: [ContactUsComponent],
    exports:[ContactUsComponent],
    providers: [],
    schemas:[CUSTOM_ELEMENTS_SCHEMA]
})

export class ContactUsModule{}