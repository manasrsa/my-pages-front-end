import { Routes } from "@angular/router";
import { ContactUsComponent } from './contact-us.component';
export const ContactRoutes: Routes = [
    {
        path:'',
        component:ContactUsComponent,
        runGuardsAndResolvers:'always',
    }
]
