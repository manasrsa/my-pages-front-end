import { APP_BASE_HREF } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { CommonService } from 'src/app/shared/_service/common/common.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  conuntryCode = 'se';
  currentLang:string='';
  constructor(private router: Router,
    private translate:TranslateService,
    private commonService:CommonService,
    private cookieService:CookieService,
    @Inject(APP_BASE_HREF) private baseHref: string
    ) { 
      this.conuntryCode = this.baseHref.split("/")[1];
      this.currentLang=this.conuntryCode=='se' ? 'sv' : 'fi';
      this.commonService.setErrorMsg('');
      // get localization
      this.commonService.getLanguage.subscribe((data:any) => {
        this.translate.use(data);
      });
    }

  ngOnInit() {
    this.setLocalization();
  }
   // set localization
   setLocalization() {
    this.translate.addLangs(['sv', 'en', 'fi']);
    if (this.translate.getBrowserLang() !== undefined) {
      let lang = this.cookieService.get('.AspNetCore.Culture');
      
      if (lang) {
        var langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.currentLang=langCode;
        this.translate.use(langCode);
      }else {
        this.translate.use(this.currentLang);
      }
    }
    else {
      this.translate.use(this.currentLang);
    }
  }
 
}
