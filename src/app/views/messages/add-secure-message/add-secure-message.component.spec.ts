/* tslint:disable:no-unused-variable */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AddSecureMessageComponent } from './add-secure-message.component';
import { SharedModule } from 'src/app/shared/shared/shared.module';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateMessagesLoader } from 'src/app/shared/testing/translate.messages';
import { HttpClient } from '@angular/common/http';

describe('AddSecureMessageComponent', () => {
  let component: AddSecureMessageComponent;
  let fixture: ComponentFixture<AddSecureMessageComponent>;
  let translateService;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports:[
        BrowserAnimationsModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        MatDialogModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useClass: TranslateMessagesLoader,
            deps: [HttpClient]
          }
        })
      ],
      declarations: [ AddSecureMessageComponent ],
      providers:[
        {
          provide: APP_BASE_HREF,
          useValue: "/se"
        },
        TranslateService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSecureMessageComponent);
    component = fixture.componentInstance;
    translateService = TestBed.get(TranslateService);
    translateService.use('en');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
