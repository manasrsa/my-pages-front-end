import { APP_BASE_HREF } from '@angular/common';
import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { throwError } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { ToastsComponent } from 'src/app/shared/components/toasts/toasts.component';
import { UploadQueue } from 'src/app/shared/modal/uploader';
import { Uploader } from 'src/app/shared/modal/uploadquer';
import { CommonService } from 'src/app/shared/_service/common/common.service';
import { MessageService } from 'src/app/shared/_service/messages/messages.service';
import { ValidationService } from 'src/app/shared/_service/validation/validation.service';

@Component({
  selector: 'app-add-secure-message',
  templateUrl: './add-secure-message.component.html',
  styleUrls: ['./add-secure-message.component.scss']
})
export class AddSecureMessageComponent implements OnInit {
  secureForm: FormGroup;
  securitySolutions: any = [];
  selectOption: any = '';
  conuntryCode = 'se';
  casenumber= 0;
  public uploader: Uploader = new Uploader();
  fileSizeError = '';
  fileTypeError = '';
  formFile: FormData = new FormData();
  sendButton= false;
  fileName = '';
  currentLang:string='';
  @ViewChild('myInput', { static: true }) myInputVariable: ElementRef;
  constructor(
    private translate: TranslateService,
    private builder: FormBuilder,
    private messageService: MessageService,
    private commonService: CommonService,
    private router: Router,
    private matDialog: MatDialog,
    private snackBar:MatSnackBar,
    private cookieService:CookieService,
    @Inject(APP_BASE_HREF) private baseHref: string
  ) {
    this.conuntryCode=this.baseHref.split("/")[1];
    this.currentLang=this.conuntryCode=='se' ? 'sv' : 'fi';
    this.commonService.setErrorMsg('');
     // get localization
     this.commonService.getLanguage.subscribe((data:any) => {
      this.translate.use(data).subscribe((res:any)=>{
        this.selectOption = this.translate.instant('select-a-case');
      });
    });

  }

  ngOnInit() {
    this.secureForm = this.builder.group({
      CaseLookUp: ['', [Validators.required]],
      Subject: ['', [Validators.required]],
      ConversationGuid: ['', ''],
      MessageText: ['', [Validators.required]]
    });
    this.getCaseListMessage();
    this.setLocalization();
    this.init();
    this.selectOption = this.translate.instant('select-a-case');
  }
  init() {
    document.querySelector('.select-wrapper').addEventListener('click', function() {
      this.querySelector('.selectric-wrapper').classList.toggle('selectric-open');
  });
  }
  selectedOption(index, securitySolution) {
  const options = document.querySelectorAll('.last');
  for (let i = 0; i < options.length; i++) {
      if (i === index) {
        options[index].classList.add('selected');
        this.selectOption = securitySolution.clientName;
        this.casenumber = securitySolution.caseNumber;
        const CaseLookUp = {
          CaseIdentification: 0,
          CaseIdentificationValue: securitySolution.caseGuid};
        this.secureForm.controls.CaseLookUp.setValue(CaseLookUp);

      } else {
        options[i].classList.remove('selected');
      }
    }
  }
  // set localization
  setLocalization() {
    this.translate.addLangs(['sv', 'en', 'fi']);
    if (this.translate.getBrowserLang() !== undefined) {
      let lang = this.cookieService.get('.AspNetCore.Culture');

      if (lang) {
        var langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.currentLang=langCode;
        this.translate.use(langCode);
      }else {
        this.translate.use(this.currentLang);
      }
    }
    else {
      this.translate.use(this.currentLang);
    }
  }
  // get case
  getCaseListMessage() {
    this.messageService.getCaseListForMessage().subscribe((response: any) => {

      if (response.result.resultCode === 0) {
        if (response.data.messageCaseLists) {
          this.securitySolutions = response.data.messageCaseLists;
        }
      } else {
        this.commonService.setErrorMsg(response.result.resultText);
        this.openErrorToast()
      }
    }, error => {
        let msgerror = this.translate.instant('technicalIssue');
        throwError(msgerror);
        this.commonService.setErrorMsg(msgerror);
      this.openErrorToast()
    });
  }
  onSelectChange(event: EventTarget) {
    // this.formFile = new FormData();
    const fileUpload = document.getElementById('edit-file-upload-upload') as HTMLInputElement;
    fileUpload.onchange = (e) => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        const fileSize = Math.ceil(fileUpload.files[index].size / 1024 / 1024);
        
        if (fileSize <= 5) {
          // pdf,tiff,jpg,png)
          const type = fileUpload.files[index].name.split('.')[fileUpload.files[index].name.split('.').length - 1].toLowerCase();
          if (type === 'pdf' || type == 'jpeg' || type == 'jpg' || type == 'png' ) {
            this.uploader.queue.push(new UploadQueue(file));
           // this.formFile.append('file', file);
            this.fileSizeError='';
            this.fileTypeError='';
          } else {
            this.fileName = file.name;
            this.fileTypeError = 'active';
          }
        } else {
          this.fileSizeError = 'active';

        }
      }
    };
  }
  removeFile() {
    this.uploader = new Uploader();
    const file = document.getElementById('edit-file-upload-upload') as HTMLInputElement;
    file.value = '';
  }
  //cencel file method
//   cancelFile(file: UploadQueue) {

//     if (file) {
//         if (file.sub) {
//             file.sub.unsubscribe();
//         }
//         this.myInputVariable.nativeElement.value = "";
//         this.removeFileFromArray(file);
//     }
// }
// private removeFileFromArray(file: UploadQueue) {
//     const index = this.uploader.queue.indexOf(file);
//     if (index > -1) {
//         this.uploader.queue.splice(index, 1);
//         if (this.uploader.queue.length == 0) {
//             //this.uploadQuelength = 0;
//         }
//     }
// }
  submitMessageForm() {
    this.sendButton = true;
    if (this.uploader.queue.length > 0) {
      this.formFile.append('file', this.uploader.queue[0].file);
      }
    this.formFile.append('postMessageRequest', JSON.stringify(this.secureForm.value));
    if (this.secureForm.valid) {
      this.messageService.createMessage(this.formFile).subscribe((res: any) => {
        if (res.result.resultCode === 0) {
          this.router.navigateByUrl('/messages/secure-messages/thank-you');
        } else {
          this.commonService.setErrorMsg(res.result.resultText);
          this.openErrorToast()
        }
        this.sendButton = false;
      }, error => {
          this.sendButton = false;
          let msgerror = this.translate.instant('technicalIssue');
          throwError(msgerror);
          this.commonService.setErrorMsg(msgerror);
        this.openErrorToast()
      });
    }

  }
   //open error Toast Component
   openErrorToast()
   {
     this.snackBar.openFromComponent(ToastsComponent
       , {
       panelClass: 'error',
       horizontalPosition:  'center',
       verticalPosition: 'top',
       });
   }
  leavePage() {
    const modalName = 'add-secure-message';
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.id = 'modal-component';
    dialogConfig.height = 'auto';
    dialogConfig.width = '600px';
    dialogConfig.data = {
      name: 'secure-message',
      // title: 'Are you sure you want to revoke this power of attorney?',
      bodyContent: '',
      cancelButtonText: '',
      actionButtonText: '',
      modalName
    };
    dialogConfig.data.actionButtonText = this.translate.instant('addSecureModalButtonYesLeaveText');
    dialogConfig.data.cancelButtonText = this.translate.instant('msgCancelButtonText');
    dialogConfig.data.bodyContent = '<h3>' + this.translate.instant('do-you-want-to-leave') + '</h3><p>' + this.translate.instant('your-changes-will-be-discarded') + '</p>';
    const modalDialog = this.matDialog.open(ModalComponent, dialogConfig);
  }
}
