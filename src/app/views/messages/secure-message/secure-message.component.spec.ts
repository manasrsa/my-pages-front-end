import { APP_BASE_HREF } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateMessagesLoader } from 'src/app/shared/testing/translate.messages';

import { SecureMessageComponent } from './secure-message.component';

describe('SecureMessageComponent', () => {
  let component: SecureMessageComponent;
  let fixture: ComponentFixture<SecureMessageComponent>;
  let translateService;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports:[
        BrowserAnimationsModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useClass: TranslateMessagesLoader,
            deps: [HttpClient]
          }
        })
      ],
      declarations: [ SecureMessageComponent ],
      providers:[
        TranslateService,
        {
          provide: APP_BASE_HREF,
          useValue: "/se"
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecureMessageComponent);
    component = fixture.componentInstance;
    translateService = TestBed.get(TranslateService);
    translateService.use('en');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
