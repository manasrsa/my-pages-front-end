import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { messagesRoutes } from './messages.module.router';
import { MessageComponent } from './message/message.component';
import { AddSecureMessageComponent } from './add-secure-message/add-secure-message.component';
import { JwPaginationComponent } from 'src/app/shared/components/jw-pagination/jw-pagination.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SharedModule } from 'src/app/shared/shared/shared.module';
import { EditSecureMessageComponent } from './edit-secure-message/edit-secure-message.component';
import { ThankYouComponent } from './thank-you/thank-you.component';
import { SecureMessageComponent } from './secure-message/secure-message.component';
export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href.split('/')[3];
}
export function HttpLoaderFactory(httpClient: HttpClient) {
  const countryCode=getBaseUrl();
    return new TranslateHttpLoader(httpClient, "./assets/i18n/"+countryCode+"/messages/", ".json");
  }
@NgModule({
    imports:[
        CommonModule,
        FormsModule,
        SharedModule,
        ReactiveFormsModule,
        TranslateModule.forChild({
            loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
            },
            isolate: true
          }),
        RouterModule.forChild(messagesRoutes)
    ],
    declarations: [MessageComponent,AddSecureMessageComponent,JwPaginationComponent, EditSecureMessageComponent, ThankYouComponent, SecureMessageComponent],
    exports:[MessageComponent,AddSecureMessageComponent,JwPaginationComponent],
    providers: [],
    schemas:[CUSTOM_ELEMENTS_SCHEMA]
})

export class MessagesModule{}