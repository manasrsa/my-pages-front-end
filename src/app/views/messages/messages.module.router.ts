import { Route} from '@angular/router';
import { MessageComponent } from './message/message.component';
import { AddSecureMessageComponent } from './add-secure-message/add-secure-message.component';
import { EditSecureMessageComponent } from './edit-secure-message/edit-secure-message.component';
import { ThankYouComponent } from './thank-you/thank-you.component';
import { SecureMessageComponent } from './secure-message/secure-message.component';
export const messagesRoutes: Route[] = [
    {
        path: '',
        component: MessageComponent,
    },
    {
        path: 'secure-messages',
        component: SecureMessageComponent,
        data: {
            title: 'titleMsgSecure',
            breadcrumb: 'breadcrumNewMessage',
            auth: 'Secure messages'
          },
        children: [
            {
                path:'',
                component: AddSecureMessageComponent,
                data: {
                    title: 'titleMsgSecure',
                    breadcrumb: 'breadcrumNewMessage',
                    auth: 'Secure messages'
                }
            },
            {
                path:'thank-you',
                component: ThankYouComponent,
                data: {
                    title: 'breadcrumMsgThankYou',
                    breadcrumb: 'breadcrumMsgThankYou',
                    auth: 'Thank you'
                }
            }
        ]
    },
    {
        path: ':id',
        component: EditSecureMessageComponent,
        data: {
          title: 'titleMsgSecure',
          breadcrumb: '',
          auth: 'Secure messages'
        }
      },
];
