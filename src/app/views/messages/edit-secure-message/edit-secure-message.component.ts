import { APP_BASE_HREF } from '@angular/common';
import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { throwError } from 'rxjs';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { ToastsComponent } from 'src/app/shared/components/toasts/toasts.component';
import { UploadQueue } from 'src/app/shared/modal/uploader';
import { Uploader } from 'src/app/shared/modal/uploadquer';
import { BreadrumService } from 'src/app/shared/_service/breadcrum/breadcrum.service';
import { CommonService } from 'src/app/shared/_service/common/common.service';
import { MessageService } from 'src/app/shared/_service/messages/messages.service';

@Component({
  selector: 'app-edit-secure-message',
  templateUrl: './edit-secure-message.component.html',
  styleUrls: ['./edit-secure-message.component.scss']
})
export class EditSecureMessageComponent implements OnInit {
  breadCrumData = [];
  messages: any = [];
  secureForm: FormGroup;
  btnEnable = false;
  conuntryCode = 'se';
  messageSubject = '';
  public uploader: Uploader = new Uploader();
  fileSizeError = '';
  fileTypeError = '';
  formFile: FormData = new FormData();
  ajaxActiveVal = false;
  sendButton = false;
  fileName = '';
  fileLists:any='';
  msgList: any = '';
  currentLang:string='';
  replyOption:boolean=false;
  @ViewChild('myInput') myInputVariable: ElementRef;
  constructor(
    public breadCrumService: BreadrumService,
    private commonService: CommonService,
    private messageService: MessageService,
    private activateRoute: ActivatedRoute,
    private builder: FormBuilder,
    private router: Router,
    private translate: TranslateService,
    private snackBar:MatSnackBar,
    private matDialog: MatDialog,
    private cookieService:CookieService,
    @Inject(APP_BASE_HREF) private baseHref: string
  ) {
    this.conuntryCode=this.baseHref.split("/")[1];
    this.currentLang=this.conuntryCode=='se' ? 'sv' : 'fi';
    this.commonService.setErrorMsg('');
    this.breadCrumService.getBreadcrum().subscribe((response: any) => {
      this.breadCrumData = response.result;
    });
     // get localization
    this.commonService.getLanguage.subscribe((data:any) => {
      this.translate.use(data);
    });
  }

  ngOnInit(): void {
    this.secureForm = this.builder.group({
      CaseLookUp: ['', []],
      Subject: ['', []],
      ConversationGuid: [null, []],
      MessageText: ['', [Validators.required]],
    });
    this.getCaseListMessage();
    this.getMessageConversion();
    this.setLocalization();
    this.setBreadCrumb();
  }
  setBreadCrumb() {
    this.msgList = JSON.parse(this.commonService.getSessionStorageMsg());
   
    if (this.breadCrumData.length > 1) {
      this.breadCrumData.map((obj, index) => {
        if (obj.params.id) {
          this.breadCrumData[index].breadcrumb = this.msgList.subject;
        }
      });

    }
  }
   // get case
   getCaseListMessage() {
    this.messageService.getCaseListForMessage().subscribe((response: any) => {

      if (response.result.resultCode === 0) {
        if (response.data.messageCaseLists) {
          const result = response.data.messageCaseLists;
         let val=result.filter(obj=>obj.caseNumber==this.msgList.caseNumber);
          
          this.msgList.clientName=val[0].clientName;
          
        }
      } else {
        this.commonService.setErrorMsg(response.result.resultText);
        this.openErrorToast()
      }
    }, error => {
        let msgerror = this.translate.instant('technicalIssue');
        throwError(msgerror);
        this.commonService.setErrorMsg(msgerror);
      this.openErrorToast()
    });
  }
   // set localization
   setLocalization() {
    this.translate.addLangs(['sv', 'en', 'fi']);
    if (this.translate.getBrowserLang() !== undefined) {
      let lang = this.cookieService.get('.AspNetCore.Culture');
    
      if (lang) {
        var langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.currentLang=langCode;
        this.translate.use(langCode);
      }else {
        this.translate.use(this.currentLang);
      }
    }
    else {
      this.translate.use(this.currentLang);
    }
  }
  onSelectChange(event: EventTarget) {
    // this.formFile = new FormData();
    const fileUpload = document.getElementById('edit-file-upload-upload') as HTMLInputElement;
    fileUpload.onchange = (e) => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        const fileSize = Math.ceil(fileUpload.files[index].size / 1024 / 1024);
        if (fileSize <= 5) {
          // pdf,tiff,jpg,png)
          const type = fileUpload.files[index].name.split('.')[fileUpload.files[index].name.split('.').length - 1].toLowerCase();
          if (type === 'pdf' || type == 'jpeg' || type == 'jpg' || type == 'png'  ) {
            this.uploader.queue.push(new UploadQueue(file));
     
            this.fileSizeError = '';
            this.fileTypeError = '';
          } else {
                  this.fileTypeError = 'active';
                  this.fileName = file.name;
          }
        } else {
          this.fileSizeError = 'active';

        }
      }
    };
  }
  removeFile() {
    this.uploader = new Uploader();
    const file = document.getElementById('edit-file-upload-upload') as HTMLInputElement;
    file.value = '';
  }
   //cencel file method
//    cancelFile(file: UploadQueue) {

//     if (file) {
//         if (file.sub) {
//             file.sub.unsubscribe();
//         }
//         this.myInputVariable.nativeElement.value = "";
//         this.removeFileFromArray(file);
//     }
// }
// private removeFileFromArray(file: UploadQueue) {
//     const index = this.uploader.queue.indexOf(file);
//     if (index > -1) {
//         this.uploader.queue.splice(index, 1);
//         if (this.uploader.queue.length == 0) {
//             //this.uploadQuelength = 0;
//         }
//     }
// }
  submitMessageForm() {
    this.sendButton = true;
    if (this.uploader.queue.length > 0) {
     // this.formFile.append('AttachedDocument', this.uploader.queue[0].file);
     this.formFile.append('file', this.uploader.queue[0].file);
    }
    this.formFile.append('postMessageRequest', JSON.stringify(this.secureForm.value));
    if (this.secureForm.valid) {
      this.messageService.createMessage(this.formFile).subscribe((res: any) => {

        if (res.result.resultCode === 0) {
          this.router.navigateByUrl('/messages/secure-messages/thank-you');
        } else {
          this.commonService.setErrorMsg(res.result.resultText);
          this.openErrorToast()
        }
        this.sendButton = false;
      }, error => {
          this.sendButton = false;
          let msgerror = this.translate.instant('technicalIssue');
          throwError(msgerror);
          this.commonService.setErrorMsg(msgerror);
        this.openErrorToast();
      });
    }

  }
  getMessageConversion() {
    const guid = this.activateRoute.snapshot.paramMap.get('id');
    this.ajaxActiveVal = true;
    this.messageService.conversionMessages(guid).subscribe((response: any) => {
      if (response.result.resultCode === 0) {
        if (response.data.debtorMesssagesResult) {
          const result = response.data.debtorMesssagesResult;
          this.ajaxActiveVal = false;
          this.messages = result;
          this.fileLists=response.data.fileNames;
          this.replyOption=response.data.replyOption;
          console.log(this.replyOption);
          const CaseLookUp = {
                  CaseIdentification: 0,
                  CaseIdentificationValue: guid
          };
          this.secureForm.controls.ConversationGuid.setValue(guid);
          this.secureForm.controls.CaseLookUp.setValue(CaseLookUp);
        }



      } else {
        this.ajaxActiveVal = false;
        this.commonService.setErrorMsg(response.result.resultText);
        this.openErrorToast()
      }
    }, error => {
        this.ajaxActiveVal = false;
        let msgerror = this.translate.instant('technicalIssue');
        throwError(msgerror);
        this.commonService.setErrorMsg(msgerror);
      this.openErrorToast()
    });
  }
   //open error Toast Component
   openErrorToast()
   {
     this.snackBar.openFromComponent(ToastsComponent
       , {
       panelClass: 'error',
       horizontalPosition:  'center',
       verticalPosition: 'top',
       });
   }
   //leave page
   leavePage() {
     if(this.secureForm.valid){
      const modalName = 'add-secure-message';
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.id = 'modal-component';
    dialogConfig.height = 'auto';
    dialogConfig.width = '600px';
    dialogConfig.data = {
      name: 'secure-message',
      // title: 'Are you sure you want to revoke this power of attorney?',
      bodyContent: '',
      cancelButtonText: '',
      actionButtonText: '',
      modalName
    };
    dialogConfig.data.actionButtonText = this.translate.instant('addSecureModalButtonYesLeaveText');
    dialogConfig.data.cancelButtonText = this.translate.instant('msgCancelButtonText');
    dialogConfig.data.bodyContent = '<h3>' + this.translate.instant('do-you-want-to-leave') + '</h3><p>' + this.translate.instant('your-changes-will-be-discarded') + '</p>';
    const modalDialog = this.matDialog.open(ModalComponent, dialogConfig);
     }
     else{
       this.router.navigateByUrl('/messages')
     }
    
  }
}
