import { APP_BASE_HREF } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { SharedModule } from 'src/app/shared/shared/shared.module';
import { TranslateMessagesLoader } from 'src/app/shared/testing/translate.messages';

import { EditSecureMessageComponent } from './edit-secure-message.component';

describe('EditSecureMessageComponent', () => {
  let component: EditSecureMessageComponent;
  let fixture: ComponentFixture<EditSecureMessageComponent>;
  let translateService;
  let mockCommonService;
  beforeEach(waitForAsync(() => {
    mockCommonService=jasmine.createSpyObj('CommonService',['getSessionStorageMsg','setSessionStorageMsg'])
    TestBed.configureTestingModule({
      imports:[
        BrowserAnimationsModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        MatDialogModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useClass: TranslateMessagesLoader,
            deps: [HttpClient]
          }
        })
      ],
      declarations: [ EditSecureMessageComponent],
      providers:[TranslateService,
        {
          provide: APP_BASE_HREF,
          useValue: "/se"
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSecureMessageComponent);
    component = fixture.componentInstance;
    
    translateService = TestBed.get(TranslateService);
    translateService.use('en');
    mockCommonService.setSessionStorageMsg({subject:'Secure Message'});
    component.msgList=mockCommonService.getSessionStorageMsg();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
