import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MessageComponent } from './message.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from 'src/app/shared/shared/shared.module';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { APP_BASE_HREF } from '@angular/common';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateMessagesLoader } from 'src/app/shared/testing/translate.messages';
import { HttpClient } from '@angular/common/http';
import { JwPaginationComponent } from 'src/app/shared/components/jw-pagination/jw-pagination.component';

describe('MessageComponent', () => {
  let component: MessageComponent;
  let fixture: ComponentFixture<MessageComponent>;
  let translateService;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports:[
        BrowserAnimationsModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        MatDialogModule,
        RouterTestingModule,
        SharedModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useClass: TranslateMessagesLoader,
            deps: [HttpClient]
          }
        })
      ],
      declarations: [ MessageComponent,JwPaginationComponent ],
      providers:[
        {
          provide: APP_BASE_HREF,
          useValue: "/se"
        },
        TranslateService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageComponent);
    component = fixture.componentInstance;
    translateService = TestBed.get(TranslateService);
    translateService.use('en');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
