import { APP_BASE_HREF } from '@angular/common';
import { Component, Inject, OnChanges, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { throwError } from 'rxjs';
import { ToastsComponent } from 'src/app/shared/components/toasts/toasts.component';
import { CommonService } from 'src/app/shared/_service/common/common.service';
import { MessageService } from 'src/app/shared/_service/messages/messages.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {
  conuntryCode = 'se';
  messages = [];
  pageOfItems: Array<any>;
  ajaxActive: boolean = true;
  currentLang:string='';
  constructor(
    private messageService: MessageService,
    private translate: TranslateService,
    private commonService: CommonService,
    private snackBar:MatSnackBar,
    private cookieService:CookieService,
    @Inject(APP_BASE_HREF) private baseHref: string
  ) {
    this.conuntryCode=this.baseHref.split("/")[1];
    this.currentLang=this.conuntryCode=='se' ? 'sv' : 'fi';
    this.commonService.setErrorMsg('');
     // get localization
     this.commonService.getLanguage.subscribe((data:any) => {
      this.translate.use(data);
    });
  }

  ngOnInit() {
    this.getMessageConversion();
    this.setLocalization();
  }
  // set localization
  setLocalization() {
    this.translate.addLangs(['sv', 'en', 'fi']);
    if (this.translate.getBrowserLang() !== undefined) {
      let lang = this.cookieService.get('.AspNetCore.Culture');
      console.log(lang);
      if (lang) {
        var langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.currentLang=langCode;
        this.translate.use(langCode);
      }else {
        this.translate.use(this.currentLang);
      }
    }
    else {
      this.translate.use(this.currentLang);
    }
  }
  getMessageConversion() {
    this.messageService.getCollectionConversations().subscribe((response: any) => {
      if (response.result.resultCode === 0) {
        this.messages=response.data.debtorConversationsResult;
        this.ajaxActive=false;
      } else {
        this.commonService.setErrorMsg(response.result.resultText);
        this.ajaxActive=false;
        this.openErrorToast();
      }
    }, error => {
        let msgerror = this.translate.instant('technicalIssue');
        throwError(msgerror);
      this.ajaxActive=false;
        this.commonService.setErrorMsg(msgerror);
      this.openErrorToast()
    });
  }
  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }
  setMessage(obj){
    this.commonService.setSessionStorageMsg(obj)
  }
  //open error Toast Component
  openErrorToast()
  {
    this.snackBar.openFromComponent(ToastsComponent
      , {
      panelClass: 'error',
      horizontalPosition:  'center',
      verticalPosition: 'top',
      });
  }
}
