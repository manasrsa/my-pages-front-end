import { APP_BASE_HREF } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { CommonService } from 'src/app/shared/_service/common/common.service';

@Component({
  selector: 'app-nets-terms-of-delivery',
  templateUrl: './nets-terms-of-delivery.component.html',
  styleUrls: ['./nets-terms-of-delivery.component.scss']
})
export class NetsTermsOfDeliveryComponent implements OnInit {
  msg:string;
  str = '';
  currentLang: string = '';
  conuntryCode: string = 'se';
  constructor(private activateRouter: ActivatedRoute, 
    private translate: TranslateService,
    private commonService:CommonService,
    private cookieService: CookieService,
    @Inject(APP_BASE_HREF) private baseHref: string
  ) {
    this.conuntryCode = this.baseHref.split("/")[1];
    this.currentLang = this.conuntryCode == 'se' ? 'sv' : 'fi';
      this.commonService.getLanguage.subscribe((data:any) => {
        this.translate.use(data).subscribe(res=>{
          this.str=this.translate.instant('paytrail-plc-innova');
        });
      });
      this.commonService.setErrorMsg('');
    }

  ngOnInit(): void {
    this.msg = this.activateRouter.snapshot.paramMap.get('name');
    this.setLocalization();
    this.str = this.translate.instant('paytrail-plc-innova');
    
  }
  // set localization
  setLocalization() {
    this.translate.addLangs(['sv', 'en', 'fi']);
    if (this.translate.getBrowserLang() !== undefined) {
      let lang = this.cookieService.get('.AspNetCore.Culture');

      if (lang) {
        var langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.currentLang = langCode;
        this.translate.use(langCode);
      } else {
        this.translate.use(this.currentLang);
      }
    }
    else {
      this.translate.use(this.currentLang);
    }
  }

}
