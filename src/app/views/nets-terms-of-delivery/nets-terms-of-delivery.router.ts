import { Routes } from "@angular/router";
import { NetsTermsOfDeliveryComponent } from './nets-terms-of-delivery.component';

export const netTermOfDeliveryRoutes: Routes = [
    {
        path:':name',
        component:NetsTermsOfDeliveryComponent,
        runGuardsAndResolvers:'always',
    }
]