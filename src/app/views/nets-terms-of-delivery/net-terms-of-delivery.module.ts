import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { netTermOfDeliveryRoutes } from './nets-terms-of-delivery.router';
import { NetsTermsOfDeliveryComponent } from './nets-terms-of-delivery.component';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href.split('/')[3];
}
export function HttpLoaderFactory(httpClient: HttpClient) {
 const countryCode=getBaseUrl();
    return new TranslateHttpLoader(httpClient, './assets/i18n/'+countryCode+'/net-terms/', '.json');
  }
@NgModule({
    imports:[
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        TranslateModule.forChild({
            loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
            },
            isolate: true
          }),
        RouterModule.forChild(netTermOfDeliveryRoutes)
    ],
    declarations: [NetsTermsOfDeliveryComponent],
    exports:[NetsTermsOfDeliveryComponent],
    providers: [],
    schemas:[CUSTOM_ELEMENTS_SCHEMA]
})

export class NetTermsOfDeliveryModule{}