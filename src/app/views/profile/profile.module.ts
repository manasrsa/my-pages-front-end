import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { ProfileRoutes } from './profile.module.router';
import { ProfileComponent } from './profile.component';
import { MatTabsModule } from '@angular/material/tabs';
import { SharedModule } from 'src/app/shared/shared/shared.module';
export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href.split('/')[3];
}
export function HttpLoaderFactory(httpClient: HttpClient) {
    const countryCode=getBaseUrl();
    return new TranslateHttpLoader(httpClient, './assets/i18n/'+countryCode+'/profile/', '.json');
  }
@NgModule({
    imports:[
        CommonModule,
        RouterModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        MatTabsModule,
        TranslateModule.forChild({
            loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
            },
            isolate: true
          }),
        RouterModule.forChild(ProfileRoutes)
    ],
    declarations: [ProfileComponent],
    exports:[ProfileComponent],
    providers: [],
    entryComponents: [ProfileComponent],
    schemas:[CUSTOM_ELEMENTS_SCHEMA]
})

export class ProfileModule{}