import { Route, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PaymentOptionsComponent } from './payment-options/payment-options.component';
import { OverviewListComponent } from './overview-list/overview-list.component';
import { PostponementComponent } from './postponement/postponement.component';
import { PaymentComponent } from './payment/payment.component';
import { PlanapproveComponent } from './planapprove/planapprove.component';
import { ConfirmationOfPaymentComponent } from './confirmation-of-payment/confirmation-of-payment.component';
import { InformConfirmationComponent } from './inform-confirmation/inform-confirmation.component';
import { NetCompleteComponent } from './net-complete/net-complete.component';
import { NetErrrorComponent } from './net-errror/net-errror.component';
import { NetCancelComponent } from './net-cancel/net-cancel.component';
import { NetPaymentinprogressComponent } from './net-paymentinprogress/net-paymentinprogress.component';


export const overViewListRoutes: Route[] = [
  {
    path: '',
    component: OverviewListComponent,
    pathMatch: 'full',
  },
 
 
  {
    path:':id',
    component:PaymentComponent,
    data:{
      title: "titlePayments",
      breadcrumb: "",
      auth: 'Payment Options',
    },
    children: [
      {
          path:'',
          component: PaymentOptionsComponent,
          data: {
              title: 'titlePayments',
              breadcrumb: 'New message',
              auth: 'Payment Options'
          }
      },
      {
        path:'postponement',
        component:PostponementComponent,
        data:{
          title:'titlePostponey',
          breadcrumb:'breadcrumPostponeConfirmation',
          auth: 'Postponement',
          url:'overview/postponement'
        }
     },
      {
          path:'plan-approved',
          component: PlanapproveComponent,
          data: {
              title: 'breadcrumplanConfirmation',
              breadcrumb: 'breadcrumplanConfirmation',
              auth: 'Confirmation'
          }
      },
      {
        path:'swish-complete',
        component: ConfirmationOfPaymentComponent,
        data: {
            title: 'breadcrumMsgConfirmPayment',
            breadcrumb: 'breadcrumMsgConfirmPayment',
          name:'swish-complete',
            auth: 'Confirmation'
        }
      },
      {
        path: 'swish-decline',
        component: ConfirmationOfPaymentComponent,
        data: {
          title: 'breadcrumMsgDecline',
          breadcrumb: 'breadcrumMsgDecline',
          name: 'swish-decline',
          auth: 'Confirmation'
        }
      },
      {
        path: 'swish-cancel',
        component: ConfirmationOfPaymentComponent,
        data: {
          title: 'breadcrumMsgConfirmation',
          breadcrumb: 'breadcrumMsgConfirmation',
          name: 'swish-cancel',
          auth: 'Confirmation'
        }
      },
      {
        path: 'swish-timeout',
        component: ConfirmationOfPaymentComponent,
        data: {
          title: 'breadcrumMsgTimeout',
          breadcrumb: 'breadcrumMsgTimeout',
          name: 'swish-timeout',
          auth: 'Confirmation'
        }
      },
      {
        path: 'swish-error',
        component: ConfirmationOfPaymentComponent,
        data: {
          title: 'breadcrumMsgError',
          breadcrumb: 'breadcrumMsgError',
          name: 'swish-error',
          auth: 'Confirmation'
        }
      },
      {
        path: 'swish-bookpaymentfailed',
        component: ConfirmationOfPaymentComponent,
        data: {
          title: 'breadcrumMsgConfirmation',
          breadcrumb: 'breadcrumMsgConfirmation',
          name: 'swish-bookpaymentfailed',
          auth: 'Confirmation'
        }
      },
      {
        path: 'swish-paymentinprogress',
        component: ConfirmationOfPaymentComponent,
        data: {
          title: 'breadcrumSwishPaymentInProgressTitle',
          breadcrumb: 'breadcrumSwishPaymentInProgress',
          name: 'swish-paymentinprogress',
          auth: 'Confirmation'
        }
      },
    {
      path:'partial-payment',
      component:InformConfirmationComponent,
      data: {
        title: 'breadcrumMsgConfirmation',
        breadcrumb: 'breadcrumMsgConfirmation',
        auth: 'HP'
      }
    },
    {
      path:'full-payment',
      component:InformConfirmationComponent,
      data: {
        title: 'breadcrumMsgConfirmation',
        breadcrumb: 'breadcrumMsgConfirmation',
        auth: 'FP'
      }
    },
    {
      path:'net-complete',
      component:NetCompleteComponent,
      data: {
        title: 'breadcrumMsgConfirmation',
        breadcrumb: 'breadcrumMsgConfirmation',
        auth: 'net-complete'
      }
    },
    {
      path:'net-cancel',
      component:NetCancelComponent,
      data: {
        title: 'breadcrumMsgNetCancel',
        breadcrumb: 'breadcrumMsgNetCancel',
        auth: 'net-cancel'
      }
      },
      {
        path: 'net-paymentinprogress',
        component: NetPaymentinprogressComponent,
        data: {
          title: 'breadcrumMsgNetPaymentinProgress',
          breadcrumb: 'breadcrumMsgNetPaymentinProgress',
          auth: 'net-paymentinprogress'
        }
      },
    {
      path:':error-name',
      component:NetErrrorComponent,
      data: {
        title: 'breadcrumMsgConfirmation',
        breadcrumb: 'breadcrumMsgConfirmation',
        auth: 'net-error'
      }
    }
    


  ]
  },
  
];

