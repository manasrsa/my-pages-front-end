import { APP_BASE_HREF } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { BreadrumService } from 'src/app/shared/_service/breadcrum/breadcrum.service';
import { CommonService } from 'src/app/shared/_service/common/common.service';

@Component({
  selector: 'app-net-errror',
  templateUrl: './net-errror.component.html',
  styleUrls: ['./net-errror.component.scss']
})
export class NetErrrorComponent implements OnInit {
  conuntryCode:string='se';
  currentLang:string='se';
  errorMsg:string='';
  breadCrumData:any='';
  constructor(
    private activateRouter:ActivatedRoute,
    private breadCrumService:BreadrumService,
    private commonService:CommonService,
    private translate:TranslateService,
    private cookieService:CookieService,
    @Inject(APP_BASE_HREF) private baseHref: string
    ) { 
      this.commonService.setErrorMsg('');
      this.conuntryCode=this.baseHref.split("/")[1];
      this.currentLang=this.conuntryCode=='se' ? 'sv' : 'fi';
    this.breadCrumService.getBreadcrum().subscribe((response: any) => {
      this.breadCrumData = response.result;
    });
       // get localization
       this.commonService.getLanguage.subscribe((data:any) => {
        this.translate.use(data).subscribe(res=>{
         let errorCode= this.activateRouter.snapshot.paramMap.get('error-name');
          console.log(errorCode);
          if(errorCode=='001'){
            this.errorMsg=this.translate.instant('netPaymentErrorBackend');
          }
          else if(errorCode=='002'){
            this.errorMsg=this.translate.instant('netPaymentErrorConnecting');
          }
          else if(errorCode=='003'){
            this.errorMsg=this.translate.instant('netPaymentErrorGeneric');
          }
          else if(errorCode=='004'){
            this.errorMsg=this.translate.instant('netPaymentErrorReceive');
          }
        });
      });
   }

  ngOnInit() {
    this.setLocalization();
    this.setBreadCrumd();
  }
  // set localization
  setLocalization() {
    this.translate.addLangs(['sv', 'en', 'fi']);
    if (this.translate.getBrowserLang() !== undefined) {
      let lang = this.cookieService.get('.AspNetCore.Culture');
      
      if (lang) {
        var langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.currentLang=langCode;
        this.translate.use(langCode);
      }else {
        this.translate.use(this.currentLang);
      }
    }
    else {
      this.translate.use(this.currentLang);
    }
  }
  //set localisation
  setBreadCrumd(){
    const breadcrum:any=JSON.parse(this.commonService.getBreadCrumDynamic());
    if(this.breadCrumData){
      this.breadCrumData.map((obj, index) => {
        if (obj.params.id) {
          if(obj){
            this.breadCrumData[index].breadcrumb = breadcrum.data;
          }
        }
    });
    }
  }


}
