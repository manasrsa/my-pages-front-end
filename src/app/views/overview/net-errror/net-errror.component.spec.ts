import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NetErrrorComponent } from './net-errror.component';

describe('NetErrrorComponent', () => {
  let component: NetErrrorComponent;
  let fixture: ComponentFixture<NetErrrorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NetErrrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NetErrrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
