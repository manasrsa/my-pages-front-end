import { APP_BASE_HREF } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateOverviewLoader } from 'src/app/shared/testing/translate.overview';

import { PlanapproveComponent } from './planapprove.component';

describe('PlanapproveComponent', () => {
  let component: PlanapproveComponent;
  let fixture: ComponentFixture<PlanapproveComponent>;
  let translateService;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanapproveComponent ],
      imports:[
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useClass: TranslateOverviewLoader,
            deps: [HttpClient]
          }
        })
      ],
      providers: [
        {
          provide: ActivatedRoute, useValue: {
            firstChild:{ snapshot: 
             {params: {'Id': '1'},data:{auth:''}
     }}}
        },
        TranslateService,
        {
          provide: APP_BASE_HREF,
          useValue: "/se"
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanapproveComponent);
    component = fixture.componentInstance;
    translateService = TestBed.get(TranslateService);
    translateService.use('en');
    
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
