import { APP_BASE_HREF } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { BreadrumService } from 'src/app/shared/_service/breadcrum/breadcrum.service';
import { CommonService } from 'src/app/shared/_service/common/common.service';
import { OverviewService } from 'src/app/shared/_service/overview/overview.service';

@Component({
  selector: 'app-planapprove',
  templateUrl: './planapprove.component.html',
  styleUrls: ['./planapprove.component.scss']
})
export class PlanapproveComponent implements OnInit {
  breadCrumData:any='';
  countrycode:string='se';
  currentLang:string='';
  constructor(
    public breadCrumService: BreadrumService,
    private overviewService: OverviewService,
    private translate: TranslateService,
    private commonService: CommonService,
    private activateRoute: ActivatedRoute,
    private cookieService:CookieService,
    @Inject(APP_BASE_HREF) private baseHref: string,
  ) {
    this.commonService.setErrorMsg('');
    this.countrycode = this.baseHref.split('/')[1];
    this.currentLang=this.countrycode=='se' ? 'sv' : 'fi';
     // get localization
     this.commonService.getLanguage.subscribe((data:any) => {
      
      this.translate.use(data);
    });
    this.breadCrumService.getBreadcrum().subscribe((response: any) => {
      this.breadCrumData = response.result;
    });
   }

  ngOnInit() {
    this.setLocalization();
    this.setBreadCrumd();
  }
  setBreadCrumd(){
    const breadcrum:any=JSON.parse(this.commonService.getBreadCrumDynamic());
    if(this.breadCrumData){
      this.breadCrumData.map((obj, index) => {
        if (obj.params.id) {
          if(obj){
            this.breadCrumData[index].breadcrumb = breadcrum.data;
          }
        }
    });
    }
   
  }
   // set localization
   setLocalization() {
    this.translate.addLangs(['sv', 'en', 'fi']);
    if (this.translate.getBrowserLang() !== undefined) {
      let lang = this.cookieService.get('.AspNetCore.Culture');
     
      if (lang) {
        var langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.currentLang=langCode;
        this.translate.use(langCode);
      }else {
        this.translate.use(this.currentLang);
      }
    }
    else {
      this.translate.use(this.currentLang);
    }
  }
}
