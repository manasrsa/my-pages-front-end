import { Component, Inject, OnInit } from '@angular/core';

import { throwError } from 'rxjs';
import { OverviewService } from '../../../shared/_service/overview/overview.service';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from 'src/app/shared/_service/common/common.service';
import { DomSanitizer } from '@angular/platform-browser';
import { style } from '@angular/animations';
import { Router } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';
import { ToastsComponent } from 'src/app/shared/components/toasts/toasts.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-overview-list',
  templateUrl: './overview-list.component.html',
  styleUrls: ['./overview-list.component.scss']
})
export class OverviewListComponent implements OnInit {
  current = 0;
  accordianFirst = false;
  // tslint:disable-next-line: ban-types
  state: String = 'inactive';
  openCaseSummaries: any = [];
  OpenCaseCount = '';
  totalDebtToPay = 0;
  openCaseLength = 0;
  closedCaseSummaries: any = [];
  // tslint:disable-next-line: variable-name
  size_li = 0;
  sizeMinLength = 5;
  sizeMinLengthClosedCase = 5;
  alreadyPaidContent = 'please-note-that-it-takes';
  alreadyPaidActiveReadMore = false;
  readAlreadyMore = true;
  contentInvoice = 'you-have-8-banking-days-in-which-to';
  contentInvoiceActiveReadMore = false;
  readMore = true;

  debtSpecification: any = [];
  ajaxActive = 'ajax-';
  errorActive = 'error-';
  currency = 'kr'; // '€'
  greetingValue = '';
  conuntryCode = 'se';
  ajaxActiveVal = false;
  technicalError = "";
  currentLang: string = '';
  noOpenCase = 'overviewNoOpenCaseLabelText';

  constructor(
    private overviewService: OverviewService,
    public matDialog: MatDialog,
    private translate: TranslateService,
    private commonService: CommonService,
    private router: Router,
    sanitizer: DomSanitizer,
    private cookieService: CookieService,
    private snackBar: MatSnackBar,
    @Inject(APP_BASE_HREF) private baseHref: string,

  ) {
    this.commonService.setErrorMsg('');
    this.conuntryCode = this.baseHref.split("/")[1];
    this.currentLang = this.conuntryCode == 'se' ? 'sv' : 'fi';
    this.currency = this.conuntryCode == 'se' ? 'kr' : '€';
    // get localization
    this.commonService.getLanguage.subscribe((data: any) => {
      this.translate.use(data).subscribe(res => {
        this.technicalError = this.translate.instant('there-was-a-technical-error');
        this.noOpenCase = this.translate.instant('overviewNoOpenCaseLabelText');
        this.greeting();
      });
    });
  }

  ngOnInit() {

    // call set localization
    this.setLocalization();
    // call the overview method
    this.getOverViewData();
    this.greeting();
  }


  // invoice content show more and less more
  invoiceContent(textContent) {
    let contentInvoice = textContent;
    const fullText = textContent;

    if (fullText.length > 180) {
      
      this.contentInvoiceActiveReadMore = true;
      const subText = fullText.substring(0, 35).split(' ').slice(0, -1).join(' ');
      const blurText = fullText.substring(subText.length, subText.length + 40).split(' ').slice(0, -1).join(' ');
      const shortText = subText + '<span class="read-more-blur">' + blurText + '...</span>';
      contentInvoice = this.readMore ? shortText : fullText;
    }
    else {
      this.contentInvoiceActiveReadMore = false;
    }
    return contentInvoice;
  }
  // allready paid show more and less more
  allreadyPaid(content) {

    let contentInvoice = content;
    const fullText = content;

    if (fullText.length > 180) {
     
      this.alreadyPaidActiveReadMore = true;
      const subText = fullText.substring(0, 35).split(' ').slice(0, -1).join(' ');
      const blurText = fullText.substring(subText.length, subText.length + 40).split(' ').slice(0, -1).join(' ');
      const shortText = subText + '<span class="read-more-blur">' + blurText + '...</span>';
      contentInvoice = this.readAlreadyMore ? shortText : fullText;
    }
    else {
      this.alreadyPaidActiveReadMore = false;
    }
    return contentInvoice;
  }
  // set localization
  setLocalization() {
    this.translate.addLangs(['sv', 'en', 'fi']);
    if (this.translate.getBrowserLang() !== undefined) {
      let lang = this.cookieService.get('.AspNetCore.Culture');

      if (lang) {
        var langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.currentLang = langCode;
        this.translate.use(langCode);
      } else {
        this.translate.use(this.currentLang);
      }
    }
    else {
      this.translate.use(this.currentLang);
    }
  }
  // get overview Data
  getOverViewData() {
    //this.ajaxActive = 'ajax-active';
    this.ajaxActiveVal = true;
    this.commonService.notifySpinnerAction(true);
    this.overviewService.getOverView().subscribe((response: any) => {

      if (response.result.resultCode === 0) {

        this.openCaseSummaries = response.data.openCases;
        this.OpenCaseCount = response.data.openCaseCount;
        this.totalDebtToPay = response.data.totalDebtToPay;
        //if(this.openCaseSummaries){
        this.openCaseSummaries.map((obj, index) => {
          this.openCaseSummaries.debtSpecification = '';

        });
        this.openCaseLength = this.openCaseSummaries.length;
        //}


        this.closedCaseSummaries = response.data.closedCases;
        this.ajaxActive = 'ajax-';
        this.ajaxActiveVal = false;
        if (this.openCaseSummaries.length == 0) {
          this.commonService.setPoa(true);
        }
       
        this.commonService.notifySpinnerAction(false);
      } else {
        this.ajaxActive = 'ajax-';
        this.ajaxActiveVal = false;

        this.commonService.setErrorMsg(response.result.resultText);
        this.openErrorToast();
        this.commonService.setPoa(true);
        this.commonService.notifySpinnerAction(false);
      }

    }, error => {
        let msgerror = this.translate.instant('technicalIssue');
        this.commonService.setPoa(true);
        throwError(msgerror);
      this.commonService.notifySpinnerAction(false);
    });
  }
  // accordian method get the value

  accordian(index, caseGuid) {
    if (this.openCaseSummaries[index].debtSpecification == undefined) {
      this.getCaseDetails(index, caseGuid);
    }
    const chevronContainer = document.querySelectorAll('.chevron');
    chevronContainer.forEach((el, i) => { if (i === index) { el.classList.toggle('down'); } else { el.classList.remove('down'); } });

    const openElaborateContainer = document.querySelectorAll('.open-elaborate');
    openElaborateContainer.forEach((el, i) => {
      if (i === index) { el.classList.toggle('in'); } else { el.classList.remove('in'); }
    });

  }
  // sub accordian method
  subAccordian(index) {

    const chevronContainer = document.querySelectorAll('.arrow-down');
    chevronContainer.forEach((el, i) => { if (i === index) { el.classList.toggle('down'); } else { el.classList.remove('down'); } });
    const openElaborateSecondContainer = document.querySelectorAll('.open-elaborate-second');
    openElaborateSecondContainer.forEach((el, i) => { if (i === index) { el.classList.toggle('in'); } else { el.classList.remove('in'); } });
  }
  getCaseDetails(index, caseNumber) {
    this.ajaxActive = this.ajaxActive + caseNumber;
    this.overviewService.getCaseDetails(caseNumber).subscribe((res: any) => {
      if (res.result.resultCode == 0) {
        this.openCaseSummaries[index].debtSpecification = res.data;
        this.ajaxActive = 'ajax-';
      } else {
        this.errorActive = 'error-' + caseNumber;
        this.ajaxActive = 'ajax-';
        this.technicalError = res.result.resultText;
        // this.commonService.setErrorMsg(response.result.resultText);
      }
    }, error => {
      this.errorActive = 'error-' + caseNumber;
        this.ajaxActive = 'ajax-';
        let msgerror = this.translate.instant('technicalIssue');
        throwError(msgerror);
    });
  }

  // get date time
  greeting() {
    this.overviewService.getGreeting().subscribe((res: any) => {
      if (res.result.resultCode == 0) {
        this.greetingValue = res.data;
      } else {
        this.commonService.setErrorMsg(res.result.resultText);
        this.openErrorToast();
      }
    }, error => {
        let msgerror = this.translate.instant('technicalIssue');
        throwError(msgerror);
        this.commonService.setErrorMsg(msgerror);
      this.openErrorToast();
    });

  }
  //open error Toast Component
  openErrorToast() {
    this.snackBar.openFromComponent(ToastsComponent
      , {
        panelClass: 'error',
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
  }
  // open modal box have already paid
  openAlreadyPaidModal() {
    const modalName = 'overview';
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.id = 'modal-component';
    dialogConfig.height = 'auto';
    dialogConfig.width = '600px';
    dialogConfig.data = {
      name: 'having-paid',
      title: 'What if I have already paid?',
      bodyContent: '',
      cancelButtonText: '',
      actionButtonText: 'Got it',
      modalName
    };
    if (this.conuntryCode == 'se') {
      dialogConfig.data.actionButtonText = this.translate.instant('overviewWhatIfModelButtonText');
      dialogConfig.data.bodyContent = '<h3>' + this.translate.instant('what-if-i-have-already-paid') + '</h3><p>' + this.translate.instant('keep-in-mind-that-it-takes-between') + '</p><div>' + this.translate.instant('do-you-want-to-inform-about-a-payment-please-send-us') + '<a class="routerlink text-underline" style="cursor:pointer">' + this.translate.instant('a-message') + '</a></div>';

    } else {
      dialogConfig.data.actionButtonText = this.translate.instant('overviewWhatIfModelButtonText');
      dialogConfig.data.bodyContent = '<h3>' + this.translate.instant('what-if-i-have-already-paid') + '</h3><p>' + this.translate.instant('keep-in-mind-that-it-takes-between') + '</p><p><strong>' + this.translate.instant('if-you-want-a-confirmation') + '</strong></p><ul><li>&nbsp;' + this.translate.instant('please-check-your-my-pages') + '</li></ul>' + this.translate.instant('of-course-you-are-also') + '';
    }
    const modalDialog = this.matDialog.open(ModalComponent, dialogConfig);
  }


}
