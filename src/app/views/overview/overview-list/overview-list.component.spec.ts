/* tslint:disable:no-unused-variable */
import { ComponentFixture, TestBed, fakeAsync, tick, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { OverviewListComponent } from './overview-list.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialogModule, MatDialogConfig } from '@angular/material/dialog';
import { TranslateModule, TranslateLoader, TranslateService, TranslateFakeLoader } from '@ngx-translate/core';
import { of, Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpLoaderFactory } from '../overview.module';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { OverviewService } from 'src/app/shared/_service/overview/overview.service';
import { SharedModule } from 'src/app/shared/shared/shared.module';
import { APP_BASE_HREF } from '@angular/common';
import { TranslateOverviewLoader } from 'src/app/shared/testing/translate.overview';

describe('OverviewListComponent', () => {
  let component: OverviewListComponent;
  let fixture: ComponentFixture<OverviewListComponent>;
  let mockOverviewService;
  let translateService;
  const mockGretting={data:'Good Afternoon',result: {resultCode: 0, resultText: 'Ok'}}
  const mockOverViewData = {
    "data": {
      "openCases": [
        {
          "caseNumber": "90265756",
          "clientName": "Sergel Finans AB (Övertag från Tele2 Sverige AB)",
          "closureDate": null,
          "amountToPay": 301.25,
          "caseGuid": "1629178A-65B4-4F97-AD16-864F23F394CB"
        },
        {
          "caseNumber": "90454140",
          "clientName": "Sergel Finans AB (Övertag från Tele2 Sverige AB)",
          "closureDate": null,
          "amountToPay": 455,
          "caseGuid": "A268C3E2-6E58-45C8-BD05-9D8F016CD654"
        },
        {
          "caseNumber": "90454141",
          "clientName": "Sergel Finans ABB (Övertag från Tele2 Sverige AB)",
          "closureDate": null,
          "amountToPay": 465,
          "caseGuid": "A268C3E2-6E58-45C8-BD05-9D8F016CD655"
        },
        {
          "caseNumber": "90454142",
          "clientName": "Sergel Finans ABB (Övertag från Tele2 Sverige AB)",
          "closureDate": null,
          "amountToPay": 466,
          "caseGuid": "A268C3E2-6E58-45C8-BD05-9D8F016CD656"
        },
        {
          "caseNumber": "90454143",
          "clientName": "Sergel Finans ABB (Övertag från Tele2 Sverige AB)",
          "closureDate": null,
          "amountToPay": 467,
          "caseGuid": "A268C3E2-6E58-45C8-BD05-9D8F016CD657"
        },
        {
          "caseNumber": "90454144",
          "clientName": "Sergel Finans ABB (Övertag från Tele2 Sverige AB)",
          "closureDate": null,
          "amountToPay": 468,
          "caseGuid": "A268C3E2-6E58-45C8-BD05-9D8F016CD658"
        }
      ],
      "closedCases": [
        {
          "caseNumber": "84573767",
          "clientName": "Sergel Finans AB (Övertag från Tele2 Sverige AB)",
          "closureDate": "2020-06-30T00:00:00",
          "amountToPay": 0,
          "caseGuid": "8366DD13-02D5-4EF6-88DA-7AACFA76A816"
        },
        {
          "caseNumber": "84535016",
          "clientName": "Sergel Finans AB (Övertag från Tele2 Sverige AB)",
          "closureDate": "2020-06-30T00:00:00",
          "amountToPay": 0,
          "caseGuid": "B10BFAA2-DCEE-498A-9D30-F177019122B3"
        },
        {
          "caseNumber": "83630540",
          "clientName": "Sergel Portfolio AB (Övertag från ComHem AB)",
          "closureDate": "2020-06-30T00:00:00",
          "amountToPay": 0,
          "caseGuid": "9701B88D-3F77-4383-B6F4-714A7AD6E5AE"
        },
        {
          "caseNumber": "83613007",
          "clientName": "Sergel Portfolio AB (Övertag från ComHem AB)",
          "closureDate": "2020-06-30T00:00:00",
          "amountToPay": 0,
          "caseGuid": "346FEDBE-F5FD-428B-87A4-C2487F0E2727"
        },
        {
          "caseNumber": "90105348",
          "clientName": "Tele2 Sverige AB",
          "closureDate": "2020-05-29T00:00:00",
          "amountToPay": 0,
          "caseGuid": "49245199-A07D-496E-88DA-CD32DC425C05"
        },
        {
          "caseNumber": "84824467",
          "clientName": "Tele2 Sverige AB",
          "closureDate": "2020-03-30T00:00:00",
          "amountToPay": 0,
          "caseGuid": "470A6EB4-83EB-44DD-959C-26BCA3AD870E"
        }
      ],
      "openCaseCount": 6,
      "closedCaseCount": 6,
      "totalDebtToPay": 756.25
    },
    "result": {
      "resultCode": 0,
      "resultText": null
    }
  };
  const mockGetDebt={
    "data": {
      "caseNumber": "90265756",
      "caseGuid": "1629178A-65B4-4F97-AD16-864F23F394CB",
      "clientName": "Sergel Finans AB (Övertag från Tele2 Sverige AB)",
      "isInstallmentPlanAllowed": true,
      "paymentPlanAllowed": true,
      "paymentPlanMaxNumOfOccasions": 6,
      "debtCollectionFee": 180,
      "interest": 10.25,
      "paidAmount": 0,
      "respiteMaxDaysAllowed": 0,
      "status": "Efterbevakning",
      "debtToPay": 301.25,
      "debts": [
        {
          "invoiceAmount": 111,
          "invoiceNumber": "1018889261039",
          "groundsOfClaim": "Tele tjänster",
          "dueDate": "2019-10-29T00:00:00"
        }
      ]
    },
    "result": {
      "resultCode": 0,
      "resultText": "Ok"
    }
  }
  beforeEach(waitForAsync(() => {
    mockOverviewService = jasmine.createSpyObj('mockOverviewService',['getOverView','getGreeting','getCaseDetails']);
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        MatDialogModule,
        RouterTestingModule,
        SharedModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useClass: TranslateOverviewLoader,
            deps: [HttpClient]
          }
        })
      ],
      declarations: [ OverviewListComponent ],
      providers: [
        TranslateService,
        {provide: OverviewService, useValue: mockOverviewService},
        {
          provide: APP_BASE_HREF,
          useValue: "/se"
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewListComponent);
    component = fixture.componentInstance;
    component.conuntryCode='se';
    component.contentInvoiceActiveReadMore=false;
    translateService = TestBed.get(TranslateService);
    translateService.use('en');
    component.sizeMinLength=5;
    component.openCaseSummaries=mockOverViewData.data;
    mockOverviewService.getGreeting.and.returnValue(of(mockGretting))
    mockOverviewService.getOverView.and.returnValue(of(mockOverViewData));
    mockOverviewService.getCaseDetails.and.returnValue(of(mockGetDebt));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  // should call the getOverview Data for open case summary
  it('should call getOverViewData', () => {
    component.conuntryCode='se';
    mockOverviewService.getOverView.and.returnValue(of(mockOverViewData));
    component.ngOnInit();
    fixture.detectChanges();
   // expect(component.openCaseSummaries.length).toEqual(mockOverViewData.data.openCaseSummaries.length);
  });
  it('should check for the getOverViewData error call ', () => {
    mockOverviewService.getOverView.and.returnValue(throwError('Some issues for fetching the overview data'));
    component.getOverViewData();
    expect(mockOverviewService.getOverView).toHaveBeenCalled();
  });
  // should open case with show more button
  it('should have open case with show more button', () => {
    mockOverviewService.getOverView.and.returnValue(of(mockOverViewData));
    const openTitleDE = fixture.debugElement.query(By.css('.open-title'));
    expect(openTitleDE.nativeElement.textContent).toContain('Open cases');
    const openCasesDE = fixture.debugElement.query(By.css('.open-cases'));
    fixture.detectChanges();
    expect(openCasesDE.nativeElement.textContent).toContain('(6)');
    const  openCaseTitle = fixture.debugElement.queryAll(By.css('.open-case-title div'));
    expect(openCaseTitle[0].nativeElement.textContent).toContain('CREDITOR');
    expect(openCaseTitle[1].nativeElement.textContent).toContain('Amount to pay');
    const showMoreDe = fixture.debugElement.queryAll(By.css('.show-more'));
    expect(showMoreDe[0].nativeElement.textContent).toContain('Show more');
  });
  // should open case when less than without show more button
  it('should have open case when open case less than 5 without show more button', () => {
    mockOverviewService.getOverView.and.returnValue(of(mockOverViewData));
    component.getOverViewData();
    expect(component.openCaseSummaries).toEqual(mockOverViewData.data.openCases);
    component.openCaseSummaries = mockOverViewData.data.openCases.slice(0, 5);
    fixture.detectChanges();
    const openCasesDE = fixture.debugElement.query(By.css('.open-cases'));
    expect(openCasesDE.nativeElement.textContent).toContain('(6)');
    expect(component.openCaseSummaries.length).toEqual(5);
  });
  // should render the accordian li
  it('should render the accordian li ', () => {
    mockOverviewService.getOverView.and.returnValue(of(mockOverViewData));
    fixture.detectChanges();
    const accordianDE = fixture.debugElement.queryAll(By.css('#accordion li'));
    expect(accordianDE.length).toEqual(mockOverViewData.data.openCases.length - 1);
  });
  // should call the oepn accordian
  it('should open accordian ', fakeAsync (() => {
    mockOverviewService.getOverView.and.returnValue(of(mockOverViewData));
    mockOverviewService.getCaseDetails.and.returnValue(of(mockGetDebt));
    fixture.detectChanges();
    spyOn(component, 'accordian');
    const accordianDE = fixture.debugElement.queryAll(By.css('.chevron'));
    accordianDE[0].triggerEventHandler('click', null);
    tick();
    component.accordian(0,0);
    fixture.detectChanges();
    expect(component.accordian).toHaveBeenCalled();
  }));
  // should open first accordian after call sub accordian
  it('should open accordian  for sub accordian ', fakeAsync (() => {
    mockOverviewService.getOverView.and.returnValue(of(mockOverViewData));
    fixture.detectChanges();
    
    
    // spyOn(component,"accordian");
    const accordianDE = fixture.debugElement.queryAll(By.css('#accordion .first_accordian'));
    accordianDE[0].triggerEventHandler('click', null);
    component.getCaseDetails(0,90265756);
    mockOverviewService.getCaseDetails.and.returnValue(of(mockGetDebt));
    spyOn(component, 'subAccordian');
    component.accordian(0,0);
    const subaccordianDE = fixture.debugElement.queryAll(By.css('#accordion .arrow-down'));
    subaccordianDE[0].triggerEventHandler('click', null);
    tick();
    component.subAccordian(0);
    fixture.detectChanges();
    expect(component.subAccordian).toHaveBeenCalled();
  }));
  // should call the subaccordian
  it('should call the subaccordian', fakeAsync(() => {
    const spy = spyOn(component, 'subAccordian').and.callThrough();
    component.subAccordian(0);
    expect(component).toBeDefined();
    expect(spy);
   }));
   // should have closed cases
  it('should have closed cases', () => {
     const closedTitleDE = fixture.debugElement.query(By.css('.closed-h2'));
     expect(closedTitleDE.nativeElement.textContent).toContain('Closed cases');
     const closedDate = fixture.debugElement.query(By.css('.closing-date'));
     expect(closedDate.nativeElement.textContent).toContain('CLOSING DATE');
     const creditorDE = fixture.debugElement.query(By.css('.creditor'));
     expect(creditorDE.nativeElement.textContent).toContain('CREDITOR');
     const casenumberDE = fixture.debugElement.query(By.css('.case-number'));
     expect(casenumberDE.nativeElement.textContent).toContain('CASE NUMBER');
    });
    // should calosed cases render
 
});
