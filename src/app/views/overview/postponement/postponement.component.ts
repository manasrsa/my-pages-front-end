import { APP_BASE_HREF } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { StringFormat } from 'src/app/shared/modal/global';
import { BreadrumService } from 'src/app/shared/_service/breadcrum/breadcrum.service';
import { CommonService } from 'src/app/shared/_service/common/common.service';

@Component({
  selector: 'app-postponement',
  templateUrl: './postponement.component.html',
  styleUrls: ['./postponement.component.scss']
})
export class PostponementComponent implements OnInit {
  days: any = '';
  daysMsg='';
  countrycode:string='se';
  currentLang:string='';
  breadCrumData:any='';
  constructor(private commonService: CommonService,
              private translate: TranslateService,
              private router:Router,
              public breadCrumService: BreadrumService,
              private cookieService:CookieService,
              @Inject(APP_BASE_HREF) private baseHref: string,
              ) {
                this.commonService.setErrorMsg('');
                this.countrycode = this.baseHref.split('/')[1];
                this.currentLang=this.countrycode=='se' ? 'sv' : 'fi';
                this.commonService.getDays.subscribe(res => {
                  this.days = res;
                });
    
    // get localization
    this.commonService.getLanguage.subscribe((data:any) => {
      this.translate.use(data).subscribe(res=>{
        this.setPostponeMsg();
      });
    });
    this.breadCrumService.getBreadcrum().subscribe((response: any) => {
      this.breadCrumData = response.result;
    });
   }

  ngOnInit(): void {
    this.setLocalization();
    this.setBreadCrumd();
    this.setPostponeMsg();
  }
  setBreadCrumd(){
    const breadcrum:any=JSON.parse(this.commonService.getBreadCrumDynamic());
    if(this.breadCrumData){
      this.breadCrumData.map((obj, index) => {
        if (obj.params.id) {
          if(obj){
            this.breadCrumData[index].breadcrumb = breadcrum.data;
          }
        }
    });
    }
   
  }
  //set translation postpone payment msg
  setPostponeMsg(){
    let days:any=JSON.parse(sessionStorage.getItem('days'));
    if(!days)
    {
      this.router.navigateByUrl('/overview')
    }else{
      this.daysMsg=StringFormat(this.translate.instant('postponeMsgLabelText'),days.postponepayment)
      
    }
  }

  // set localization
  setLocalization() {
    this.translate.addLangs(['sv', 'en', 'fi']);
    if (this.translate.getBrowserLang() !== undefined) {
      let lang = this.cookieService.get('.AspNetCore.Culture');
     
      if (lang) {
        var langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.currentLang=langCode;
        this.translate.use(langCode);
      }else {
        this.translate.use(this.currentLang);
      }
    }
    else {
      this.translate.use(this.currentLang);
    }
  }
}
