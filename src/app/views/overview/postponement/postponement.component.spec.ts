import { APP_BASE_HREF } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateOverviewLoader } from 'src/app/shared/testing/translate.overview';

import { PostponementComponent } from './postponement.component';

describe('PostponementComponent', () => {
  let component: PostponementComponent;
  let fixture: ComponentFixture<PostponementComponent>;
  let translateService;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PostponementComponent ],
      imports:[
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useClass: TranslateOverviewLoader,
            deps: [HttpClient]
          }
        })
      ],
      providers: [
        TranslateService,
        {
          provide: APP_BASE_HREF,
          useValue: "/se"
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostponementComponent);
    component = fixture.componentInstance;
    translateService = TestBed.get(TranslateService);
    translateService.use('en');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
