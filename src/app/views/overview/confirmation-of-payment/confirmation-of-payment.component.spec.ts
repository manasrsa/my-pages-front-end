import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

import { ConfirmationOfPaymentComponent } from './confirmation-of-payment.component';

describe('ConfirmationOfPaymentComponent', () => {
  let component: ConfirmationOfPaymentComponent;
  let fixture: ComponentFixture<ConfirmationOfPaymentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports:[RouterTestingModule,
        TranslateModule.forRoot(),HttpClientTestingModule],
      declarations: [ ConfirmationOfPaymentComponent ],
      providers:[
        TranslateService,
        {
          provide: APP_BASE_HREF,
          useValue: "/se"
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationOfPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
