import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NetPaymentinprogressComponent } from './net-paymentinprogress.component';

describe('NetPaymentinprogressComponent', () => {
  let component: NetPaymentinprogressComponent;
  let fixture: ComponentFixture<NetPaymentinprogressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NetPaymentinprogressComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NetPaymentinprogressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
