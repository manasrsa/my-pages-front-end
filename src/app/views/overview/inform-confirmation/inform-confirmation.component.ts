import { APP_BASE_HREF } from '@angular/common';
import { Inject } from '@angular/core';
import { Component,  OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { BreadrumService } from 'src/app/shared/_service/breadcrum/breadcrum.service';
import { CommonService } from 'src/app/shared/_service/common/common.service';

@Component({
  selector: 'app-inform-confirmation',
  templateUrl: './inform-confirmation.component.html',
  styleUrls: ['./inform-confirmation.component.scss']
})
export class InformConfirmationComponent implements OnInit {
  breadCrumData:any='';
  confirmationMsg:string='';
  conuntryCode:string='';
  currentLang:string='';
  constructor(
    private breadCrumService:BreadrumService,
    private commonService:CommonService,
    private translate:TranslateService,
    private activateRoute:ActivatedRoute,
    private cookieService:CookieService,
    @Inject(APP_BASE_HREF) private baseHref: string
    ) { 
      this.commonService.setErrorMsg('');
      this.conuntryCode=this.baseHref.split("/")[1];
      this.currentLang=this.conuntryCode=='se' ? 'sv' : 'fi';
      this.breadCrumService.getBreadcrum().subscribe((response: any) => {
        this.breadCrumData = response.result;
      });
       // get localization
       this.commonService.getLanguage.subscribe((data:any) => {
        this.translate.use(data).subscribe(res=>{
          this.setConfirmationMsg();
           
        });
      });
   }

  ngOnInit() {
    this.setLocalization();
    this.setBreadCrumd();
    this.setConfirmationMsg();
  }
  setConfirmationMsg(){
    let payment_mode=this.activateRoute.snapshot.data.auth;
   
        if(payment_mode==='FP'){
         this.confirmationMsg= this.translate.instant('informconfiramtionFpText');
         
        }
        else{
          this.confirmationMsg=this.translate.instant('informconfiramtionHpText') ;
         
        }
  }
  // set localization
  setLocalization() {
    this.translate.addLangs(['sv', 'en', 'fi']);
    if (this.translate.getBrowserLang() !== undefined) {
      let lang = this.cookieService.get('.AspNetCore.Culture');
     
      if (lang) {
        var langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.currentLang=langCode;
        this.translate.use(langCode);
      }else {
        this.translate.use(this.currentLang);
      }
    }
    else {
      this.translate.use(this.currentLang);
    }
  }
  setBreadCrumd(){
    const breadcrum:any=JSON.parse(this.commonService.getBreadCrumDynamic());
    if(this.breadCrumData){
      this.breadCrumData.map((obj, index) => {
        if (obj.params.id) {
          if(obj){
            this.breadCrumData[index].breadcrumb = breadcrum.data;
          }
        }
    });
    }
  }
}
