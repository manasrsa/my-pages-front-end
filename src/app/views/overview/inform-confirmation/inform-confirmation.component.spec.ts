import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InformConfirmationComponent } from './inform-confirmation.component';

describe('InformConfirmationComponent', () => {
  let component: InformConfirmationComponent;
  let fixture: ComponentFixture<InformConfirmationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InformConfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
