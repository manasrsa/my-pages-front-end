import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
// import { overViewListRoutes } from './overview-list/overview-list.routing';
import { PaymentOptionsComponent } from './payment-options/payment-options.component';
import { overViewListRoutes } from './overview.module.routing';
import { OverviewListComponent } from './overview-list/overview-list.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SharedModule } from 'src/app/shared/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PostponementComponent } from './postponement/postponement.component';
import { MatInputModule } from '@angular/material/input';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { PaymentComponent } from './payment/payment.component';
import { PlanapproveComponent } from './planapprove/planapprove.component';
import { ConfirmationOfPaymentComponent } from './confirmation-of-payment/confirmation-of-payment.component';
import { InformConfirmationComponent } from './inform-confirmation/inform-confirmation.component';
import { NetCompleteComponent } from './net-complete/net-complete.component';
import { NetErrrorComponent } from './net-errror/net-errror.component';
import { NetCancelComponent } from './net-cancel/net-cancel.component';
import { NetPaymentinprogressComponent } from './net-paymentinprogress/net-paymentinprogress.component';
export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href.split('/')[3];
}
export function HttpLoaderFactory(httpClient: HttpClient) {
  const countryCode=getBaseUrl();
  return new TranslateHttpLoader(httpClient, './assets/i18n/'+countryCode+'/overview/', '.json');
}

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatMomentDateModule,
    MatDatepickerModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      isolate: true
    }),
    RouterModule.forChild(overViewListRoutes),
  ],
  declarations: [OverviewListComponent, PaymentOptionsComponent, PostponementComponent, PaymentComponent, PlanapproveComponent, ConfirmationOfPaymentComponent, InformConfirmationComponent, NetCompleteComponent, NetErrrorComponent, NetCancelComponent, NetPaymentinprogressComponent],
  exports: [OverviewListComponent, PaymentOptionsComponent],
  // schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class OverviewModule {
  constructor() {
  }

 }
