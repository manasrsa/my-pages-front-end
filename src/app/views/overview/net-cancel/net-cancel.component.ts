import { APP_BASE_HREF } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { BreadrumService } from 'src/app/shared/_service/breadcrum/breadcrum.service';
import { CommonService } from 'src/app/shared/_service/common/common.service';

@Component({
  selector: 'app-net-cancel',
  templateUrl: './net-cancel.component.html',
  styleUrls: ['./net-cancel.component.scss']
})
export class NetCancelComponent implements OnInit {
  conuntryCode:string='se';
  currentLang:string='';
  breadCrumData:any='';
  constructor(
    private breadCrumService:BreadrumService,
    private commonService:CommonService,
    private translate:TranslateService,
    private cookieService:CookieService,
    @Inject(APP_BASE_HREF) private baseHref: string
    ) { 
        this.commonService.setErrorMsg('');
        this.conuntryCode=this.baseHref.split("/")[1];
        this.currentLang=this.conuntryCode=='se' ? 'sv' : 'fi';   

        this.breadCrumService.getBreadcrum().subscribe((response: any) => {
          this.breadCrumData = response.result;
        });
       // get localization
       this.commonService.getLanguage.subscribe((data:any) => {
        this.translate.use(data).subscribe(res=>{
           
        });
      });
  }

  ngOnInit() {
    this.setLocalization();
    this.setBreadCrumd()
  }
  // set localization
  setLocalization() {
    this.translate.addLangs(['sv', 'en', 'fi']);
    if (this.translate.getBrowserLang() !== undefined) {
      let lang = this.cookieService.get('.AspNetCore.Culture');
    
      if (lang) {
        var langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.currentLang=langCode;
        this.translate.use(langCode);
      }else {
        this.translate.use(this.currentLang);
      }
    }
    else {
      this.translate.use(this.currentLang);
    }
  }
  setBreadCrumd(){
    const breadcrum:any=JSON.parse(this.commonService.getBreadCrumDynamic());
    if(this.breadCrumData){
      this.breadCrumData.map((obj, index) => {
        if (obj.params.id) {
          if(obj){
            this.breadCrumData[index].breadcrumb = breadcrum.data;
          }
        }
    });
    }
  }


}
