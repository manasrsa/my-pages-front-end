import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NetCancelComponent } from './net-cancel.component';

describe('NetCancelComponent', () => {
  let component: NetCancelComponent;
  let fixture: ComponentFixture<NetCancelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NetCancelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NetCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
