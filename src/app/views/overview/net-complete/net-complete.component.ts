import { APP_BASE_HREF } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { BreadrumService } from 'src/app/shared/_service/breadcrum/breadcrum.service';
import { CommonService } from 'src/app/shared/_service/common/common.service';

@Component({
  selector: 'app-net-complete',
  templateUrl: './net-complete.component.html',
  styleUrls: ['./net-complete.component.scss']
})
export class NetCompleteComponent implements OnInit {
  breadCrumData:any='';
  confirmationMsg:string='';
  conuntryCode:string='se';
  currentLang:string='';
  constructor(
    private breadCrumService:BreadrumService,
    private commonService:CommonService,
    private translate:TranslateService,
    private activateRoute:ActivatedRoute,
    private cookieService:CookieService,
    @Inject(APP_BASE_HREF) private baseHref: string
    ) { 
      this.commonService.setErrorMsg('');
      this.conuntryCode=this.baseHref.split("/")[1];
      this.currentLang=this.conuntryCode=='se' ? 'sv' : 'fi'; 
     this.breadCrumService.getBreadcrum().subscribe((response: any) => {
      this.breadCrumData = response.result;
    });
       // get localization
       this.commonService.getLanguage.subscribe((data:any) => {
        this.translate.use(data).subscribe(res=>{
          this.setConfirmationMsg();
           
        });
      });
   }

  ngOnInit() {
    this.setLocalization();
    this.setBreadCrumd();
    this.setConfirmationMsg();
  }
  setConfirmationMsg(){
    this.confirmationMsg= this.translate.instant('netconfiramtionText');
  }
  // set localization
  setLocalization() {
    this.translate.addLangs(['sv', 'en', 'fi']);
    //this.translate.setDefaultLang('en');
  }
  setBreadCrumd(){
    const breadcrum:any=JSON.parse(this.commonService.getBreadCrumDynamic());
    if(this.breadCrumData){
      this.breadCrumData.map((obj, index) => {
        if (obj.params.id) {
          if(obj){
            this.breadCrumData[index].breadcrumb = breadcrum.data;
          }
        }
    });
    }
  }

}
