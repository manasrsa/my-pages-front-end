import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NetCompleteComponent } from './net-complete.component';

describe('NetCompleteComponent', () => {
  let component: NetCompleteComponent;
  let fixture: ComponentFixture<NetCompleteComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NetCompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NetCompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
