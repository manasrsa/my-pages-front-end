/* tslint:disable:no-unused-variable */
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PaymentOptionsComponent } from './payment-options.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateOverviewLoader } from 'src/app/shared/testing/translate.overview';
import { HttpClient } from '@angular/common/http';
import { APP_BASE_HREF } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared/shared.module';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of, throwError } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { OverviewService } from 'src/app/shared/_service/overview/overview.service';
import { CommonService } from 'src/app/shared/_service/common/common.service';

import { Observable } from 'rxjs-compat';
import { ValidationService } from 'src/app/shared/_service/validation/validation.service';
describe('PaymentOptionsComponent', () => {
  let component: PaymentOptionsComponent;
  let fixture: ComponentFixture<PaymentOptionsComponent>;
  let translateService;
  let mockOverviewService;
  let mockOverViewData={
    "data": {
      "caseNumber": "90265756",
      "caseGuid": "1629178A-65B4-4F97-AD16-864F23F394CB",
      "clientName": "Sergel Finans AB (Övertag från Tele2 Sverige AB)",
      "isInstallmentPlanAllowed": true,
      "paymentPlanAllowed": true,
      "paymentPlanMaxNumOfOccasions": 6,
      "debtCollectionFee": 180,
      "interest": 10.27,
      "paidAmount": 0,
      "respiteMaxDaysAllowed": 0,
      "status": "Efterbevakning",
      "debtToPay": 301.27,
      "debts": [
        {
          "invoiceAmount": 111,
          "invoiceNumber": "1018889261039",
          "groundsOfClaim": "Tele tjänster",
          "dueDate": "2019-10-29T00:00:00"
        }
      ]
    },
    "result": {
      "resultCode": 0,
      "resultText": "Ok"
    }
  }
  let mockPaymentRef={
    "data": {
      "paymentReferenceNumber": "970100771537254643812"
    },
    "result": {
      "resultCode": 0,
      "resultText": "Ok"
    }
  }
  const testError = {
    status: 406,
    error: {
        message: 'Test 406 error'
    }
};
const mockData = {
  firstName: {
      pattern: '^[^0-9]+$'
  },
  lastName: {
      pattern: '^[^0-9]+$'
  },
  ssn: {
      se: {
          pattern: '^(19|20)?\\d{8}([-]|\\s)\\d{4}$',
          datepattern: '^(?!0000)(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])$'
      },
      en: {
          pattern: '^(?!0000)[0-9]{6}[-A][0-9]{3}[0-9A-Z]{1}$',
          datepattern: '^(?!0000)(0[1-9]|[1-2][0-9]|3[0-1])(0[1-9]|1[0-2])$'
      }

  },
  zipCode: {
      pattern: '^(?!0+$)[0-9]{5}$'
  },
  phone: {
      pattern: '^[+]?[0-9- ]*$'
  },
  email: {
      pattern: '^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'
  },
  mobile: {
      pattern: '^[+]?[0-9- ]*$'
  },
  config: {
      required: 'Please fill this field.',
      invalidFirstName: 'Numbers are not allowed.',
      invalidLastName: 'Numbers are not allowed.',
      invalidSsnFormat: 'Format should be YYYYMMDD-NNNN.',
      invalidSsnDate: 'Please enter a valid SSN.',
      invalidSsnDebtor: 'Receiver of power of attorney can\'t have the same SSN as debtor',
      invalidPoa_ssn_list: 'There already is a power of attorney for this SSN',
      invalidSocialSecurity: 'Please enter social security number in the format DDMMYY-NNNN.',
      invalidZipCode: 'Format should be 01234',
      invalidMobile: 'Please enter a valid phone number',
      invalueWhitespace: 'Please enter valid input',
      invalidPhone: 'Please enter a valid phone number',
      invalidEmail: 'The email is not in valid format.'

  }
};

  beforeEach(waitForAsync(() => {
    mockOverviewService=jasmine.createSpyObj('overviewService',['getCaseDetails','getPaymentReference'])
    TestBed.configureTestingModule({
      declarations: [ PaymentOptionsComponent ],
      imports: [
        BrowserAnimationsModule,
        NoopAnimationsModule,
        RouterTestingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useClass: TranslateOverviewLoader,
          deps: [HttpClient]
        }
      })],
      providers: [
        TranslateService,
        ValidationService,
        {
          provide: APP_BASE_HREF,
          useValue: "/se"
        },
        {provide: OverviewService, useValue: mockOverviewService},
        {provide:ActivatedRoute,useValue:{
          snapshot: { url:[{path:'Payment Options'}],
          data: {title:'Payment Options'},params:{id:"1629178A-65B4-4F97-AD16-864F23F394CB"},paramMap:{get: () =>'1629178A-65B4-4F97-AD16-864F23F394CB'} }
        }}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentOptionsComponent);
    component = fixture.componentInstance;
    translateService = TestBed.get(TranslateService);
    ValidationService.validFile=mockData;
    ValidationService.countryCode='se';
    translateService.use('en');
    mockOverviewService.getCaseDetails.and.returnValue(of(mockOverViewData));
    mockOverviewService.getPaymentReference.and.returnValue(of(mockPaymentRef));
    component.caseResult.debtToPay=301.27;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call the caseDetails',()=>{
    mockOverviewService.getCaseDetails.and.returnValue(of(mockOverViewData));
    fixture.detectChanges();
    expect(component.caseResult).toEqual(mockOverViewData.data);
    expect(mockOverViewData.result.resultCode).toEqual(0);
  })
  it('should call the caseDetails when result code not equal 0',()=>{
    mockOverViewData.data=null;
    mockOverViewData.result.resultCode=100;
    mockOverViewData.result.resultText="Some issues for fetching the caseDetail details "
    mockOverviewService.getCaseDetails.and.returnValue(of(mockOverViewData));
    fixture.detectChanges();
    expect(component.caseResult).not.toEqual(mockOverViewData.data);
  })
  it('should call the getOcrNumber when throw error ',()=>{
    mockOverviewService.getCaseDetails.and.returnValue(Observable.throw(testError))
    component.caseDetails();
    fixture.detectChanges();
    expect(component.ocrNumber).not.toEqual(mockPaymentRef.data);
  })
  it('should call the getOcrNumber',()=>{
    mockOverviewService.getPaymentReference.and.returnValue(of(mockPaymentRef));
    component.getOcrNumber("1629178A-65B4-4F97-AD16-864F23F394CB");
    fixture.detectChanges();
    expect(component.ocrNumber).toEqual(mockPaymentRef.data.paymentReferenceNumber);
  })
  it('should call the getOcrNumber when result code not equal 0',()=>{
    let paymentRef=mockPaymentRef;//.data=null;
    paymentRef.data=null;
    paymentRef.result.resultCode=100;
    paymentRef.result.resultText="Some issues for fetching the paymentReferenceNumber details "
    mockOverviewService.getPaymentReference.and.returnValue(of(paymentRef));
    component.getOcrNumber("1629178A-65B4-4F97-AD16-864F23F394CB");
    fixture.detectChanges();
    expect(component.ocrNumber).not.toEqual(mockPaymentRef.data);
  });
  
  it('should call the postPonePaymentCalculateDate when country code fi',()=>{
    component.countrycode='fi';
    spyOn(component,'postPonePaymentCalculateDate').and.callThrough();
    component.postPonePaymentCalculateDate();
    fixture.detectChanges();
    expect(component.postPonePaymentCalculateDate).toHaveBeenCalled();
  });
  it('should check country code fi',()=>{
    component.countrycode='fi';
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.countrycode).toEqual('fi');
  })
  it('should call the accordian for swish',()=>{
    spyOn(component, 'accordian');
    mockOverviewService.getCaseDetails.and.returnValue(of(mockOverViewData))
    component.caseDetails();
    component.countrycode='se';
    component.caseResult=mockOverViewData.data;
    const swishPaymentDebug=fixture.debugElement.query(By.css('.swish-payment-title'));
    swishPaymentDebug.triggerEventHandler('click',null);
    
    component.accordian('','swish');
    expect(component.accordian).toHaveBeenCalled();
  })
});
