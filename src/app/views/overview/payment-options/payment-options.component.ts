import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subscription, throwError } from 'rxjs';
import { BreadrumService } from 'src/app/shared/_service/breadcrum/breadcrum.service';
import { CommonService } from 'src/app/shared/_service/common/common.service';
import { OverviewService } from 'src/app/shared/_service/overview/overview.service';
import * as _moment from 'moment';
import * as _rollupMoment from 'moment';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { APP_BASE_HREF, LocationStrategy } from '@angular/common';
import { ValidationService } from 'src/app/shared/_service/validation/validation.service';
import { HostListener } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { ApplicationInsightsService } from 'src/app/shared/_service/app-insights/appInsights-service';
import { filter } from 'rxjs/operators';
import { ToastsComponent } from 'src/app/shared/components/toasts/toasts.component';
import { StringFormat } from 'src/app/shared/modal/global';
import { interval } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { ProfileModalComponent } from 'src/app/shared/components/profile-modal/profile-modal.component';
import { LogOutService } from 'src/app/shared/_service/logout/logout.service';
const moment = _rollupMoment || _moment;

export const MAT_MOMENT_DATE_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD.MM.YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-payment-options',
  templateUrl: './payment-options.component.html',
  styleUrls: ['./payment-options.component.scss'],
  providers: [{ provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },

  { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },]
})
export class PaymentOptionsComponent implements OnInit {
  // All ready paid
  netpaymentActive: boolean = false
  informPaymentForm: FormGroup;
  paymentIHavePaids: string = '';
  maxIHaveDate: Date = new Date();
  countrycode = 'se';
  splitPaymentForm: FormGroup;
  swishForm: FormGroup;
  currency = 'kr'; // '€'
  monthLists: any = [

  ];
  selectMonthName = 'select-months';
  monthValue = '';
  breadCrumData: any = '';
  // [] as CaseDetails[]
  caseResult: any = { caseNumber: null, clientName: '', debtToPay: 0 };
  ocrNumber = '';
  postponePaymentForm: FormGroup;
  guid = '';
  newPaymentDate: any = '';
  calculateInstallmentPlan: any = '';
  backBrowserEnable: boolean = false;
  swishTimer: number = 0;
  currentLang: string = '';
  debtorDetail = [];
  notificationChecked = false;
  debtorDetailSubscription: Subscription;

  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    //history.pushState(null,null,window.location.href);
    if (this.backBrowserEnable) {
      history.pushState(null, null, window.location.href);
      this.openDialog();
    }
    if (!this.commonService.transactionGet()) {
      this.backBrowserEnable = false;
    }

  }

  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler($event) {


    if (this.backBrowserEnable) {
      return $event.returnValue = 'Transaction in progress';
    }

  }

  constructor(private formBuilder: FormBuilder,
    private activateRoute: ActivatedRoute,
    public breadCrumService: BreadrumService,
    private overviewService: OverviewService,
    private translate: TranslateService,
    private commonService: CommonService,
    private snackBar: MatSnackBar,
    private appInsightService: ApplicationInsightsService,
    private router: Router,
    @Inject(APP_BASE_HREF) private baseHref: string,
    private location: LocationStrategy,
    public dialog: MatDialog,
    private cookieService: CookieService,
    private logOutService: LogOutService
  ) {
    // preventing back button in browser 
    history.pushState(null, null, window.location.href);
    this.commonService.setErrorMsg('');
    this.countrycode = this.baseHref.split('/')[1];
    this.currentLang = this.countrycode == 'se' ? 'sv' : 'fi';
    this.currency = this.countrycode == 'se' ? 'kr' : '€';
    this.breadCrumService.getBreadcrum().subscribe((response: any) => {
      this.breadCrumData = response.result;
    });
    // get localization
    this.commonService.getLanguage.subscribe((data: any) => {

      this.translate.use(data).subscribe(() => {
        if (this.caseResult.clientName != '') {
          this.paymentIHavePaids = StringFormat(this.translate.instant("paymentIHavePaid"), this.caseResult.clientName, this.caseResult.caseNumber);
          this.selectMonthName = this.translate.instant('select-months');
        }
      });
    });

    this.debtorDetailSubscription = this.commonService.getCaseDetails().subscribe((data)=>{
      this.debtorDetail = data;
    });
  }



  ngOnInit() {
    this.splitPaymentForm = this.formBuilder.group({
      selectMonth: ['', [Validators.required]],
      approve: ['', [Validators.required]]
    });
    this.postponePaymentForm = this.formBuilder.group({
      postponepayment: ['', [Validators.required]],
      approvePostpone: ['', []],
    });
    this.swishForm = this.formBuilder.group({
      swishMobile: ['', [Validators.required, ValidationService.getMobileValidate]]
    });

    this.informPaymentForm = this.formBuilder.group({
      payment_mode: ['FP', Validators.required],
      partial_amount: [''],
      payment_date: ['', Validators.required]
      // checkInformPayment: ['']
    })
    this.commonService.setConfirmation({ "payment-mode": 'FP' });

    this.setLocalization();
    this.guid = this.activateRoute.snapshot.paramMap.get('id');
    if (this.countrycode == 'fi') {
      this.postPonePaymentCalculateDate();
    }
    this.selectMonthName = this.translate.instant('select-months');
    this.caseDetails();
    this.getOcrNumber(this.activateRoute.snapshot.paramMap.get('id'));
    this.debtorDetail = JSON.parse(sessionStorage.getItem("DebtorDetails"));
  }

  postPonePaymentCalculateDate() {
    this.newPaymentDate = new Date(Date.now() + 12096e5);
    const val = this.translate.instant('days');
    this.postponePaymentForm.controls['postponepayment'].setValue(val);
  }
  // init method for click drop down
  init() {
    document.querySelector('.selectric-wrapper').classList.toggle('selectric-open');
  }
  selectedOption(index, month) {
    const options = document.querySelectorAll('.last');
    for (let i = 0; i < options.length; i++) {
      if (i === index) {
        options[index].classList.add('selected');

        if (month !== 0) {
          this.monthValue = month;
          this.calculateInstallMentPlan(month);
          this.splitPaymentForm.controls['selectMonth'].setValue(month);

        } else {
          this.monthValue = '';
          this.selectMonthName = 'select-months';
          this.splitPaymentForm.controls['selectMonth'].setValue('');
        }

      } else {
        options[i].classList.remove('selected');
      }
    }
  }
  calculateInstallMentPlan(month) {
    this.commonService.notifySpinnerAction(true);
    const guid = this.activateRoute.snapshot.paramMap.get('id');
    this.overviewService.getCalculateInstalement(guid, month).subscribe((response: any) => {
      if (response.result.resultCode === 0) {
        this.calculateInstallmentPlan = response.data;
        this.commonService.notifySpinnerAction(false);
      } else {
        this.commonService.notifySpinnerAction(false);
        this.commonService.setErrorMsg(response.result.resultText);
        this.openErrorToast();
      }

    }, error => {
      this.commonService.notifySpinnerAction(false);
      let msgerror = this.translate.instant('technicalIssue');
      this.commonService.setErrorMsg(msgerror);
      this.openErrorToast();
      throwError(msgerror);
    })
  }
  //create installment plan
  createInstallmentPlan() {
    this.commonService.notifySpinnerAction(true);
    const guid = this.activateRoute.snapshot.paramMap.get('id');
    this.overviewService.createInstalmentPlan(guid, this.calculateInstallmentPlan.monthlyPayment).subscribe((response: any) => {
      if (response.result.resultCode === 0) {
        this.router.navigateByUrl('/overview/' + guid + '/plan-approved');
        this.commonService.notifySpinnerAction(false);
      } else {
        this.commonService.notifySpinnerAction(false);
        this.commonService.setErrorMsg(response.result.resultText);
        this.openErrorToast();
      }
    }, error => {
      this.commonService.notifySpinnerAction(false);
      let msgerror = this.translate.instant('technicalIssue');
      this.commonService.setErrorMsg(msgerror);
      this.openErrorToast();
      throwError(msgerror);
    })
  }
  // get case detail specification
  caseDetails() {
    const guid = this.activateRoute.snapshot.paramMap.get('id');
    this.commonService.notifySpinnerAction(true);
    this.overviewService.getCaseDetails(guid).subscribe((response: any) => {

      if (response.result.resultCode === 0) {
        this.caseResult = response.data;
        this.paymentIHavePaids = StringFormat(this.translate.instant('paymentIHavePaid'), this.caseResult.clientName, this.caseResult.caseNumber);
        this.commonService.setTotalDebt(this.caseResult.debtToPay);

        for (let i = 2; i <= this.caseResult.paymentPlanMaxNumOfOccasions; i++) {
          this.monthLists.push(i);
        }

        if (this.breadCrumData.length > 1) {
          this.breadCrumData.map((obj, index) => {
            if (obj.params.id) {
              this.breadCrumData[index].breadcrumb = response.data.clientName + ': ' + response.data.caseNumber;
              this.commonService.removeBreadCrum();
              this.commonService.setBreadCrumDynamic({ 'data': response.data.clientName + ': ' + response.data.caseNumber });
            }
          });
        }
        this.commonService.notifySpinnerAction(false);

      } else {

        this.commonService.setErrorMsg(response.result.resultText);
        this.openErrorToast();
        this.commonService.notifySpinnerAction(false);
      }

    }, error => {
      this.commonService.notifySpinnerAction(false);
      let msgerror = this.translate.instant('technicalIssue');
      this.commonService.setErrorMsg(msgerror);
      this.openErrorToast();
      throwError(msgerror);
    });
  }
  // get ocr number
  getOcrNumber(caseGuid) {
    this.commonService.notifySpinnerAction(true);
    this.overviewService.getPaymentReference(caseGuid).subscribe((res: any) => {
      if (res.result.resultCode === 0) {
        this.ocrNumber = res.data.paymentReferenceNumber;
        this.commonService.notifySpinnerAction(false);
      } else {
        this.commonService.setErrorMsg(res.result.resultText);
        this.openErrorToast();
        this.commonService.notifySpinnerAction(false);
      }

    }, err => {
      this.commonService.notifySpinnerAction(false);
      let msgerror = this.translate.instant('technicalIssue');
      this.commonService.setErrorMsg(msgerror);
      this.openErrorToast();
      throwError(msgerror);
    });
  }
  // set localization
  setLocalization() {
    this.translate.addLangs(['sv', 'en', 'fi']);
    if (this.translate.getBrowserLang() !== undefined) {
      let lang = this.cookieService.get('.AspNetCore.Culture');

      if (lang) {
        var langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.currentLang = langCode;
        this.translate.use(langCode);
      } else {
        this.translate.use(this.currentLang);
      }
    }
    else {
      this.translate.use(this.currentLang);
    }
  }
  accordian(index, className) {
    const chevronContainer = document.querySelectorAll('.accordian-header');
    const openElaborateContainer = document.querySelectorAll('.open-elaborate');
    openElaborateContainer.forEach((el, i) => {
      if (el.classList.contains(className)) {
        el.classList.toggle('in');
        chevronContainer[i].classList.toggle('active');
      } else {
        el.classList.remove('in');
        chevronContainer[i].classList.remove('active');
      }
    });

  }
  // sub accordian
  subaccordian(index, className) {
    const chevronContainer = document.querySelectorAll('.spec-inst-plan');
    const openElaborateContainer = document.querySelectorAll('.open-elaborate-second');
    openElaborateContainer.forEach((el, i) => {
      if (el.classList.contains(className)) {
        el.classList.toggle('in'); chevronContainer[i].classList.toggle('active');
      } else { el.classList.remove('in'); chevronContainer[i].classList.remove('active'); }
    });
  }
  // Selected month
  selectedMonth() {
    const result = this.monthLists.find(obj => obj.val == this.splitPaymentForm.value.selectMonth);
    if (result) {
      this.selectMonthName = result.name;
    }
  }
  // postpone payment
  postponePayment() {
    this.commonService.notifySpinnerAction(true); 
    const result = this.countrycode == 'se' ? this.postponePaymentForm.value : { postponepayment: '0' };
    const guid = this.activateRoute.snapshot.paramMap.get('id');
    if ((this.countrycode == 'se' && this.caseResult.respiteMaxDaysAllowed > 0) || (this.countrycode == 'fi' && this.caseResult.promiseToPayMaxDaysAllowed > 0)) {
      //if (result.postponepayment <= this.caseResult.respiteMaxDaysAllowed) {
      this.commonService.setErrorMsg('');
      this.overviewService.getRequestRespite(guid, result.postponepayment, this.countrycode).subscribe((response: any) => {
        if (response.result.resultCode == 0) {
          this.router.navigateByUrl('overview/' + this.guid + '/postponement');
          let days: any = ''
          if (this.countrycode == 'se') {
            days = { postponepayment: this.postponePaymentForm.value.postponepayment }
          }
          else {
            days = { postponepayment: this.caseResult.promiseToPayMaxDaysAllowed }
          }

          sessionStorage.setItem('days', JSON.stringify(days));
          this.commonService.setDays(days);
          this.commonService.notifySpinnerAction(false); 
        } else {
          this.commonService.setErrorMsg(response.result.resultText);
          this.openErrorToast();
          this.commonService.notifySpinnerAction(false);
        }
      }, error => {
        this.commonService.notifySpinnerAction(false);
        let msgerror = this.translate.instant('technicalIssue');
        this.commonService.setErrorMsg(msgerror);
        this.openErrorToast();
        throwError(msgerror);
      });
    } else {
      this.commonService.setErrorMsg(this.translate.instant('paymentDateInputValidErrorMsg'));
      this.openErrorToast();
    }
  }
  //crete swishpayment
  swishPayment() {
    const swishPaymentParameter = {
      "mobileNumber": this.swishForm.get('swishMobile').value,
      "ocr": this.ocrNumber,
      "caseGuid": this.activateRoute.snapshot.paramMap.get('id')

    }
    if (this.swishForm.valid && this.caseResult.debtToPay > 0) {
      this.backBrowserEnable = false;
      this.commonService.notifySpinnerAction(true);
      this.overviewService.createSwishPayment(swishPaymentParameter).subscribe((res: any) => {
        if (res.result.resultCode == 0) {
          this.swishTimer = 420;
          this.swishPaymentStatus(res.data);
          //this.commonService.notifySpinnerAction(false);
        } else if(res.result.resultCode == 10) {
          this.router.navigateByUrl('/overview/' + this.guid + '/swish-paymentinprogress');
          this.commonService.setswishErrorMsg(res.result.resultText);
          this.appInsightService.setTrackEvent('Payment in Progress');
          this.commonService.notifySpinnerAction(false);
          this.backBrowserEnable = false;
        }
        else {
          this.commonService.notifySpinnerAction(false);
          this.commonService.setErrorMsg(res.result.resultText);
          this.openErrorToast();
          this.backBrowserEnable = false;
        }
      }, error => {
        this.commonService.notifySpinnerAction(false);
        let msgerror = this.translate.instant('technicalIssue');
        this.commonService.setErrorMsg(msgerror);
        this.openErrorToast();
        this.backBrowserEnable = false;
        throwError(msgerror);
      })
    }

  }

  // swish payment status
  swishPaymentStatus(transactionID: string) {
    //this.caseResult.caseNumber
    this.overviewService.getSwishPayment(transactionID).subscribe((res: any) => {
      if (res.result.resultCode == 0) {

        this.router.navigateByUrl('/overview/' + this.guid + '/swish-complete');
        this.commonService.notifySpinnerAction(false);
        this.appInsightService.setTrackEvent('Susccess Swish Payment');
      }
      else if (res.result.resultCode == 1) {
        if (this.swishTimer > 0) {

          this.swishTimer = this.swishTimer - 5;
          // interval(5000)
          // .subscribe(() => {
          //   this.swishPaymentStatus();
          // });
          setTimeout(() => {
            this.swishPaymentStatus(transactionID);

          }, 5000);
          // setTimeout(this.swishPaymentStatus, 5000);
        }else{
          this.router.navigateByUrl('/overview/' + this.guid + '/swish-timeout');
          this.appInsightService.setTrackEvent('Transaction got TimeOut');
        }

      }
      else if (res.result.resultCode == 2) {

        this.router.navigateByUrl('/overview/' + this.guid + '/swish-decline');
        //this.commonService.setErrorMsg(res.result.resultText);
        //this.openErrorToast();
        this.appInsightService.setTrackEvent('Decline Swish Payment');
        this.commonService.notifySpinnerAction(false);
        this.backBrowserEnable = false;
      }
      else if (res.result.resultCode == 3) {

        this.router.navigateByUrl('/overview/' + this.guid + '/swish-error');
        this.commonService.setswishErrorMsg(res.result.resultText);
        //this.openErrorToast();
        this.appInsightService.setTrackEvent('Error Swish Payment');
        this.commonService.notifySpinnerAction(false);
        this.backBrowserEnable = false;
      }
      else if (res.result.resultCode == 4) {

        this.router.navigateByUrl('/overview/' + this.guid + '/swish-bookpaymentfailed');
        this.commonService.setswishErrorMsg(res.result.resultText);
        //this.openErrorToast();
        this.appInsightService.setTrackEvent('Error Swish Payment');
        this.commonService.notifySpinnerAction(false);
        this.backBrowserEnable = false;
      } else if (res.result.resultCode == 0o1) {
        
      }
      else {

        this.router.navigateByUrl('/overview/' + this.guid + '/swish-cancel');
        this.commonService.setErrorMsg(res.result.resultText);
        //this.openErrorToast();
        this.appInsightService.logException(res.result.resultText);
        this.backBrowserEnable = false;
        this.commonService.notifySpinnerAction(false);
      }

    }, (error: any) => {
      this.commonService.notifySpinnerAction(false);
      let msgerror = this.translate.instant('technicalIssue');
      this.commonService.setErrorMsg(msgerror);
      this.openErrorToast();
      this.appInsightService.logException(msgerror);
      this.backBrowserEnable = false;
    })
  }
  //inform payment
  informPayment() {
    let paymentMode = this.informPaymentForm.get('payment_mode').value;
    let guid = this.activateRoute.snapshot.paramMap.get('id');
    let obj = {
      "Amount": paymentMode != 'FP' ? this.informPaymentForm.get('partial_amount').value : 0,
      "CaseGuid": guid,
      "isFullAmount": paymentMode == 'FP' ? true : false,
      "PaymentDateVal": moment(this.informPaymentForm.get('payment_date').value).format('YYYY-MM-DD'),
      // "isNotify": this.informPaymentForm.get('checkInformPayment').value || false
      "isNotify": false
    }
    this.commonService.notifySpinnerAction(true);
    this.overviewService.getHowToPay(obj).subscribe((res: any) => {
      if (res.result.resultCode == 0) {
        if (paymentMode == 'FP') {
          this.router.navigateByUrl('/overview/' + guid + '/full-payment');
        }
        else {
          this.router.navigateByUrl('/overview/' + guid + '/partial-payment');
        }
        this.commonService.notifySpinnerAction(true);
      }
      else {
        this.commonService.setErrorMsg(res.result.resultText);
        this.openErrorToast();
        this.appInsightService.logException(res.result.resultText);
      }
      this.commonService.notifySpinnerAction(false);
    }, error => {
      let msgerror = this.translate.instant('technicalIssue');
      this.commonService.setErrorMsg(msgerror);
      this.openErrorToast();
      this.appInsightService.logException(msgerror);
      this.commonService.notifySpinnerAction(false);
      throwError(msgerror);
    })

  }

  changePartial(val) {

    if (val == 'HP') {
      this.informPaymentForm.controls["partial_amount"].setValidators([Validators.required, ValidationService.validateInformPartialAmount]);
    }
    else {
      this.informPaymentForm.get('partial_amount').setValue('');
      this.informPaymentForm.controls["partial_amount"].clearValidators();
    }
    this.informPaymentForm.controls["partial_amount"].updateValueAndValidity();
    this.commonService.setConfirmation({ "payment-mode": val });
  }
  //open error Toast Component
  openErrorToast() {
    this.snackBar.openFromComponent(ToastsComponent
      , {
        panelClass: 'error',
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
  }
  openDialog(): void {
    const modalName = 'Transaction';
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.id = 'modal-component';
    dialogConfig.height = 'auto';
    dialogConfig.width = '600px';
    dialogConfig.data = {
      name: 'transaction',
      // title: 'Are you sure you want to revoke this power of attorney?',
      buttonText: '',
      cancelButtonText: '',
      actionButtonText: '',
      modalName
    };
    dialogConfig.data.actionButtonText = this.translate.instant('Yes');
    dialogConfig.data.cancelButtonText = this.translate.instant('No');
    dialogConfig.data.bodyContent = '<h3>Transaction in progress</h3>';
    const modalDialog = this.dialog.open(ModalComponent, dialogConfig);
    modalDialog.afterClosed().subscribe(result => {
      let val = this.commonService.transactionGet();
      if (val) {
        this.backBrowserEnable = false;
        this.router.navigateByUrl('/overview');
        modalDialog.close();
      }
      else {
        this.backBrowserEnable = true;
      }
    })
  }


  //  @HostListener('window:unload', ['$event'])
  //    closeunloadHandler(event) {
  //     console.log("Processing unload...");
  //     return event.returnValue = 'Transaction in progress';
  //    }
  netPaymentButton() {
    this.netpaymentActive = true;
    this.commonService.notifySpinnerAction(true);
  }

  //open profile modal 
  openProfileModal() {
    const dialogRef = this.dialog.open(ProfileModalComponent, {
      height: 'auto',
      width: '900px',
      disableClose: true
    });

      dialogRef.afterClosed().subscribe(result => {
        this.notificationChecked = false;
      });
    
  }
}
