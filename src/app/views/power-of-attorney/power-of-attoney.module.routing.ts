import { Routes } from "@angular/router";
import { PowerOfAttorneyComponent } from './power-of-attorney.component';
export const PoweOfAtorneyRoutes: Routes = [
    {
        path:'',
        component:PowerOfAttorneyComponent,
        runGuardsAndResolvers:'always',
    }
]
