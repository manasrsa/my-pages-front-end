import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PowerOfAttorneyComponent } from './power-of-attorney.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PoweOfAtorneyRoutes } from './power-of-attoney.module.routing';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SharedModule } from 'src/app/shared/shared/shared.module';
export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href.split('/')[3];
}
export function HttpLoaderFactory(httpClient: HttpClient) {
  const countryCode=getBaseUrl();
    return new TranslateHttpLoader(httpClient, "./assets/i18n/"+countryCode+"/power/", ".json");
  }
@NgModule({
    imports:[
        CommonModule,
        SharedModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        TranslateModule.forChild({
            loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
            },
            isolate: true
          }),
        RouterModule.forChild(PoweOfAtorneyRoutes)
    ],
    declarations: [PowerOfAttorneyComponent],
    exports:[PowerOfAttorneyComponent],
    providers: []
})

export class PowerOfAttoneyModule{}