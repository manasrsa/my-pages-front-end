import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ValidationService } from 'src/app/shared/_service/validation/validation.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from 'src/app/shared/_service/common/common.service';
import { PoaService } from 'src/app/shared/_service/poa/poa.service';
import { throwError } from 'rxjs';
import { APP_BASE_HREF } from '@angular/common';
import { ToastsComponent } from 'src/app/shared/components/toasts/toasts.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CookieService } from 'ngx-cookie-service';
@Component({
  selector: 'app-power-of-attorney',
  templateUrl: './power-of-attorney.component.html',
  styleUrls: ['./power-of-attorney.component.scss']
})
export class PowerOfAttorneyComponent implements OnInit {
  powerOfAttorneyForm: FormGroup;
  toogleButton = false;
  public activePoweofAtonies: any = [];
  tooglePoaButtonText = 'plus-add-a-power-of-attorney';
  currency = 'kr'; // '€'
  conuntryCode = 'se';
  activatePowerOfAttorneyBut: boolean = false;
  revokeCaseBut = false;
  radomvalues = "off";
  currentLang: string = '';
  constructor(
    private formBuilder: FormBuilder,
    private matDialog: MatDialog,
    private translate: TranslateService,
    private commonService: CommonService,
    private poaService: PoaService,
    private snackBar: MatSnackBar,
    @Inject(APP_BASE_HREF) private baseHref: string,
    private cookieService: CookieService,
  ) {
    this.commonService.setErrorMsg('');

    // get localization
    this.conuntryCode = this.baseHref.split("/")[1];
    this.currentLang = this.conuntryCode == 'se' ? 'sv' : 'fi';
    this.currency = this.conuntryCode == 'se' ? 'kr' : '€';
    this.commonService.getLanguage.subscribe((data: any) => {
      this.translate.use(data);
    });
    this.commonService.getRevokeCaseActive.subscribe(data => {
      this.revokeCaseBut = data;
    });
    this.commonService.revoke.subscribe(data => {
      if (data == 'success') {
        this.getPoa();
      }
    });
  }

  ngOnInit() {
    // form
    this.createForm();
    // call set localization
    this.setLocalization();
    // call the get poa list
    this.getPoa();
  }
  //create form 
  createForm() {
    this.powerOfAttorneyForm = this.formBuilder.group({
      firstName: ['', [Validators.required, ValidationService.getFirstNameValidator]],
      lastName: ['', [Validators.required, ValidationService.getLastNameValidator]],
      ssn: ['', [Validators.required, ValidationService.getSssnValidator]],
      street_address: ['', [Validators.required, ValidationService.AddressValidator, ValidationService.noWhitespace]],
      zip_code: ['', [Validators.required, ValidationService.getZipCodeValidate]],
      city: ['', [Validators.required, ValidationService.getCityValidate, ValidationService.noWhitespace]],
      phone_number: [''],
      poa_approval: [false, Validators.requiredTrue]
    }
    );
  }
  // set localization
  setLocalization() {
    this.translate.addLangs(['sv', 'en', 'fi']);
    if (this.translate.getBrowserLang() !== undefined) {
      let lang = this.cookieService.get('.AspNetCore.Culture');

      if (lang) {
        var langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.currentLang = langCode;
        this.translate.use(langCode);
      } else {
        this.translate.use(this.currentLang);
      }
    }
    else {
      this.translate.use(this.currentLang);
    }
  }
  //phone validation
  mobileValidation() {
    if (this.powerOfAttorneyForm.get('phone_number').value.length != 0) {
      this.powerOfAttorneyForm.get('phone_number').setValidators([ValidationService.getPhoneValidate]);
      this.powerOfAttorneyForm.get('phone_number').updateValueAndValidity();
    }
    else {
      this.powerOfAttorneyForm.get('phone_number').clearValidators();
      this.powerOfAttorneyForm.get('phone_number').updateValueAndValidity();
    }
  }
  // get poa list
  getPoa() {
    this.commonService.notifySpinnerAction(true);
    this.poaService.getPoa().subscribe((res: any) => {
      if (res.result.resultCode == 0) {
        if (res.data.debtorPowerOfAttorneys) {
          this.commonService.notifySpinnerAction(false);
          this.activePoweofAtonies = res.data.debtorPowerOfAttorneys;
          console.log(this.activePoweofAtonies);
          this.commonService.setActivePowerOfAttorney(this.activePoweofAtonies);
        }

      } else {
        this.commonService.notifySpinnerAction(false);
        this.commonService.setActivePowerOfAttorney(this.activePoweofAtonies);
        this.commonService.setErrorMsg(res.result.resultText);
        this.openErrorToast();
      }

    }, error => {
        this.commonService.notifySpinnerAction(false);
        let msgerror = this.translate.instant('technicalIssue');
      this.commonService.setActivePowerOfAttorney(this.activePoweofAtonies);
        throwError(msgerror);
        this.commonService.setErrorMsg(msgerror);
      this.openErrorToast();
    });
  }

  // get f() { return this.powerOfAttorneyForm.controls; }
  powerOfAttorneySubmit() {
    this.activatePowerOfAttorneyBut = true;
    this.commonService.notifySpinnerAction(true);
    if (this.powerOfAttorneyForm.valid) {
      const attorneyParty = {
        AttorneyParty: {
          FullName: this.powerOfAttorneyForm.get('firstName').value + '  ' + this.powerOfAttorneyForm.get('lastName').value,
          IdNumber: this.powerOfAttorneyForm.get('ssn').value,
          CellularPhone: this.powerOfAttorneyForm.get('phone_number').value,
          Addresses: [{
            Street: this.powerOfAttorneyForm.get('street_address').value,
            ZipCode: this.powerOfAttorneyForm.get('zip_code').value,
            City: this.powerOfAttorneyForm.get('city').value
          }]
        }
      };
      this.poaService.addPoa(attorneyParty).subscribe((res: any) => {
        if (res.result.resultCode == 0) {
          this.getPoa();
          this.activatePowerOfAttorneyBut = false;
          this.toogleButton = true;
          this.toogleAddPowerOfAttorney();
          this.createForm();
          this.commonService.notifySpinnerAction(false);
        }
        else {
          this.activatePowerOfAttorneyBut = false;
          this.commonService.notifySpinnerAction(false);
          this.commonService.setErrorMsg(res.result.resultText);
          this.openErrorToast();
        }

      }, error => {
        this.commonService.notifySpinnerAction(false);
          this.activatePowerOfAttorneyBut = false;
          let msgerror = this.translate.instant('technicalIssue');
          this.commonService.setErrorMsg(msgerror);
        this.openErrorToast();
          throwError(msgerror);
      });
    }
  }
  //open error Toast Component
  openErrorToast() {
    this.snackBar.openFromComponent(ToastsComponent
      , {
        panelClass: 'error',
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
  }
  /**Toogle Power of attorney */
  toogleAddPowerOfAttorney() {
    this.toogleButton = !this.toogleButton;
    this.tooglePoaButtonText = this.toogleButton ? 'hide-form' : 'plus-add-a-power-of-attorney';
  }
  /** Revoke case */
  revokeCase(PartyGuid) {
    const modalName = 'revoke';
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.id = 'modal-component';
    dialogConfig.height = 'auto';
    dialogConfig.width = '600px';
    dialogConfig.data = {
      name: 'revoke',
      // title: 'Are you sure you want to revoke this power of attorney?',
      bodyContent: '',
      cancelButtonText: '',
      actionButtonText: '',
      PartyGuid,
      modalName
    };
    dialogConfig.data.actionButtonText = this.translate.instant('yes-revoke');
    dialogConfig.data.cancelButtonText = this.translate.instant('msgCancelButtonText');
    dialogConfig.data.bodyContent = '<h3>' + this.translate.instant('poaRevokeModalBodyContent') + '</h3>';
    const modalDialog = this.matDialog.open(ModalComponent, dialogConfig);
  }
}
