/* tslint:disable:no-unused-variable */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PowerOfAttorneyComponent } from './power-of-attorney.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';
import { MatDialogModule } from '@angular/material/dialog';
import { TranslateModule } from '@ngx-translate/core';
import { ValidationService } from 'src/app/shared/_service/validation/validation.service';
import { SharedModule } from 'src/app/shared/shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { APP_BASE_HREF } from '@angular/common';
const mockData = {
  firstName: {
      pattern: '^[^0-9]+$'
  },
  lastName: {
      pattern: '^[^0-9]+$'
  },
  ssn: {
      se: {
          pattern: '^(19|20)?\\d{8}([-]|\\s)\\d{4}$',
          datepattern: '^(?!0000)(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])$'
      },
      en: {
          pattern: '^(?!0000)[0-9]{6}[-A][0-9]{3}[0-9A-Z]{1}$',
          datepattern: '^(?!0000)(0[1-9]|[1-2][0-9]|3[0-1])(0[1-9]|1[0-2])$'
      }

  },
  zipCode: {
      pattern: '^(?!0+$)[0-9]{5}$'
  },
  phone: {
      pattern: '^[+]?[0-9- ]*$'
  },
  email: {
      pattern: '^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'
  },
  mobile: {
      pattern: '^[+]?[0-9- ]*$'
  },
  config: {
      required: 'Please fill this field.',
      invalidFirstName: 'Numbers are not allowed.',
      invalidLastName: 'Numbers are not allowed.',
      invalidSsnFormat: 'Format should be YYYYMMDD-NNNN.',
      invalidSsnDate: 'Please enter a valid SSN.',
      invalidSsnDebtor: 'Receiver of power of attorney can\'t have the same SSN as debtor',
      invalidPoa_ssn_list: 'There already is a power of attorney for this SSN',
      invalidSocialSecurity: 'Please enter social security number in the format DDMMYY-NNNN.',
      invalidZipCode: 'Format should be 01234',
      invalidMobile: 'Please enter a valid phone number',
      invalueWhitespace: 'Please enter valid input',
      invalidPhone: 'Please enter a valid phone number',
      invalidEmail: 'The email is not in valid format.'

  }
};
describe('PowerOfAttorneyComponent', () => {
  let component: PowerOfAttorneyComponent;
  let fixture: ComponentFixture<PowerOfAttorneyComponent>;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        PowerOfAttorneyComponent,
      ],
      imports: [
        RouterTestingModule,
        FormsModule,
        MatDialogModule,
        SharedModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        ReactiveFormsModule],
      providers: [ValidationService,
        {
          provide: APP_BASE_HREF,
          useValue: "/se"
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PowerOfAttorneyComponent);
    component = fixture.componentInstance;
    ValidationService.validFile=mockData;
    component.toogleButton = true;
    ValidationService.countryCode='se';
    fixture.detectChanges();
  
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('form invalid when empty', () => {
    expect(component.powerOfAttorneyForm.valid).toBeFalsy();
  });
  it('First Name field validity required', () => {
    let errors = {};
    let firstName = component.powerOfAttorneyForm.controls['firstName'];
    errors = firstName.errors || {};
    expect(errors['required']).toBeTruthy();
    
    // set first Name to somthing
    firstName.setValue('12133')
    errors=firstName.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['invalidFirstName']).toBeTruthy();

    //set first Name to only space
    firstName.setValue('   ');
    errors=firstName.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['invalidFirstName']).toBeFalsy();
    expect(errors['invalueWhitespace']).toBeTruthy();
  });
  // Last Name field 
  it('Last Name field validity required', () => {
    let errors={};
    let lastName=component.powerOfAttorneyForm.controls['lastName'];
    errors=lastName.errors || {};
    expect(errors['required']).toBeTruthy();
    
    
    //set last name only space
    lastName.setValue('  ');
    errors=lastName.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['invalueWhitespace']).toBeTruthy();

    //set last name numeric
    lastName.setValue('123233');
    errors=lastName.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['invalueWhitespace']).toBeFalsy();
    expect(errors['invalidLastName']).toBeTruthy();
  });
  it('Ssn field validity required', () => {
    let errors={};
    let ssn=component.powerOfAttorneyForm.controls['ssn'];
    errors=ssn.errors || {};
    expect(errors['required']).toBeTruthy();    

    //Set ssn something space only
    ssn.setValue('  ');
    errors=ssn.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['invalueWhitespace']).toBeTruthy();

    // set ssn something wrong ssn invalidSsnDate
    ssn.setValue('11730126-0225');
    errors=ssn.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['invalueWhitespace']).toBeFalsy();
    expect(errors['invalidSsnDate']).toBeTruthy();
    //19730126-0225

  });
  it('should Street Address validity required',()=>{
    let errors={};
    let streetAddress=component.powerOfAttorneyForm.controls['street_address'];
    errors=streetAddress.errors || {};
    expect(errors['required']).toBeTruthy();

     //set input value only space
    streetAddress.setValue('  ');
    errors=streetAddress.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['invalueWhitespace']).toBeTruthy(); 

  });

  it('should zip code validity required',()=>{
    let errors={};
    let zipcode=component.powerOfAttorneyForm.controls['zip_code'];
    errors=zipcode.errors || {};
    expect(errors['required']).toBeTruthy();

    //set somthing input value only space

    zipcode.setValue('  ');
    errors=zipcode.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['invalueWhitespace']).toBeTruthy();

    // set somthing input value is not valid zip code

    zipcode.setValue('123123123123'); //01235
    errors=zipcode.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['invalueWhitespace']).toBeFalsy();
    expect(errors['invalidZipCode']).toBeTruthy();

});
  it('should city validity required',()=>{
  let errors={};
  let city=component.powerOfAttorneyForm.controls['city'];
  errors=city.errors || {};
  expect(errors['required']).toBeTruthy();

  //set something input value only space
  city.setValue('  ');
  errors=city.errors || {};
  expect(errors['required']).toBeFalsy();
  expect(errors['invalueWhitespace']).toBeTruthy();

  //set somthing input value 

});
  it('should phone validity',()=>{
  let errors={};
  let phone_number=component.powerOfAttorneyForm.controls['phone_number'];
  errors=phone_number.errors || {};
  //set somthing input value
  phone_number.setValue('asdasd');
  errors=phone_number.errors || {};
  expect(errors['invalidPhone']).toBeTruthy();

});
  it('form is invalide button is disable',()=>{
    component.conuntryCode='se';
    component.toogleButton = true;
    fixture.detectChanges();
    let submitEL: DebugElement = fixture.debugElement.query(By.css('Button'));
    expect(submitEL.nativeElement.disabled).toBe(true);
});
  it('form is valid button is enable', () => {
    component.toogleButton=true;
    ValidationService.countryCode='se';
    component.powerOfAttorneyForm.controls['firstName'].setValue('Mukesh');
    component.powerOfAttorneyForm.controls['lastName'].setValue('Singh');
    component.powerOfAttorneyForm.controls['ssn'].setValue('19730126-0225');
    component.powerOfAttorneyForm.controls['street_address'].setValue('Street 4');
    component.powerOfAttorneyForm.controls['zip_code'].setValue('01234');
    component.powerOfAttorneyForm.controls['city'].setValue('Bangalore');
    component.powerOfAttorneyForm.controls['phone_number'].setValue('+912323923');
    component.powerOfAttorneyForm.controls['poa_approval'].setValue('true');
    //expect(component.powerOfAttorneyForm.valid).toBeTruthy();

    let submitEL: DebugElement = fixture.debugElement.query(By.css('Button'));
    fixture.detectChanges();
    expect(submitEL.nativeElement.disabled).toBe(false);
});
  it('should call the submit button', () => {
    component.toogleButton=true;
   
    component.powerOfAttorneyForm.controls['firstName'].setValue('Mukesh');
    component.powerOfAttorneyForm.controls['lastName'].setValue('Singh');
    component.powerOfAttorneyForm.controls['ssn'].setValue('19730126-0225');
    component.powerOfAttorneyForm.controls['street_address'].setValue('Street 4');
    component.powerOfAttorneyForm.controls['zip_code'].setValue('01234');
    component.powerOfAttorneyForm.controls['city'].setValue('Bangalore');
    component.powerOfAttorneyForm.controls['phone_number'].setValue('+912323923');
    component.powerOfAttorneyForm.controls['poa_approval'].setValue('true');
    let submitEL: DebugElement = fixture.debugElement.query(By.css('Button.js-form-submit'));
    fixture.detectChanges();
    component.powerOfAttorneySubmit();
    expect(component.powerOfAttorneyForm.valid).toBeTruthy();

});

});
