/* tslint:disable:no-unused-variable */
import { TestBed, ComponentFixture, tick, fakeAsync, inject, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { Routes, Router, NavigationEnd, RouterEvent,
   NavigationStart,  ActivatedRoute} from '@angular/router';
import { APP_BASE_HREF, CommonModule,Location } from '@angular/common';
import { LayoutComponent } from './shared/components/layout/layout.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './shared/shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Subject,  ReplaySubject, BehaviorSubject } from 'rxjs';
import { Renderer2, Type } from '@angular/core';
import { SpyLocation }   from '@angular/common/testing';
import { OverviewListComponent } from './views/overview/overview-list/overview-list.component';
import { Title } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { ModalComponent } from './shared/components/modal/modal.component';
import { CustomDirective } from './shared/directive/custom.directive';
import { SpinnerComponent } from './shared/components/spinner/spinner.component';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { appRoutingModule } from './app-routing.module';

describe('AppComponent', () => {
    let fixture: ComponentFixture<AppComponent>;
    let component: AppComponent;
    let location: SpyLocation;
    let router:Router;
    let mockAuthenticationService;
    let methodSpy: jasmine.Spy;
    let titleMethodSpy:jasmine.Spy;
    const eventSubject = new ReplaySubject<RouterEvent>(1);
    let renderer2: Renderer2;
    
   let mockRoute: any = { snapshot: {}};
    mockRoute.parent = { params: new Subject<any>()};
    mockRoute.params = new Subject<any>();
    mockRoute.queryParams = new Subject<any>()
    let applicationTitle: Title;
    const routerMock = {
        navigate: jasmine.createSpy('navigate'),
        events: eventSubject.asObservable(),
        // url: 'test/url'
    };
    // const routes: Routes = [
    //     {
    //         path: '',
    //         redirectTo: '/overview',
    //         pathMatch: 'full'
    //       },
    //       {
    //         path: '', 
    //         component: LayoutComponent,
    //           children: [
    //             {
    //               path: 'overview',
    //               loadChildren: './views/overview/overview.module#OverviewModule',
    //               data:{
    //                 title: "Overview",
    //                 breadcrumb: "Overview",
    //                 auth: 'Overview'
    //               }
    //             },
    //             {
    //              path: 'power-of-attorney', 
    //              loadChildren:'./views/power-of-attorney/power-of-attoney.module#PowerOfAttoneyModule', 
    //              data: {title: 'Power Of Attoney', breadcrumb: 'Power Of Attoney', auth: 'Power Of Attoney'},
    //             },
    //             {
    //              path: 'contact-us', 
    //              loadChildren: './views/contact-us/contactus.module#ContactUsModule',
    //              data: {title: 'Contact Us', breadcrumb: 'Contact Us', auth: 'Contact Us'},
    //             },
    //             {
    //              path: 'messages', 
    //              loadChildren: './views/messages/messages.module#MessagesModule',
    //              data: {title: 'Messages', breadcrumb: 'Messages', auth: 'Messages'},
    //             }
    //           ]  
    //       },
          
      
    //       // otherwise redirect to home
    //       { path: '**', redirectTo: 'overview' }
    //     ];
    const fakeActivatedRoute = {
            snapshot: { url:[{path:'overview'}] }
          } as ActivatedRoute;
          let appTitle: Title;
    beforeEach(waitForAsync(() => {
        mockAuthenticationService=jasmine.createSpyObj('AutheticationService',['currentUser'])
        TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(appRoutingModule),
        CommonModule,
        FormsModule,
        SharedModule,
        MatButtonModule,
        MatDialogModule,
        MatInputModule,
        MatNativeDateModule, MatRippleModule,
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        // RouterModule.forRoot(routes)
      ],
      declarations: [
        AppComponent,
        ModalComponent,
        CustomDirective,
        SpinnerComponent
      ],
      providers: [
        Renderer2,
        { provide: Title, useClass: Title },
        {provide:ActivatedRoute,useValue:{
          snapshot: { url:[{path:'overview'}],
          data: {title:'overview'},params:{id:1} }
        }},
          {provide: APP_BASE_HREF, useValue: '/'
        },
        {
            provide: Router,
            useValue: routerMock
        }
    ]
    }).compileComponents();
  }));
    beforeEach(()=>{
      
      fixture = TestBed.createComponent(AppComponent);
      component = fixture.componentInstance;
      const injector = fixture.debugElement.injector;
      location = injector.get(Location) as SpyLocation;
      router = injector.get(Router);
      applicationTitle=TestBed.get(Title);
      //router.initialNavigation();
      //router.initialNavigation();
      // fakeActivatedRoute.snapshot.data={title: "Overview",breadcrumb: "",auth: 'Overview'};
      // fakeActivatedRoute.snapshot.params={id:1};
      
      renderer2 = fixture.componentRef.injector.get<Renderer2>
      (Renderer2 as Type<Renderer2>);
      spyOn(renderer2, 'addClass').and.callThrough();
      spyOn(renderer2, 'removeClass').and.callThrough();
 
      methodSpy=spyOn(component,'renderDomClass');
      //titleMethodSpy=spyOn(component,'changePageTitle');
      component.ngOnInit();
      component.changePageTitle();
      //component.title.setTitle('My Pages');
      fixture.detectChanges();
  });
  
    it('should create the app', () => {
            const app = fixture.debugElement.componentInstance;
            expect(app).toBeTruthy();
        });
    it('should call the  router naviation start',()=>{
          eventSubject.next(new NavigationStart(1, '/overview'));
          fixture.detectChanges();
          expect(methodSpy).toHaveBeenCalledTimes(1);
       });
    it('should call the renderDomClass when contact-us page',()=>{
        eventSubject.next(new NavigationStart(1, '/contact-us'));
        fixture.detectChanges();
        expect(methodSpy).toHaveBeenCalledTimes(1);
    });
    it('should call the renderDomClass when power-of-attorney',()=>{
        eventSubject.next(new NavigationStart(1, '/power-of-attorney'));
       expect(methodSpy).toHaveBeenCalledTimes(1);
    });
    it('should call the renderDomClass when messages',()=>{
        eventSubject.next(new NavigationStart(1, '/messages'));
        fixture.detectChanges();
        expect(methodSpy).toHaveBeenCalledTimes(1);
    });
    it('Page title Should be Overview', () => {
      expect(applicationTitle.getTitle()).toBe("overview | My Pages");
  });
});

